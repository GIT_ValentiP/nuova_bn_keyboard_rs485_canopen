; 20121007 : - aggiunti tasti per movimentazione schienale a motori singoli

#include <P16F877A.INC>

#define    TLC_OPERATIVO             ; disabilita il tasto ROMBO (STORE)

#define    LED_SHIFT      PORTC,1
#define    CNT_DEBOUNCE   30         ; contatore debounce (n campionamenti consecutivi)
#define    CNT_DEBOUNCE_IRDA   1     ; contatore debounce (n campionamenti consecutivi) per funz. irda
#define    TIME_SHIFT     .38        ; 20s (timebase = 530ms)
#define    TIME_SHIFT_1   .113       ; 60s (timebase = 530ms)
#define    TIME_SHIFT_IRDA  .210     ; 20s (timebase = 95ms = mainloop)
#define    BIT_MODO_FUNZ  0          ; modo di funzionamento
#define    MODO_485       0          ; modo di funzionamento
#define    MODO_IRDA      1          ; modo di funzionamento
#define    LED_IR         PORTC,0
#define    BUZZER         PORTC,0
#define    DURATA_TX_IR   ((72*2) + (24*2))  ; 72ms+24ms con loop interno a 500us
#define    DURATA_PAUSA_IR   (24*2)          ; 24ms con loop interno a 500us
#define    COD_MANTENIMENTO   0x21   ; codice IRDA di mantenimento con pochi "1" per non saturare il  ricevitore
                                     ; (il tasto vero viene trasmesso ogni MAX_CODICI_MNT di mantenimento)
#define    MAX_CODICI_MNT     5
#define    TASTO_SHIFT    0x3F       ; codice tasto shift

;----------------------------------------------
; Definizione delle variabili

Banco0  UDATA  0x20

  GLOBAL Temp, Slave, RX_Buff1, RX_Buff2, RX_Buff3, RX_Buff4, RX_Buff5, RX_Cnt, RX_Clear
  GLOBAL TX_Buff1, TX_Buff2, TX_Buff3, TX_Buff4, TX_Buff5, TX_Buff6, TX_Buff7, CRC_LO, CRC_HI
  GLOBAL Param1, Status_1, Status_2, Tasto_C1, Tasto_C2, Tasto_C3, Tasto_C4, Tasto
  GLOBAL Led_Fissi, Led_Lamp, Lamp_Timer, Shift_Timer, Beep, Temp1, Cnt_debounce_shift, Shift_status
  GLOBAL Addr_IR, Dato_IR, Cnt_bit_IR, Tx_IR, del38, Max_cod_mnt, Cnt_mnt

Temp          res 1   ; equ 0x20  ;Variabile temporanea di appoggio
Slave         res 1   ; equ 0x21

RX_Buff1		res 1   ; equ 0x22
RX_Buff2		res 1   ; equ 0x23
RX_Buff3		res 1   ; equ 0x24
RX_Buff4		res 1   ; equ 0x25
RX_Buff5		res 1   ; equ 0x26
RX_Cnt			res 1   ; equ 0x27 ;Num Byte ricevuti
RX_Clear		res 1   ; equ 0x28 ;Se zero devo azzerare il buffer di ricezione al prox carattere

TX_Buff1		res 1   ; equ 0x29
TX_Buff2		res 1   ; equ 0x2A
TX_Buff3		res 1   ; equ 0x2B
TX_Buff4		res 1   ; equ 0x2C
TX_Buff5		res 1   ; equ 0x2D
TX_Buff6		res 1   ; equ 0x2E
TX_Buff7		res 1   ; equ 0x2F

CRC_LO			res 1   ; equ 0x30
CRC_HI			res 1   ; equ 0x31


Param1			res 1   ; equ 0x32

Status_1		res 1   ; equ 0x33
Status_2		res 1   ; equ 0x34

Tasto_C1		res 1   ; equ 0x35
Tasto_C2		res 1   ; equ 0x36
Tasto_C3		res 1   ; equ 0x37
Tasto_C4		res 1   ; equ 0x38
Tasto			  res 1   ; equ 0x39

Led_Fissi		res 1   ; equ 0x3A        ; 0=OFF ; 1=ON
Led_Lamp		res 1   ; equ 0x3B        ; 0=OFF ; 1=ON
Lamp_Timer	res 1   ; equ 0x3C
Shift_Timer	res 1   ; equ 0x3D        ; timer per accensione led SHIFT

Beep			  res 1   ; equ 0x3E
Temp1			  res 1   ; equ 0x3F	;Variabile temporanea

Cnt_debounce_shift res 1   ; equ 0x40     ; debounce tasto shift
Shift_status       res 1   ; equ 0x41     ; stato shift (temporizzato)

Tlc_type    res 1   ; equ 0x42        ; modo di funzionamento (485/IRDA)
Addr_IR     res 1   ; equ 0x43        ; indirizzo da trasmettere IR (3 bits)
Dato_IR     res 1   ; equ 0x44        ; dato da trasmettere IR      (6 bits)
Cnt_bit_IR  res 1   ; equ 0x45        ; contatore bit da trasmettere
Tx_IR       res 1   ; equ 0x46        ; registro fisico di trasmissione IR
del38       res 1   ; equ 0x47        ; usata per temporizzare il segnale 38KHz
Max_cod_mnt res 1   ; equ 0x48        ; numero codice di mantenimento (per tasti nuovi) da trasmettere al posto del tasto vero
Cnt_mnt     res 1   ; equ 0x49        ; contatore per trasmettere codice vero/mantenimento (=0 --> tasto vero; !=0 --> cod. mantenim.)

; Memoria comune a tutti i banchi
W_TEMP        equ 0x70
STATUS_TEMP   equ 0x71
PCLATH_TEMP   equ 0x72

;----------------------------------------------
; EEPROM MEMORY

;----------------------------------------------
; Mappa tasti
;  cod_IR new = cod_IR_OLD << 2  per includere i due bit fissi a "0" del telecomando IR vecchio
;
;  I bit vengono trasmessi con questo ordine su Irda (MC14026) da telecomando:
;  primo                                   ultimo
;   A1 - A2 - A3 - A4 - A5 - D0 - D1 - D2 - D3      <-- pin telec. IR old (con MC14026)
;  |------------| |-------| |-----------------|
;    dip-switch    fissi=0    tasto
;                  su tlc_old
;
;
;   es. Tasto 0x22 (485) -->  cod_IR_old = 03 = 0011   (D3-D2-D1_D0)
;                             cod_IR_new = 0C = 001100 (D3-D2-D1-D0-A5-A4)
;   es. Tasto 0x24 (485) -->  cod_IR_old = 07 = 0111   (D3-D2-D1_D0)
;                             cod_IR_new = 1C = 011100 (D3-D2-D1-D0-A5-A4)
;  A5 e A4 sono utilizzati per espandere da 16 a 64 i tasti
;
; Mappa compatibile con telecomando/logica vecchia
;  cod_485   cod_IR   cod_IR
;             old      new
;     11       00       00      mot 01 avanti        su
;     21       01       04      mot 01 indietro      giu
;     41                        mot 01 zero
;     12       02       08      mot 02 avanti        controtrendle
;     22       03       0C      mot 02 indietro      trendle
;     42                        mot 02 zero
;     13       04       10      mot 03 avanti        lat. sx
;     23       05       14      mot 03 indietro      lat. dx
;     43                        mot 03 zero
;     14       06       18      mot 04 avanti        trasl. piedi
;     24       07       1C      mot 04 indietro      trasl. testa
;     44                        mot 04 zero
;     15       08       20      mot 05 avanti        schienale su
;     25       09       24      mot 05 indietro      schienale giu
;     45                        mot 05 zero
;     16       0A       28      mot 06 avanti        torace su
;     26       0B       2C      mot 06 indietro      torace giu
;     46                        mot 06 zero
;     17                        mot 07 avanti
;     27                        mot 07 indietro
;     47                        mot 07 zero
;     18                        mot 07 avanti
;     28                        mot 07 indietro
;     19                        mot 07 avanti
;     29                        mot 07 indietro
;     33       0F       3C      zero
;
;
; Mappa compatibile con telecomando/logica vecchia con tasti nuovi
;  cod_485   cod_IR   cod_IR
;             old      new
;     11       00       00      mot 01 avanti
;     12       02       08      mot 02 avanti
;     13       04       10      mot 03 avanti
;     14       06       18      mot 04 avanti
;     15       08       20      mot 05 avanti
;     16       0A       28      mot 06 avanti
;     17                30      mot 07 avanti
;     18                32      mot 07 avanti sx
;     19                31      mot 07 avanti dx
;     1A                        mot 05 avanti sx
;     1B                        mot 05 avanti dx
;     21       01       04      mot 01 indietro
;     22       03       0C      mot 02 indietro
;     23       05       14      mot 03 indietro
;     24       07       1C      mot 04 indietro
;     25       09       24      mot 05 indietro
;     26       0B       2C      mot 06 indietro
;     27                34      mot 07 indietro
;     28                35      mot 07 indietro sx
;     29                36      mot 07 indietro dx
;     2A                        mot 05 indietro sx
;     2B                        mot 05 indietro dx
;     31                11      rombo (store)
;     32                12      punto (recall)
;     33       0F       3C      zero
;     34                19      Recall_MemA
;     35                15      Recall_MemB
;     36                16      Recall_MemC
;     37                37      Store_MemA
;     38                38      Store_MemB
;     39                39      Store_MemC
;     3A                3A      Reflex
;     3B                3B      Chair
;     3C                13      Flex
;     41                01      mot 01 zero
;     42                02      mot 02 zero
;     43                03      mot 03 zero
;     44                09      mot 04 zero
;     45                05      mot 05 zero
;     46                06      mot 06 zero
;     47                07      mot 07 zero
;     54                29      Recall_MemD
;     55                25      Recall_MemE
;     56                26      Recall_MemF
;     57                3D      Store_MemD
;     58                3E      Store_MemE
;     59                3F      Store_MemF
;
;
; codici 485 (interni) rispetto a cod_irda_new (cornice)
;      0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
;  0  11  41  42  43  21  45  46  47  12  44          22
;  1  13  31  32  3C  23  35  36      14  34          24
;  2  15  EE          25  55  56      16  54          26
;  3  17  19  18      27  28  29  37  38  39  3A  3B  33  57  58  59
;
;
; 0xEE = codice di mantenimento (il tasto vero viene trasmesso una volta ogni
;        CNT_MANTENIMENTO, perch� se contiene troppi "1" satura il ricevitore
;        --> in caso di tasto continuamente premuto si trasmette il codice
;        di mantenimento che ha pochi "1" (b100001); nel caso dei tasti del tavolo
;        operatorio vecchio invece si trasmette sempre il tasto vero per
;        compatibilit�

;----------------------------------------------

	ORG 0
	goto start__code

	ORG 4
	goto _interrupt

;----------------------------------------------
; Reset
start__code

	nop 
	nop 
	nop
	nop
				; Azzero tutte le Porte
	clrf PORTA
	clrf PORTB
	clrf PORTC

	BANKSEL OPTION_REG
	movlw  B'11011111'       ; TMR0 clocked by internal clok; prescaler assigned to WDT
  movwf   OPTION_REG

_Init_1
	; Abilitazione Interrupt
	banksel PIE1
	bsf 	STATUS, RP0
	bsf		PIE1, TMR1IE  ; Abilita interrupt Timer1
;	bsf		PIE1, TMR2IE	; Abilita interrupt Timer2

	banksel INTCON
	bsf		INTCON, GIE	  ; Abilitazione generale interrupt
	bsf		INTCON, 6	    ; Abilitazione interrupt interni
	movlw	B'00000001'
	movwf	T1CON		      ; Timer1 Abilitato F.Osc/4 Prescaler=0

					
; Imposto la porta A come digitali
	banksel CMCON
	movlw 0x07
	movwf CMCON
	banksel ADCON1
	movlw 0x06
	movwf ADCON1
	movlw 0x24
	movwf TXSTA
	movlw 0x33
	movwf SPBRG			; baudrate 9.600

	bcf		TRISA, 2	; Uscita per Lettura Tasti Colonna 4
	bcf		TRISA, 3	; Uscita per Lettura Tasti Colonna 2
	bcf		TRISA, 4	; Uscita per Lettura Tasti Colonna 3
	bcf		TRISA, 5	; Uscita per Lettura Tasti Colonna 1
;	bcf		TRISA, 6
;	bcf		TRISA, 7
	bsf		TRISA, 1  ; Ingresso per Lettura Tasti Riga 8
	bsf		TRISA, 0	; Ingresso per Lettura Tasti Riga 7
;	clrf	TRISB
	bsf		TRISB, 0	; Ingresso per Lettura Tasti Riga 2
	bsf		TRISB, 1	; Ingresso per Lettura Tasti Riga 3
	bsf		TRISB, 2	; Ingresso per Lettura Tasti Riga 6
	bsf		TRISB, 3	; Ingresso per Lettura Tasti Riga 4
	bsf		TRISB, 4	; Ingresso per Lettura Tasti Riga 1
	bsf		TRISB, 5	; Ingresso per Lettura Tasti Riga 5

	bsf		TRISC, 7	; Abilitazione porta RX
	bsf		TRISC, 6	; Abilitazione porta TX
	bcf		TRISC, 5	; Uscita per Selezione RX-TX RS485
 	bcf		TRISC, 4	; Uscita free
 	bcf		TRISC, 3	; Uscita free
 	bcf		TRISC, 2	; Uscita free
 	bcf		TRISC, 1	; Uscita LED SHIFT
	bcf		TRISC, 0	; Uscita per Cicalino


	bsf		TRISE, 0  ; Ingresso per Lettura dip
	bsf		TRISE, 1  ; Ingresso per Lettura dip
	bsf		TRISE, 2  ; Ingresso per Lettura dip


	banksel	PORTA
  movlw MODO_485
  btfsc PORTE, 2
  movlw MODO_IRDA
  movwf Tlc_type  ; setto modo di funzionamento

  ; bsf   Tlc_type, BIT_MODO_FUNZ  ;!!!!!! solo per debug


  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  Config_IR                  ; IRDA

Config_no_IR
	banksel TRISD
  clrf	TRISD		  ; Uscite LED
	banksel	PORTA
  goto  _Init_2

Config_IR
	banksel TRISD
	bcf		TRISC, 6	; Abilitazione porta TX

	bcf		TRISD, 7	; Uscita free
	bcf		TRISD, 6	; Uscita free
	bcf		TRISD, 5	; Uscita free
 	bcf		TRISD, 4	; Uscita free
 	bcf		TRISD, 3	; Uscita free
 	bsf		TRISD, 2	; dip switch codice IRDA
 	bsf		TRISD, 1	; dip switch codice IRDA
	bsf		TRISD, 0	; dip switch codice IRDA

; IRDA --> non serve interrupt (no led/buzzer) e non deve essere eseguito per non alterare 38KHz
;  nota: si perde temporizzazione Tasto Shift che deve perci� essere fatta a software (main_loop = 95ms in caso di irda)
	bcf		PIE1, TMR1IE  ; Disabilita interrupt Timer1
	bcf		PIE1, TMR2IE	; Disabilita interrupt Timer2

	banksel	PORTA

_Init_2
	bcf		STATUS, RP0
	bcf		PIR1, TMR1IF	; clear flag timer 1
	bcf		PIR1, TMR2IF	; clear flag timer 2
	movlw	0x90
	movwf	RCSTA
	bcf		PORTC,5			; RS485 in ricezione

	banksel	PORTA
  movlw 0x3F        ; colonne tutte a "1"
  movwf PORTA

; Inizializzo le variabili
	
	clrf	Tasto
	clrf	RX_Cnt
	clrf	RX_Clear
	clrf	Status_1
	clrf	Status_2
	clrf	Led_Fissi    ; led off
	clrf	Led_Lamp     ; led off
	clrf	Beep         ; no beep
	clrf	Shift_Timer  ; led SHIFT off
	movlw	0x30		   ; Assegno lo slave 0x30 (base address) al tastierino
	movwf	Slave
  movf  PORTE, W   ; l'indirizzo � funzione degli address dip switch
  andlw 0x03
  addwf Slave, F


	btfss	STATUS, 4	; Controllo se ho avuto un WatchDog (se si non faccio beep)
	incf	Beep, f
;	incf	Beep

;******************************************************************
_Main	
  btfss Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo?
  call	_RX_Char                   ; si
  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando IRDA?
	call	_TX_IR                     ; si, invio il tasto su infrarosso (se non c'� tasto esegue comunque delay)
	nop
	call	_Led
	call	_Leggi_Tasto
	
	movlw	0x30		   ; Assegno lo slave 0x30 (base address) al tastierino
	movwf	Slave
  movf  PORTE, W   ; l'indirizzo � funzione degli address dip switch
  andlw 0x03
  addwf Slave, F

  movf  PORTD, w
  andlw 0x07
  movwf Addr_IR

  btfss Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  _Mainwd                    ; filo --> skip timer perch� gestito ad interrupt
	movf	Shift_Timer, f		; if (Shift_Timer!=0) Shift_Timer--
	btfss	STATUS, Z
  decf  Shift_Timer, f

_Mainwd
	clrwdt

; !!! debug
;!!!debug
;!!!  movf  Tasto, W
;!!!  sublw 0
;!!!  bz    end_debug
;!!!	call	_TX_Tasto
;!!!;
;!!!	movlw	0xFF
;!!!	movwf	Temp
;!!!	call	_Ritardo
;!!!end_debug

	goto	_Main

;**************************************************************************
;Tasto C1 C2 C3 C4    Tasto C1 C2 C3 C4  Tasto C1 C2 C3 C4
; 11   FE-FF-FF-FF                       21   FF-FE-FF-FF       COLONNA
; 41   FE-FF-FD-FF                       41   FF-FE-FD-FF       con pallino
;
; 12   FD-FF-FF-FF                       22   FF-FD-FF-FF       TRENDLE
; 42   FD-FF-FD-FF                       42   FF-FD-FD-FF       con pallino
;
; 13   FB-FF-FF-FF                       23   FF-FB-FF-FF       TILT
; 43   FB-FF-FD-FF                       43   FF-FB-FD-FF       con pallino
;
; 14   F7-FF-FF-FF                       24   FF-F7-FF-FF       TRASLAZIONE
; 44   F7-FF-FD-FF                       44   FF-F7-FD-FF       con pallino
;
; 15   EF-FF-FF-FF                       25   FF-EF-FF-FF       SCHIENALE
; 45   EF-FF-FD-FF                       45   FF-EF-FD-FF       con pallino
; 1A   6F-FF-FF-FF                       2A   7F-EF-FF-FF       con left
; 1B   EF-7F-FF-FF                       2B   FF-6F-FF-FF       con right
;
; 16   DF-FF-FF-FF                       26   FF-DF-FF-FF       TORACICA
; 46   DF-FF-FD-FF                       46   FF-DF-FD-FF       con pallino
;
; 17   BF-FF-FF-FF                       27   FF-BF-FF-FF       GAMBALI
; 47   BF-FF-FD-FF                       47   FF-BF-FD-FF       con pallino
; 18   3F-FF-FF-FF                       28   7F-BF-FF-FF       con left
; 19   BF-7F-FF-FF                       29   FF-3F-FF-FF       con right
;
; --   7F-FF-FF-FF                       --   FF-7F-FF-FF       LEFT - RIGHT
;
; 3A   FF-FF-FF-FE    3B   FF-FF-FF-FB   3C   FF-FF-FF-FD       REFLEX - CHAIR - REFLEX
;      BF-BF-FF-FF                            FF-BF-BF-FF       con pallino  !!!!
;
; 31   FF-FF-FE-FF    32   FF-FF-FD-FF   33   FF-FF-FB-FF       ROMBO - PUNTO - ZERO
;
; 34   FF-FF-F5-FF    35   FF-FF-ED-FF   36   FF-FF-DD-FF       RECALL MEM_A - MEM_B - MEM_C
; 54   FF-FF-F5-FF    55   FF-FF-ED-FF   56   FF-FF-DD-FF       SHIFT + RECALL MEM_A - MEM_B - MEM_C
; 37   FF-FF-F6-FF    38   FF-FF-EE-FF   39   FF-FF-DE-FF       STORE  MEM_A - MEM_B - MEM_C
; 57   FF-FF-F6-FF    58   FF-FF-EE-FF   59   FF-FF-DE-FF       SHIFT + STORE  MEM_A - MEM_B - MEM_C
;
;                          FF-FF-BF-FF                          SHIFT

;**************************************************************************
; ritorna in W lo stato righe ordinato
_Leggi_Righe
  movf	PORTB, W
  clrf  Temp1         ; riordino le righe in Temp1
	btfsc	PORTB, 4
	bsf		Temp1, 0
;  btfsc	PORTB, 0      ; old hw
	btfsc	PORTB, 5      ; new hw
	bsf		Temp1, 1
	btfsc	PORTB, 1
	bsf		Temp1, 2
	btfsc	PORTB, 3
	bsf		Temp1, 3
;  btfsc	PORTB, 5      ; old hw
	btfsc	PORTB, 0      ; new hw
	bsf		Temp1, 4
	btfsc	PORTB, 2
	bsf		Temp1, 5
	btfsc	PORTA, 0
	bsf		Temp1, 6
	btfsc	PORTA, 1
	bsf		Temp1, 7
  movf  Temp1, W
	return

;*****************************************************************
; ritardo impostazione colonne -> lettura righe
_Delay
  movlw  .7
  movwf  Temp1
DelLoop
  decfsz Temp1, f
  goto   DelLoop
	return

;*****************************************************************
_Leggi_Tasto
	banksel	PORTA
	clrf	Tasto

; ATTENZIONE: RA4 � open drain --> tempo di salita = 5...10us
;             --> NON USARE BSF/BCF per impostare i pin di colonna, perch�
;             BCF/BSF legge il valore attuale porta e setta/resetta il bit
;             indicato; se eseguita subito dopo la BSF PORTA,4 --> legge ancora
;             "0" sul pin RA4 e riporta a "0" nuovamente RA4, non permettendo di
;             settarlo a "1" (a meno di mettere un ritardo tra le istruzioni)

						; seleziono linea scansione RA5 (C1)
;	bsf		PORTA,2
;	bsf		PORTA,3
;	bsf		PORTA,4
;;	bsf		PORTC,3
;	bcf		PORTA,5
  movlw 0x1F
  movwf PORTA
  call  _Delay
  call  _Leggi_Righe
	movwf Tasto_C1

						; seleziono linea scansione RA3 (C2)
;	bsf		PORTA,2
;	bsf		PORTA,4
;;	bsf		PORTC,3
;	bsf		PORTA,5
;	bcf		PORTA,3
  movlw 0x37
  movwf PORTA
  call  _Delay
  call  _Leggi_Righe
	movwf Tasto_C2

						; seleziono linea scansione RA4 (C3)
;	bsf		PORTA,2
;	bsf		PORTA,3
;	bsf		PORTA,5
;	bcf		PORTA,4
;;	bcf		PORTC,3
  movlw 0x2F
  movwf PORTA
  call  _Delay
  call  _Leggi_Righe
	movwf Tasto_C3

						; seleziono linea scansione RA2 (C4)
;	bcf		PORTA,2
;	bsf		PORTA,3
;	bsf		PORTA,5
;	bsf		PORTA,4
;;	bcf		PORTC,3
  movlw 0x3B
  movwf PORTA
  call  _Delay
  call  _Leggi_Righe
	movwf Tasto_C4

  movlw 0x3F
  movwf PORTA


_T_M1A	; Motore 1 Avanti
; FE FF FF FF
	movf	Tasto_C2, W		; Solo un tasto Premuto
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M1I
	movlw 0xFE
	subwf	Tasto_C1, W
	bnz 	_T_M1I

	movlw	0x00              ; ex 0x00 shiftato di 2 per includere i 2 bit fissi a "0" nel vecchio telecomando (A4-A5)
  movwf Dato_IR
  clrf  Max_cod_mnt       ; nessun codice di mantenimento per tasti vecchi
	movlw	0x11
	goto Tasto_ready

_T_M1I	; Motore 1 Indietro
; FF FE FF FF
	movf	Tasto_C1, W		; Solo un tasto Premuto
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M1Z
	movlw 0xFE
	subwf	Tasto_C2, W
	bnz 	_T_M1Z

	movlw	0x04              ; ex 0x01 shiftato di 2 per includere i 2 bit fissi a "0" nel vecchio telecomando (A4-A5)
  movwf Dato_IR
  clrf  Max_cod_mnt       ; nessun codice di mantenimento per tasti vecchi
	movlw	0x21
	goto Tasto_ready

_T_M1Z	; Motore 1 in ZERO
; FE FF FD FF  o  FF FE FD FF
	movlw 0xFF
	subwf	Tasto_C4, W
	bnz 	_T_M2A
	movlw 0xFD
	subwf	Tasto_C3, W
	bnz 	_T_M2A
	movlw 0xFE
	subwf	Tasto_C1, W
  bz    _T_M1Za
	movlw 0xFE
	subwf	Tasto_C2, W
	bnz 	_T_M2A

_T_M1Zb
	movlw 0xFF
	subwf	Tasto_C1, W
	bnz 	_T_M2A
	goto	_T_M1Zok

_T_M1Za
	movlw 0xFF
	subwf	Tasto_C2, W
	bnz 	_T_M2A

_T_M1Zok
	movlw	0x01              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x41
	goto Tasto_ready

_T_M2A	; Motore 2 Avanti
; FD FF FF FF
	movf	Tasto_C2, W		; Solo un tasto Premuto
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M2I
	movlw 0xFD
	subwf	Tasto_C1, W
	bnz 	_T_M2I

	movlw	0x08              ; ex 0x02 shiftato di 2 per includere i 2 bit fissi a "0" nel vecchio telecomando (A4-A5)
  movwf Dato_IR
  clrf  Max_cod_mnt       ; nessun codice di mantenimento per tasti vecchi
	movlw	0x12
	goto Tasto_ready

_T_M2I	; Motore 2 Indietro
; FF FD FF FF
	movf	Tasto_C1, W		; Solo un tasto Premuto
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M2Z
	movlw 0xFD
	subwf	Tasto_C2, W
	bnz 	_T_M2Z

	movlw	0x0C              ; ex 0x03 shiftato di 2 per includere i 2 bit fissi a "0" nel vecchio telecomando (A4-A5)
  movwf Dato_IR
  clrf  Max_cod_mnt       ; nessun codice di mantenimento per tasti vecchi
	movlw	0x22
	goto Tasto_ready

_T_M2Z	; Motore 2 in ZERO
; FD FF FD FF  o  FF FD FD FF
	movlw 0xFF
	subwf	Tasto_C4, W
	bnz 	_T_M3A
	movlw 0xFD
	subwf	Tasto_C3, W
	bnz 	_T_M3A
	movlw 0xFD
	subwf	Tasto_C1, W
  bz    _T_M2Za
	movlw 0xFD
	subwf	Tasto_C2, W
	bnz 	_T_M3A

_T_M2Zb
	movlw 0xFF
	subwf	Tasto_C1, W
	bnz 	_T_M3A
	goto	_T_M2Zok

_T_M2Za
	movlw 0xFF
	subwf	Tasto_C2, W
	bnz 	_T_M3A

_T_M2Zok
	movlw	0x02              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x42
	goto Tasto_ready

_T_M3A	; Motore 3 Avanti
; FB FF FF FF
	movf	Tasto_C2, W		; Solo un tasto Premuto
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M3I
	movlw 0xFB
	subwf	Tasto_C1, W
	bnz 	_T_M3I

	movlw	0x10              ; ex 0x04 shiftato di 2 per includere i 2 bit fissi a "0" nel vecchio telecomando (A4-A5)
  movwf Dato_IR
  clrf  Max_cod_mnt       ; nessun codice di mantenimento per tasti vecchi
	movlw	0x13
	goto Tasto_ready

_T_M3I	; Motore 3 Indietro
; FF FB FF FF
	movf	Tasto_C1, W		; Solo un tasto Premuto
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M3Z
	movlw 0xFB
	subwf	Tasto_C2, W
	bnz 	_T_M3Z

	movlw	0x14              ; ex 0x05 shiftato di 2 per includere i 2 bit fissi a "0" nel vecchio telecomando (A4-A5)
  movwf Dato_IR
  clrf  Max_cod_mnt       ; nessun codice di mantenimento per tasti vecchi
	movlw	0x23
	goto Tasto_ready

_T_M3Z	; Motore 3 in ZERO
; FB FF FD FF  o  FF FB FD FF
	movlw 0xFF
	subwf	Tasto_C4, W
	bnz 	_T_M4A
	movlw 0xFD
	subwf	Tasto_C3, W
	bnz 	_T_M4A
	movlw 0xFB
	subwf	Tasto_C1, W
  bz    _T_M3Za
	movlw 0xFB
	subwf	Tasto_C2, W
	bnz 	_T_M4A

_T_M3Zb
	movlw 0xFF
	subwf	Tasto_C1, W
	bnz 	_T_M4A
	goto	_T_M3Zok

_T_M3Za
	movlw 0xFF
	subwf	Tasto_C2, W
	bnz 	_T_M4A

_T_M3Zok
	movlw	0x03              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x43
	goto Tasto_ready

_T_M4A	; Motore 4 Avanti
; F7 FF FF FF
	movf	Tasto_C2, W		; Solo un tasto Premuto
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M4I
	movlw 0xF7
	subwf	Tasto_C1, W
	bnz 	_T_M4I

	movlw	0x18              ; ex 0x06 shiftato di 2 per includere i 2 bit fissi a "0" nel vecchio telecomando (A4-A5)
  movwf Dato_IR
  clrf  Max_cod_mnt       ; nessun codice di mantenimento per tasti vecchi
	movlw	0x14
	goto Tasto_ready

_T_M4I	; Motore 4 Indietro
; FF F7 FF FF
	movf	Tasto_C1, W		; Solo un tasto Premuto
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M4Z
	movlw 0xF7
	subwf	Tasto_C2, W
	bnz 	_T_M4Z

	movlw	0x1C              ; ex 0x07 shiftato di 2 per includere i 2 bit fissi a "0" nel vecchio telecomando (A4-A5)
  movwf Dato_IR
  clrf  Max_cod_mnt       ; nessun codice di mantenimento per tasti vecchi
	movlw	0x24
	goto Tasto_ready

_T_M4Z	; Motore 4 in ZERO
; F7 FF FD FF  o  FF F7 FD FF
	movlw 0xFF
	subwf	Tasto_C4, W
	bnz 	_T_M5A
	movlw 0xFD
	subwf	Tasto_C3, W
	bnz 	_T_M5A
	movlw 0xF7
	subwf	Tasto_C1, W
  bz    _T_M4Za
	movlw 0xF7
	subwf	Tasto_C2, W
	bnz 	_T_M5A

_T_M4Zb
	movlw 0xFF
	subwf	Tasto_C1, W
	bnz 	_T_M5A
	goto	_T_M4Zok

_T_M4Za
	movlw 0xFF
	subwf	Tasto_C2, W
	bnz 	_T_M5A

_T_M4Zok
	movlw	0x09              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x44
	goto Tasto_ready

_T_M5A	; Motore 5 Avanti
; EF FF FF FF
	movf	Tasto_C2, W		; Solo un tasto Premuto
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M5I
	movlw 0xEF
	subwf	Tasto_C1, W
	bnz 	_T_M5I

	movlw	0x20              ; ex 0x08 shiftato di 2 per includere i 2 bit fissi a "0" nel vecchio telecomando (A4-A5)
  movwf Dato_IR
  clrf  Max_cod_mnt       ; nessun codice di mantenimento per tasti vecchi
	movlw	0x15
	goto Tasto_ready

_T_M5I	; Motore 5 Indietro
; FF EF FF FF
	movf	Tasto_C1, W		; Solo un tasto Premuto
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M5Z
	movlw 0xEF
	subwf	Tasto_C2, W
	bnz 	_T_M5Z

	movlw	0x24              ; ex 0x09 shiftato di 2 per includere i 2 bit fissi a "0" nel vecchio telecomando (A4-A5)
  movwf Dato_IR
  clrf  Max_cod_mnt       ; nessun codice di mantenimento per tasti vecchi
	movlw	0x25
	goto Tasto_ready

_T_M5Z	; Motore 5 in ZERO
; EF FF FD FF  o  FF EF FD FF
	movlw 0xFF
	subwf	Tasto_C4, W
	bnz  _T_M5SXA
	movlw 0xFD
	subwf	Tasto_C3, W
	bnz  _T_M5SXA
	movlw 0xEF
	subwf	Tasto_C1, W
  bz    _T_M5Za
	movlw 0xEF
	subwf	Tasto_C2, W
	bnz  _T_M5SXA

_T_M5Zb
	movlw 0xFF
	subwf	Tasto_C1, W
	bnz  _T_M5SXA
	goto	_T_M5Zok

_T_M5Za
	movlw 0xFF
	subwf	Tasto_C2, W
	bnz  _T_M5SXA

_T_M5Zok
	movlw	0x05              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x45
	goto Tasto_ready

_T_M5SXA
; 6F FF FF FF
	movf	Tasto_C2, W		; 2 tasti Premuti
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M5SXI
	movlw 0x6F
	subwf	Tasto_C1, W
	bnz 	_T_M5SXI

  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  _T_End                     ; IRDA --> tasto disabilitato
  movlw 0x30
  subwf Slave, W      ; tasto disabilitato per slave !=0
  bnz   _T_End

;  movlw	0x32              ; irda new
;  movwf Dato_IR
;  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
;  movwf Max_cod_mnt
  movlw	0x1A
  goto Tasto_ready

_T_M5SXI
; 7F EF FF FF
	movf	Tasto_C3, W		; 2 tasti Premuti
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M5DXA
	movlw 0x7F
	subwf	Tasto_C1, W
	bnz 	_T_M5DXA
	movlw 0xEF
	subwf	Tasto_C2, W
  bnz   _T_M5DXA

  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  _T_End                     ; IRDA --> tasto disabilitato
  movlw 0x30
  subwf Slave, W      ; tasto disabilitato per slave !=0
  bnz   _T_End

;  movlw	0x35              ; irda new
;  movwf Dato_IR
;  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
;  movwf Max_cod_mnt
	movlw	0x2A
	goto Tasto_ready

_T_M5DXA
; EF 7F FF FF
	movf	Tasto_C3, W		; 2 tasti Premuti
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M5DXI
	movlw 0xEF
	subwf	Tasto_C1, W
	bnz 	_T_M5DXI
	movlw 0x7F
	subwf	Tasto_C2, W
	bnz 	_T_M5DXI

  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  _T_End                     ; IRDA --> tasto disabilitato
  movlw 0x30
  subwf Slave, W      ; tasto disabilitato per slave !=0
  bnz   _T_End

;  movlw	0x31              ; irda new
;  movwf Dato_IR
;  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
;  movwf Max_cod_mnt
	movlw	0x1B
	goto Tasto_ready

_T_M5DXI
; FF 6F FF FF
	movf	Tasto_C3, W		; 2 tasti Premuti
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz  _T_M6A
	movlw 0xFF
	subwf	Tasto_C1, W
	bnz  _T_M6A
	movlw 0x6F
	subwf	Tasto_C2, W
	bnz  _T_M6A

  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  _T_End                     ; IRDA --> tasto disabilitato
  movlw 0x30
  subwf Slave, W      ; tasto disabilitato per slave !=0
  bnz   _T_End

;  movlw	0x36              ; irda new
;  movwf Dato_IR
;  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
;  movwf Max_cod_mnt
	movlw	0x2B
	goto Tasto_ready

_T_M6A	; Motore 6 Avanti
; DF FF FF FF
	movf	Tasto_C2, W		; Solo un tasto Premuto
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M6I
	movlw 0xDF
	subwf	Tasto_C1, W
	bnz 	_T_M6I

	movlw	0x28              ; ex 0x0A shiftato di 2 per includere i 2 bit fissi a "0" nel vecchio telecomando (A4-A5)
  movwf Dato_IR
  clrf  Max_cod_mnt       ; nessun codice di mantenimento per tasti vecchi
	movlw	0x16
	goto Tasto_ready

_T_M6I	; Motore 6 Indietro
; FF DF FF FF
	movf	Tasto_C1, W		; Solo un tasto Premuto
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M6Z
	movlw 0xDF
	subwf	Tasto_C2, W
	bnz 	_T_M6Z

	movlw	0x2C              ; ex 0x0B shiftato di 2 per includere i 2 bit fissi a "0" nel vecchio telecomando (A4-A5)
  movwf Dato_IR
  clrf  Max_cod_mnt       ; nessun codice di mantenimento per tasti vecchi
	movlw	0x26
	goto Tasto_ready

_T_M6Z	; Motore 6 in ZERO
; DF FF FD FF  o  FF DF FD FF
	movlw 0xFF
	subwf	Tasto_C4, W
	bnz 	_T_M7A
	movlw 0xFD
	subwf	Tasto_C3, W
	bnz 	_T_M7A
	movlw 0xDF
	subwf	Tasto_C1, W
  bz    _T_M6Za
	movlw 0xDF
	subwf	Tasto_C2, W
	bnz 	_T_M7A

_T_M6Zb
	movlw 0xFF
	subwf	Tasto_C1, W
	bnz 	_T_M7A
	goto	_T_M6Zok

_T_M6Za
	movlw 0xFF
	subwf	Tasto_C2, W
	bnz 	_T_M7A

_T_M6Zok
	movlw	0x06              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x46
	goto Tasto_ready


_T_M7A	; Motore 7 Avanti
; BF FF FF FF
	movf	Tasto_C2, W		; Solo un tasto Premuto
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M7I
	movlw 0xBF
	subwf	Tasto_C1, W
	bnz 	_T_M7I

	movlw	0x30              ; ex 0x0C shiftato di 2 per includere i 2 bit fissi a "0" nel vecchio telecomando (A4-A5)
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x17
	goto Tasto_ready

_T_M7I	; Motore 7 Indietro
; FF BF FF FF
	movf	Tasto_C1, W		; Solo un tasto Premuto
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M7Z
	movlw 0xBF
	subwf	Tasto_C2, W
	bnz 	_T_M7Z

	movlw	0x34              ; ex 0x0D shiftato di 2 per includere i 2 bit fissi a "0" nel vecchio telecomando (A4-A5)
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x27
	goto Tasto_ready

_T_M7Z	; Motore 7 in ZERO
; BF FF FD FF  o  FF BF FD FF
	movlw 0xFF
	subwf	Tasto_C4, W
	bnz   _T_M7SXA
	movlw 0xFD
	subwf	Tasto_C3, W
	bnz   _T_M7SXA
	movlw 0xBF
	subwf	Tasto_C1, W
  bz    _T_M7Za
	movlw 0xBF
	subwf	Tasto_C2, W
	bnz   _T_M7SXA

_T_M7Zb
	movlw 0xFF
	subwf	Tasto_C1, W
	bnz   _T_M7SXA
	goto	_T_M7Zok

_T_M7Za
	movlw 0xFF
	subwf	Tasto_C2, W
  bnz   _T_M7SXA

_T_M7Zok
	movlw	0x07              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x47
	goto Tasto_ready

_T_M7SXA
; 3F FF FF FF
	movf	Tasto_C2, W		; 2 tasti Premuti
	andwf	Tasto_C3, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M7SXI
	movlw 0x3F
	subwf	Tasto_C1, W
	bnz 	_T_M7SXI

	movlw	0x32              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x18
	goto Tasto_ready

_T_M7SXI
; 7F BF FF FF
	movf	Tasto_C3, W		; 2 tasti Premuti
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M7DXA
	movlw 0x7F
	subwf	Tasto_C1, W
	bnz 	_T_M7DXA
	movlw 0xBF
	subwf	Tasto_C2, W
  bnz   _T_M7DXA

	movlw	0x35              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x28
	goto Tasto_ready

_T_M7DXA
; BF 7F FF FF
	movf	Tasto_C3, W		; 2 tasti Premuti
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_M7DXI
	movlw 0xBF
	subwf	Tasto_C1, W
	bnz 	_T_M7DXI
	movlw 0x7F
	subwf	Tasto_C2, W
	bnz 	_T_M7DXI

	movlw	0x31              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x19
	goto Tasto_ready

_T_M7DXI
; FF 3F FF FF
	movf	Tasto_C3, W		; 2 tasti Premuti
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_Rombo
	movlw 0xFF
	subwf	Tasto_C1, W
	bnz 	_T_Rombo
	movlw 0x3F
	subwf	Tasto_C2, W
	bnz 	_T_Rombo

	movlw	0x36              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x29
	goto Tasto_ready

_T_Rombo   ; Save  (Store)
#ifdef TLC_OPERATIVO
  goto	_T_Punto	; !!! Da mettere in REM per abilitare la programmaz.
#endif

; FF FF FE FF
	movf	Tasto_C1, W		; Solo un tasto Premuto
	andwf	Tasto_C2, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_Punto
	movlw 0xFE
	subwf	Tasto_C3, W
	bnz   _T_Punto

  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  _T_Rombo2                  ; IRDA --> tasto abilitato
  movlw 0x30
  subwf Slave, W      ; tasto disabilitato per slave !=0
  bnz   _T_End
_T_Rombo2
	movlw	0x11              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x31
	goto Tasto_ready

_T_Punto   ; Recall
; FF FF FD FF
	movf	Tasto_C1, W		; Solo un tasto Premuto
	andwf	Tasto_C2, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_Zero
	movlw 0xFD
	subwf	Tasto_C3, W
	bnz 	_T_Zero

  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  _T_Punto2                  ; IRDA --> tasto abilitato
  movlw 0x30
  subwf Slave, W      ; tasto disabilitato per slave !=0
  bnz   _T_End
_T_Punto2
	movlw	0x12              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x32
	goto Tasto_ready

_T_Zero				;Modificato in data 10/01/2012 si perde la compatibilit� su questo comando con le schede logiche 130 (sostituita Riga 1278 con le righe 1276-1277)
; FF FF FB FF
	movf	Tasto_C1, W		; Solo un tasto Premuto
	andwf	Tasto_C2, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_Recall_MemA
	movlw 0xFB
	subwf	Tasto_C3, W
	bnz  _T_Recall_MemA

	movlw	0x3C              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
 ; clrf  Max_cod_mnt      ; nessun codice di mantenimento per tasti vecchi
	movlw	0x33
	goto Tasto_ready

_T_Recall_MemA
; FF FF F5 FF
	movf	Tasto_C1, W		; Recall + M1
	andwf	Tasto_C2, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz  _T_Recall_MemB
	movlw 0xF5
	subwf	Tasto_C3, W
	bnz  _T_Recall_MemB

  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  _T_RecMA                   ; IRDA --> tasto abilitato
  movlw 0x30
  subwf Slave, W      ; tasto disabilitato per slave !=0
  bnz   _T_End
_T_RecMA
	movf	Shift_Timer, f		; if (Shift_Timer!=0)
  bnz   MemSA
	movlw	0x19              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
  movlw	0x34
	goto Tasto_ready

MemSA
	movlw	0x29              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
  movlw	0x54
	goto Tasto_ready

_T_Recall_MemB
; FF FF ED FF
	movf	Tasto_C1, W		; Recall + M2
	andwf	Tasto_C2, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz  _T_Recall_MemC
	movlw 0xED
	subwf	Tasto_C3, W
	bnz  _T_Recall_MemC

  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  _T_RecMB                   ; IRDA --> tasto abilitato
  movlw 0x30
  subwf Slave, W      ; tasto disabilitato per slave !=0
  bnz   _T_End
_T_RecMB
	movf	Shift_Timer, f		; if (Shift_Timer!=0)
  bnz   MemSB
	movlw	0x15              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x35
	goto Tasto_ready

MemSB
	movlw	0x25              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
  movlw	0x55
	goto Tasto_ready

_T_Recall_MemC
; FF FF DD FF
	movf	Tasto_C1, W		; Recall + M3
	andwf	Tasto_C2, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz  _T_Store_MemA
	movlw 0xDD
	subwf	Tasto_C3, W
	bnz  _T_Store_MemA

  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  _T_RecMC                   ; IRDA --> tasto abilitato
  movlw 0x30
  subwf Slave, W      ; tasto disabilitato per slave !=0
  bnz   _T_End
_T_RecMC
	movf	Shift_Timer, f		; if (Shift_Timer!=0)
  bnz   MemSC
	movlw	0x16              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x36
	goto Tasto_ready

MemSC
	movlw	0x26              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
  movlw	0x56
	goto Tasto_ready

_T_Store_MemA
; FF FF F6 FF
	movf	Tasto_C1, W		; Store + M1
	andwf	Tasto_C2, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz  _T_Store_MemB
	movlw 0xF6
	subwf	Tasto_C3, W
	bnz  _T_Store_MemB

  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  _T_StoMA                   ; IRDA --> tasto abilitato
  movlw 0x30
  subwf Slave, W      ; tasto disabilitato per slave !=0
  bnz   _T_End
_T_StoMA
	movf	Shift_Timer, f		; if (Shift_Timer!=0)
  bnz   MemSSA
	movlw	0x37              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
  movlw	0x37
	goto Tasto_ready

MemSSA
	movlw	0x3D              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
  movlw	0x57
	goto Tasto_ready

_T_Store_MemB
; FF FF EE FF
	movf	Tasto_C1, W		; Store + M2
	andwf	Tasto_C2, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz  _T_Store_MemC
	movlw 0xEE
	subwf	Tasto_C3, W
	bnz  _T_Store_MemC

  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  _T_StoMB                   ; IRDA --> tasto abilitato
  movlw 0x30
  subwf Slave, W      ; tasto disabilitato per slave !=0
  bnz   _T_End
_T_StoMB
	movf	Shift_Timer, f		; if (Shift_Timer!=0)
  bnz   MemSSB
	movlw	0x38              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
  movlw	0x38
	goto Tasto_ready

MemSSB
	movlw	0x3E              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
  movlw	0x58
	goto Tasto_ready

_T_Store_MemC
; FF FF DE FF
	movf	Tasto_C1, W		; Store + M3
	andwf	Tasto_C2, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz  _T_Reflex
	movlw 0xDE
	subwf	Tasto_C3, W
	bnz  _T_Reflex

  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  _T_StoMC                   ; IRDA --> tasto abilitato
  movlw 0x30
  subwf Slave, W      ; tasto disabilitato per slave !=0
  bnz   _T_End
_T_StoMC
	movf	Shift_Timer, f		; if (Shift_Timer!=0)
  bnz   MemSSC
	movlw	0x39              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
  movlw	0x39
	goto Tasto_ready

MemSSC
	movlw	0x3F              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
  movlw	0x59
	goto Tasto_ready

_T_Reflex
; FF FF FF FE
	movf	Tasto_C1, W		; Solo un tasto Premuto
	andwf	Tasto_C2, W
	andwf	Tasto_C3, W
	sublw 0xFF
	bnz 	_T_Chair
	movlw 0xFE
	subwf	Tasto_C4, W
	bnz 	_T_Chair

	movlw	0x3A              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x3A
	goto Tasto_ready

_T_Chair
; FF FF FF FB
	movf	Tasto_C1, W		; Solo un tasto Premuto
	andwf	Tasto_C2, W
	andwf	Tasto_C3, W
	sublw 0xFF
	bnz 	_T_Flex
	movlw 0xFB
	subwf	Tasto_C4, W
	bnz 	_T_Flex

	movlw	0x3B              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x3B
	goto Tasto_ready

_T_Flex
; FF FF FF FD
	movf	Tasto_C1, W		; Solo un tasto Premuto
	andwf	Tasto_C2, W
	andwf	Tasto_C3, W
	sublw 0xFF
	bnz 	_T_SHIFT
	movlw 0xFD
	subwf	Tasto_C4, W
	bnz 	_T_SHIFT

	movlw	0x13              ; irda new
  movwf Dato_IR
  movlw MAX_CODICI_MNT    ; imposto numero codici di mantenimento da trasmettere
  movwf Max_cod_mnt
	movlw	0x3C
	goto Tasto_ready


_T_SHIFT     ; tasto Shift
; FF FF BF FF
	movf	Tasto_C1, W		; Solo un tasto Premuto
	andwf	Tasto_C2, W
	andwf	Tasto_C4, W
	sublw 0xFF
	bnz 	_T_No_Key
	movlw 0xBF
	subwf	Tasto_C3, W
	bnz 	_T_No_Key

	movlw CNT_DEBOUNCE             ; check debounce
  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
	movlw CNT_DEBOUNCE_IRDA        ; IRDA --> il giro di polling impiega pi� tempo
  subwf Cnt_debounce_shift, W
  bnc   _T_S_last                ; non ancora trascorso
  bnz   T_Shift_end              ; gi� gestito
; debouuce trascorso --> gestisco
  incf  Cnt_debounce_shift, F    ; per indicare al prossimo giro che � gi� gestito
  movf  Shift_Timer, W           ; = 0?
  bnz   Clear_shift
  movlw TIME_SHIFT               ; setto stato shift
  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  movlw TIME_SHIFT_IRDA          ; IRDA --> setto stato shift (per mainloop=95ms)
  movwf Shift_Timer
  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  T_Shift_end                ; IRDA --> tasto abilitato
  btfss Slave, 0                 ; se slave 01 --> il tempo � maggiore (serve per abilitazione tasti)
  goto  T_Shift_end
  movlw TIME_SHIFT_1             ; setto stato shift (allungato)
  movwf Shift_Timer
  goto  T_Shift_end
Clear_shift
  clrf  Shift_Timer

; uscita con tasto SHIFT premuto
T_Shift_end
	movlw TASTO_SHIFT
	movwf	Tasto
	goto  _T_End

_T_S_last
  incf  Cnt_debounce_shift, F
	goto _T_End

; uscita con nessun tasto premuto
_T_No_Key
  clrf  Cnt_debounce_shift       ; se nessun tasto azzero contatore debounce tasto shift
	goto _T_End

; uscita con tasto premuto
Tasto_ready
	movwf	Tasto
  clrf  Cnt_debounce_shift       ; se altri tasti azzero contatore debounce tasto shift
  btfsc Tlc_type, BIT_MODO_FUNZ    ; telecomando a filo/IRDA ?
  goto  _T_End                     ; IRDA --> tasto abilitato
  btfss Slave, 0                 ; se slave 01 --> il tasto � abilitato solo se led shift acceso
  goto  _T_End                   ; slave = 00 --> tasto abilitato

Tasto_slave_01
	movf	Shift_Timer, f		       ; if (Shift_Timer==0)
  bz    _T_Disab                 ; tasto disabilitato

  movlw TIME_SHIFT_1             ; retrigger timer
  movwf Shift_Timer
  goto  _T_End                   ; uscita con tasto abilitato

_T_Disab
  clrf  Tasto                    ; if == 0 --> tasto disabilitato

_T_End
	return



;******************************************************************
; Inverte l'ordine dei bit di W modifica Temp
;  Logica invia con questo ordine:
;   - bit 0 : M1
;   - bit 1 : M2
;   - bit 2 : M3
;   - bit 3 : M4
;   - bit 4 : M5
;   - bit 5 : M6
;   - bit 6 : M7
;   - bit 7 : M7b
; mentre sul micro Tlc sono associati alla porta RD al contrario (LED1 su RD7)
;******************************************************************
_Invert_W
	movwf	Temp
	clrf	Temp1
	btfsc	Temp, 0
	bsf		Temp1, 7
	btfsc	Temp, 1
	bsf		Temp1, 6
	btfsc	Temp, 2
	bsf		Temp1, 5
	btfsc	Temp, 3
	bsf		Temp1, 4
	btfsc	Temp, 4
	bsf		Temp1, 3
	btfsc	Temp, 5
	bsf		Temp1, 2
	btfsc	Temp, 6
	bsf		Temp1, 1
	btfsc	Temp, 7
	bsf		Temp1, 0
	movf	Temp1, W
	return

;******************************************************************
; Gestione LED
; si basa su Lamp_Timer che � un contatore sw freerunning
; con risoluzione 2,05ms e overflow = 256*2,05 = 525ms
; il lampeggio avviene con 262ms ON e 262ms OFF
; Input:  Led_Fissi         (0 = OFF, 1 = ON)
;         Led_Lampeggianti  (0 = OFF, 1 = LAMP)
;******************************************************************
_Led
  movf	Shift_Timer, f      ; check stato led SHIFT
  btfss STATUS, Z
  bsf   LED_SHIFT           ; se timer != 0 --> led on
  btfsc STATUS, Z
  bcf   LED_SHIFT           ; se timer == 0 --> led off

	btfss	Lamp_Timer, 7       ; MSB = 1 indica fase ON (segue il beep)
	goto	_Led_Lampeggianti

; fase ON
	movf	Led_Fissi, W        ; accendo i led fissi
	iorwf	Led_Lamp, W         ; e quelli lampeggianti in condizione on
	movwf	PORTD
	return

; fase OFF
_Led_Lampeggianti
	movlw	0xFF
	xorwf	Led_Lamp, W         ; spengo i led lampeggianti
	andwf	Led_Fissi, W        ; e tengo accesi quelli fissi
	movwf	PORTD
	return
	

;******************************************************************
; Invia un pacchetto sulla seriale
;******************************************************************
_TX_MSG
	bsf PORTC,5				; setto la 485 in trasmissione
	movf	TX_Buff1, W
	movwf	Param1
	call	_TX_Char
	
	movf	TX_Buff2, W
	movwf	Param1
	call	_TX_Char

	movf	TX_Buff3, W
	movwf	Param1
	call	_TX_Char

	movf	TX_Buff4, W
	movwf	Param1
	call	_TX_Char

	movf	TX_Buff5, W
	movwf	Param1
	call	_TX_Char

	movf	TX_Buff6, W
	movwf	Param1
	call	_TX_Char

	movf	TX_Buff7, W
	movwf	Param1
	call	_TX_Char

_TX_MSG_1	
	bsf		STATUS, RP0
	btfss	TXSTA, TRMT 		; aspetto che il buffer di trasmissione sia vuoto
	goto	_TX_MSG_1
	bcf 	STATUS, RP0

	bcf		PORTC,5				; setto la 485 in ricezione
	clrf	RX_Cnt			; RX_Cnt=0;
	clrf	RX_Buff1		; Buff1=0;
	return
	

; Trasmette il byte "Param1_Byte" sulla seriale
_TX_Char
	btfss PIR1 , TXIF 		; aspetto che il buffer di trasmissione sia vuoto
	goto _TX_Char
	
	movf Param1, W			; Byte da trasmettere
	movwf TXREG

	return

;******************************************************************
; Invia il tasto rilevato sulla seriale
;******************************************************************
_TX_Tasto
	bsf PORTC,5				; setto la 485 in trasmissione
	movf	Tasto, W
	movwf	Param1
	call	_TX_Char
_TX_T_1
	bsf		STATUS, RP0
	btfss	TXSTA, TRMT 		; aspetto che il buffer di trasmissione sia vuoto
	goto	_TX_T_1
	bcf 	STATUS, RP0

	bcf		PORTC,5				; setto la 485 in ricezione
	return


	
	
;******************************************************************
_RX_Clear					; Svuota il buffer di ricezione
	btfss PIR1, RCIF
	return
	movf RCREG,W
	goto _RX_Clear


;******************************************************************
_RX_Char
	bcf STATUS, RP0
	bcf PORTC,5				; setto la 485 in ricezione
	btfsc PIR1, RCIF
	goto	_RX_Char_OK

	btfss	RCSTA,OERR		; Se RX � in errore resetto la porta
	return
	bcf		RCSTA, CREN
	nop
	bsf		RCSTA, CREN
	return

;******************************************************************
_RX_Char_OK
	movf	RCREG,W
	movwf	Temp        ; prelevo carattere da seriale
	
	movlw	0x00				; if (RX_Clear!=0x0) goto _RX_Read;
	subwf	RX_Clear, W
	btfss	STATUS, Z
	goto	_RX_Read

	
;******************************************************************
_RX_read_0            ; primo byte del pacchetto
	clrf	RX_Cnt				; Azzero il contatore dei caratteri

_RX_Read
;	movlw	25					; RX_Clear=25; (* reimposta il ritardo per fine ricezione pacchetto = 50ms *)
	movlw	10					; RX_Clear=10; (* reimposta il ritardo per fine ricezione pacchetto = 20ms *)
	movwf	RX_Clear
	
	movlw D'5'					; if (RX_Cnt>5) goto return
	subwf RX_Cnt, W
	btfsc STATUS, C
	return
	
	movlw	RX_Buff1			; RX_Buff1[RX_Cnt]=Temp;
	addwf	RX_Cnt, W
	movwf	FSR
	movf	Temp, W
	movwf	INDF
	
	movf	Slave,W				; if (Buff1!=Slave) return;
	subwf	RX_Buff1, W
	btfss	STATUS, Z
	return				

	incf	RX_Cnt, f

	movlw D'5'					; if (RX_Cnt>5) goto _Rispondi
	subwf RX_Cnt, W
	btfsc STATUS, C
	goto _Rispondi
	
	return
	
_RX_Reset
	clrf	RX_Cnt			; RX_Cnt=0;
	clrf	RX_Buff1		; Buff1=0;
	return

	
;******************************************************************
; Analizza il pacchetto ricevuto e risponde
;******************************************************************
_Rispondi
	call	_CRC_RX
	
	clrf	RX_Clear
	clrf	RX_Cnt
	
	movf	RX_Buff4, W		; if (RX_Buff4!=CRC_HI) goto _RX_Reset
	subwf	CRC_LO, W
	btfss	STATUS, Z
	goto	_RX_Reset
	
	movf	RX_Buff5, W		; if (RX_Buff5!=CRC_LO) _RX_Reset
	subwf	CRC_HI, W
	btfss	STATUS, Z
	goto	_RX_Reset

	movlw	0x50			; if (RX_Buff2==50) goto _Richiesta_F50
	subwf	RX_Buff2, W
	btfsc	STATUS, Z
	goto	_Richiesta_F50

	movlw	0x51			; if (RX_Buff2==51) goto _Richiesta_F51
	subwf	RX_Buff2, W
	btfsc	STATUS, Z
	goto	_Richiesta_F51

	movlw	0x52			; if (RX_Buff2==52) goto _Richiesta_F52
	subwf	RX_Buff2, W
	btfsc	STATUS, Z
	goto	_Richiesta_F52

	movlw	0x53			; if (RX_Buff2==53) goto _Richiesta_F52
	subwf	RX_Buff2, W
	btfsc	STATUS, Z
	goto	_Richiesta_F53

	return

	


;******************************************************************
; Risponde a un messaggio con codice 50 (Lettura Tastiera)
;******************************************************************	
_Richiesta_F50
	movf	Slave, W
	movwf	TX_Buff1
	bsf		TX_Buff1, 7		; setta il 7� bit dell'indirizzo per indicare che � una risposta

	movlw	0x50
	movwf	TX_Buff2

	movf	Tasto, W         ; tasto pigiato
	movwf	TX_Buff3

	movf	Shift_Timer, W   ; stato led (tempo rimanente di accensione)
	movwf	TX_Buff4

	call	_CRC_TX

	goto	_TX_MSG

;******************************************************************
; Risponde a un messaggio con codice 51 (Set Led Fissi)
;******************************************************************	
_Richiesta_F51
	movf	Slave, W
	movwf	TX_Buff1
	bsf		TX_Buff1, 7		; setta il 7� bit dell'indirizzo per indicare che � una risposta

	movlw	0x51
	movwf	TX_Buff2
	movf	Tasto, W
	movwf	TX_Buff3
	movf	Shift_Timer, W   ; stato led shift (tempo rimanente di accensione)
	movwf	TX_Buff4

	movf	RX_Buff3, W
	call	_Invert_W
	movwf	Led_Fissi

	call	_CRC_TX

	goto	_TX_MSG

;******************************************************************
; Risponde a un messaggio con codice 52 (Set Led Lampeggianti)
;******************************************************************	
_Richiesta_F52
	movf	Slave, W
	movwf	TX_Buff1
	bsf		TX_Buff1, 7		; setta il 7� bit dell'indirizzo per indicare che � una risposta

	movlw	0x52
	movwf	TX_Buff2
	movf	Tasto, W
	movwf	TX_Buff3
	movf	Shift_Timer, W   ; stato led shift (tempo rimanente di accensione)
	movwf	TX_Buff4

	movf	RX_Buff3, W
	call	_Invert_W
	movwf	Led_Lamp

	call	_CRC_TX

	goto	_TX_MSG
	
;******************************************************************
; Risponde a un messaggio con codice 53 (Beep)
;******************************************************************	
_Richiesta_F53
	movf	Slave, W
	movwf	TX_Buff1
	bsf		TX_Buff1, 7		; setta il 7� bit dell'indirizzo per indicare che � una risposta

	movlw	0x53
	movwf	TX_Buff2

	movf	Tasto, W
	movwf	TX_Buff3
	movf	Shift_Timer, W   ; stato led (tempo rimanente di accensione)
	movwf	TX_Buff4

	movf	RX_Buff3, W
	addwf	Beep, f

	movlw	0x07
	andwf	Beep, f		;Limito a 7 il numero massimo di beep

	call	_CRC_TX

	goto	_TX_MSG

;******************************************************************
; calcola il CRC del buffer ricevuto
;******************************************************************
_CRC_RX	
	movlw	0xFF
	movwf	CRC_HI			; CRC=0xFFFF;
	movwf	CRC_LO

	movf	RX_Buff1, W		; Param1=Buff1
	movwf	Param1
	call	_CRC_Add
	
	movf	RX_Buff2, W		; Param1=Buff2
	movwf	Param1
	call	_CRC_Add
	
	movf	RX_Buff3, W		; Param1=Buff3
	movwf	Param1
	call	_CRC_Add
	return

;******************************************************************
; calcola il CRC del buffer da trasmettere
;******************************************************************
_CRC_TX	
	movlw	0xFF
	movwf	CRC_HI			; CRC=0xFFFF;
	movwf	CRC_LO

	movf	TX_Buff1, W		; Param1=Buff1
	movwf	Param1
	call	_CRC_Add
	
	movf	TX_Buff2, W		; Param1=Buff2
	movwf	Param1
	call	_CRC_Add
	
	movf	TX_Buff3, W		; Param1=Buff3
	movwf	Param1
	call	_CRC_Add

	movf	TX_Buff4, W		; Param1=Buff3
	movwf	Param1
	call	_CRC_Add

	movf	TX_Buff5, W		; Param1=Buff3
	movwf	Param1
	call	_CRC_Add
	
	movf	CRC_LO, W
	movwf	TX_Buff6
	movf	CRC_HI, W
	movwf	TX_Buff7

	return


;******************************************************************
; Aggiunge il byte contenuto in Param1 al CRC
;******************************************************************
_CRC_Add
	movf	Param1, W		; CRC=CRC xor Param1;
	xorwf	CRC_LO, f
	
	movlw	8				; Temp=7
	movwf	Temp
	
_CRC_Add_1	
	bcf		STATUS, C		; Clear Carry
	rrf		CRC_HI, f
	rrf		CRC_LO, f
	btfss	STATUS, C		; if (!Carry) goto _CRC_Add_2
	goto	_CRC_Add_2

	movlw	0xA0		; W=0xA0; (* POLY_HI *)
	xorwf	CRC_HI, f
	movlw	0x01		; W=0x01; (* POLY_LO *)
	xorwf	CRC_LO, f

_CRC_Add_2	
	decfsz	Temp, f			; if (Temp-- >0) goto __CRC_Add_1
	goto	_CRC_Add_1
	
	return
	
;******************************************************************
; Trasmette il Tasto su Infrarosso
;******************************************************************
_TX_IR
  movf  Tasto, W
  bz    Solo_ritardo        ; se tasto non premuto implementa solo il ritardo pari alla trasmissione (circa 96ms)
  sublw TASTO_SHIFT         ; se tasto shift non devo trasmettere nulla
  bz    Solo_ritardo        ; se tasto non premuto implementa solo il ritardo pari alla trasmissione (circa 96ms)

; codifica e trasmette il tasto (Addr_IR + Dato_IR)
  movf  Cnt_mnt, F   ; se =0 --> trasmette tasto vero, altrimenti cod. mantenimento
  bz    _TX_IR_inc_cnt
  movlw COD_MANTENIMENTO     ; se != 0 --> tx cod. mantenimento
  movwf Dato_IR

_TX_IR_inc_cnt
  incf  Cnt_mnt, F   ; aggiorno contatore
  movf  Cnt_mnt, W
  subwf Max_cod_mnt, W  ; confronto con numero cod. mnt da trasmettere
  bc    _TX_IR_addr  ; ancora da trasmettere
  clrf  Cnt_mnt      ; terminati --> al prox. giro trasmetto nuovamente il tasto vero

_TX_IR_addr
; Addr
  movlw .3
  movwf Cnt_bit_IR   ; 5 bit di address

  movf  Addr_IR, W   ; indirizzo da trasmettere
  movwf Tx_IR

_IR_Tx_addr
  btfsc Tx_IR, 0     ; controllo bit da trasmettere
  goto  _TX_IR_bit_1 ; "1" da trasmettere

; trasmissione 0
_TX_IR_bit_0
  call  _IR_TX_0
  goto  _IR_next_bit

; trasmissione 1
_TX_IR_bit_1
  call  _IR_TX_1

_IR_next_bit
  rrf   Tx_IR, f     ; prossimo bit in posizione zero
  decfsz Cnt_bit_IR, f
  goto  _IR_Tx_addr

; Dato
  movlw .6
  movwf Cnt_bit_IR   ; 5 bit di address

  movf  Dato_IR, W   ; indirizzo da trasmettere
  movwf Tx_IR

_IR_Tx_dato
  btfsc Tx_IR, 0     ; controllo bit da trasmettere
  goto  _TX_IR_bit_1d ; "1" da trasmettere

; trasmissione 0
_TX_IR_bit_0d
  call  _IR_TX_0
  goto  _IR_next_bitd

; trasmissione 1
_TX_IR_bit_1d
  call  _IR_TX_1

_IR_next_bitd
  rrf   Tx_IR, f     ; prossimo bit in posizione zero
  decfsz Cnt_bit_IR, f
  goto  _IR_Tx_dato

;  movlw DURATA_PAUSA_IR ; 24ms = durata pausa
  movlw .48             ; 24ms = durata pausa
  goto  _TX_IR_pausa    ; pausa intertasto


Solo_ritardo         ; implementa un ritardo pari alla durata di trasmissione IR (in caso di non trasmissione)
  clrf  Cnt_mnt      ; al prox. tasto trasmetto nuovamente il tasto vero

;  bsf   LED_IR       ; !!! per monitor tempo
;  movlw DURATA_TX_IR ; 72ms+24ms = durata trasmissione codice + pausa
  movlw .192         ; 72ms+24ms = durata trasmissione codice + pausa
_TX_IR_pausa
  movwf Temp1
Delay_ir_0
  movlw .200         ; loop interno a 500us
  movwf Temp
Delay_ir_1
  nop
  nop
  decfsz Temp, f
  goto Delay_ir_1

;  movlw  0x80
;  xorwf  PORTD, F

  decfsz Temp1, f
  goto Delay_ir_0
;  bcf   LED_IR       ; !!! per monitor tempo

_TX_IR_end
	return


;*************************************************************************
; trasmissione "0"
; nr. 2 sequenze di treno 38Khz (0,6ms) + pausa (3,4ms)
;*************************************************************************
_IR_TX_0

  movlw .2
  movwf Temp         ; numero ripetizioni sequenza

; impulso con 38KHz on
IR_TX_0c
  movlw .23          ; 23 * 26.3us = 0,6m
  movwf Temp1        ; numero periodi interi a 38KHz da trasmettere

IR_TX_0a
  bcf   LED_IR       ; semiperiodo a zero
  call  Wait_semiper_38KHz
  nop
  nop
  bsf   LED_IR       ; semiperiodo a uno
  call  Wait_semiper_38KHz

  decfsz Temp1, f
  goto  IR_TX_0a

; impulso con 38KHz off
  movlw .130         ; 130 * 26.3us = 3,4ms
  movwf Temp1        ; numero periodi interi a 38KHz da rimanere muto

IR_TX_0b
  bcf   LED_IR       ; semiperiodo a zero
  call  Wait_semiper_38KHz
  nop
  nop
  bcf   LED_IR       ; semiperiodo a zero
  call  Wait_semiper_38KHz

  decfsz Temp1, f
  goto  IR_TX_0b

  decfsz Temp, f
  goto  IR_TX_0c

	return

;*************************************************************************
; trasmissione "1"
; nr. 2 sequenze di treno 38Khz (3,4ms) + pausa (0,6ms)
;*************************************************************************
_IR_TX_1

  movlw .2
  movwf Temp         ; numero ripetizioni sequenza

; impulso con 38KHz on
IR_TX_1c
  movlw .130         ; 130 * 26.3us = 3,4ms
  movwf Temp1        ; numero periodi interi a 38KHz da trasmettere

IR_TX_1a
  bcf   LED_IR       ; semiperiodo a zero
  call  Wait_semiper_38KHz
  nop
  nop
  bsf   LED_IR       ; semiperiodo a uno
  call  Wait_semiper_38KHz

  decfsz Temp1, f
  goto  IR_TX_1a

; impulso con 38KHz off
  movlw .23          ; 23 * 26.3us = 0,6m
  movwf Temp1        ; numero periodi interi a 38KHz da rimanere muto

IR_TX_1b
  bcf   LED_IR       ; semiperiodo a zero
  call  Wait_semiper_38KHz
  nop
  nop
  bcf   LED_IR       ; semiperiodo a zero
  call  Wait_semiper_38KHz

  decfsz Temp1, f
  goto  IR_TX_1b

  decfsz Temp, f
  goto  IR_TX_1c

	return

;*************************************************************************
; Wait_semiper_38KHz
;*************************************************************************
Wait_semiper_38KHz

  movlw .6
  movwf del38
Wait38a
  decfsz del38, f
  goto  Wait38a
	return

;*************************************************************************
; Ritardo di Temp * 0.5 micro secondi
;*************************************************************************
_Ritardo	
	decf	Temp,f
	nop
	nop
	nop
	nop
	btfss	STATUS, Z
	goto	_Ritardo
	return

;*************************************************************************
;*************************************************************************
; Gestione interrupt
;*************************************************************************
_interrupt

	MOVWF W_TEMP ;Copy W to TEMP register
	SWAPF STATUS,W ;Swap status to be saved into W
	CLRF STATUS ;bank 0, regardless of current bank, Clears IRP,RP1,RP0
	MOVWF STATUS_TEMP ;Save status to bank zero STATUS_TEMP register
	MOVF PCLATH, W ;Only required if using pages 1, 2 and/or 3
	MOVWF PCLATH_TEMP ;Save PCLATH into W
	CLRF PCLATH ;Page zero, regardless of current page

	;---------------------------------------------
	;(Insert user code here)
	btfsc	PIR1, TMR1IF	; Timer1
	goto	_INT_Timre1
	btfsc	PIR1, TMR2IF	; Timer2
	goto	_INT_Timre2

	;---------------------------------------------
	
_End_Interrupt	
	MOVF PCLATH_TEMP, W ;Restore PCLATH
	MOVWF PCLATH ;Move W into PCLATH
	SWAPF STATUS_TEMP,W ;Swap STATUS_TEMP register into W
	;(sets bank to original state)
	MOVWF STATUS ;Move W into STATUS register
	SWAPF W_TEMP,F ;Swap W_TEMP
	SWAPF W_TEMP,W ;Swap W_TEMP into W
_End_Int
	retfie

;*************************************************************************
; Interrupt base tempi 2,05ms
;*************************************************************************
_INT_Timre1
	banksel	PORTA
; controllo timeout ricezione
	movlw	0				; if (RX_Clear>0) dec(RX_Clear);
	subwf	RX_Clear, W
	btfss	STATUS, Z
	decf	RX_Clear, f
	
; gestione beep/lampeggio
; Lamp_Timer � un contatore software free running con risoluzione 2,05ms
; e overflow = 256*2,05 = 525ms
; se = 128 (262ms) --> start beep (se richiesto)
; se = 0           --> stop beep
; --> il beep ha una durata di 262ms ON e 262ms OFF

	incf	Lamp_Timer, f		; incremento il timer per lampeggio led

							; Gestione dei Beep
	movf	Beep, f			      ; if (Beep==0) goto _Beep_OFF
	btfsc	STATUS, Z	
	goto	_Beep_OFF
	
	movlw	0x80			        ; if (Lamp_Timer==0x80) Beep->ON
	subwf	Lamp_Timer, W
	btfsc	STATUS, Z	
	bsf		BUZZER  	        ; genero un beep
	btfsc	STATUS, Z	
	decf	Beep, f           ; e decremento numero beep

_Beep_OFF
	movf	Lamp_Timer, f			; if (Lamp_Timer==0) Beep->OFF
	btfss	STATUS, Z
  goto  _End_Beep         ; not zero
  bcf		BUZZER            ; stop beep
	movf	Shift_Timer, f		; if (Shift_Timer!=0) Shift_Timer--
	btfss	STATUS, Z
  decf  Shift_Timer, f

_End_Beep

_End_Int_Timer1

; restart base tempi = 2,05ms
  movlw	0xF0			; TMR1H=0xF0   (reimposto il contatore del timer1)
	movwf	TMR1H
	bcf		PIR1, TMR1IF	; riattiva l'interrupt

; debug per misura cadenza interrupt
;  movlw 0x02
;  xorwf PORTC         ; toggle su LSH (RC1)
; end debug

	goto	_End_Interrupt


_INT_Timre2
	bcf		PIR1, TMR2IF	; riattiva l'interrupt
	goto	_End_Interrupt

	END
