/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */




/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
/*ERROR LIST*/
#define SYSTEM_DMA_INIT_ERROR                                      0x0000
#define SYSTEM_RCC_CONFIG_ERROR                                    0x0001
#define SYSTEM_RCC_OVER_DRIVE_ERROR                                0x0002
#define SYSTEM_RCC_CLK_CONFIG_ERROR                                0x0003
#define SYSTEM_RCC_PERIPH_CLK_CONFIG_ERROR                         0x0004


#define SYSTEM_UART1_CONFIG_ERROR                                0x0004
#define SYSTEM_UART2_CONFIG_ERROR                                0x0005
#define SYSTEM_UART3_CONFIG_ERROR                                0x0006
#define SYSTEM_UART4_CONFIG_ERROR                                0x0007
#define SYSTEM_UART5_CONFIG_ERROR                                0x0008
#define SYSTEM_UART6_CONFIG_ERROR                                0x0009


#define SYSTEM_DMA_UART1_CONFIG_ERROR                                0x000A
#define SYSTEM_DMA_UART2_CONFIG_ERROR                                0x000B
#define SYSTEM_DMA_UART3_CONFIG_ERROR                                0x000C
#define SYSTEM_DMA_UART4_CONFIG_ERROR                                0x000D
#define SYSTEM_DMA_UART5_CONFIG_ERROR                                0x000E
#define SYSTEM_DMA_UART6_CONFIG_ERROR                                0x000F


#define SYSTEM_ADC_CONFIG_1_ERROR                                     0x0010
#define SYSTEM_ADC_CONFIG_2_ERROR                                     0x0011
#define SYSTEM_ADC_CONFIG_3_ERROR                                     0x0012
#define SYSTEM_ADC_CONFIG_4_ERROR                                     0x0013
#define SYSTEM_ADC_CONFIG_5_ERROR                                     0x0014
#define SYSTEM_ADC_CONFIG_6_ERROR                                     0x0015
#define SYSTEM_ADC_CONFIG_7_ERROR                                     0x0016
#define SYSTEM_ADC_CONFIG_8_ERROR                                     0x0017
#define SYSTEM_ADC_CONFIG_9_ERROR                                     0x0018
#define SYSTEM_ADC_CONFIG_10_ERROR                                     0x0019

#define SYSTEM_CRC_CONFIG_1_ERROR                                     0x0020

#define SYSTEM_I2C_CONFIG_1_ERROR                                     0x0030
#define SYSTEM_I2C_CONFIG_2_ERROR                                     0x0031
#define SYSTEM_I2C_CONFIG_3_ERROR                                     0x0032
#define SYSTEM_I2C_CONFIG_4_ERROR                                     0x0033
#define SYSTEM_I2C_CONFIG_5_ERROR                                     0x0034
#define SYSTEM_I2C_CONFIG_6_ERROR                                     0x0035
#define SYSTEM_I2C_CONFIG_7_ERROR                                     0x0036
#define SYSTEM_I2C_CONFIG_8_ERROR                                     0x0037
#define SYSTEM_I2C_CONFIG_9_ERROR                                     0x0038
#define SYSTEM_I2C_CONFIG_10_ERROR                                     0x0039
#define SYSTEM_I2C_CONFIG_11_ERROR                                     0x003A
#define SYSTEM_I2C_CONFIG_12_ERROR                                     0x003B


#define SYSTEM_IWDG_CONFIG_1_ERROR                                    0x0040
#define SYSTEM_IWDG_CONFIG_2_ERROR                                    0x0041

#define SYSTEM_RTC_CONFIG_1_ERROR                                     0x0050
#define SYSTEM_RTC_CONFIG_2_ERROR                                     0x0051
#define SYSTEM_RTC_CONFIG_3_ERROR                                     0x0052

#define SYSTEM_SPI_CONFIG_1_ERROR                                     0x0060
#define SYSTEM_SPI_CONFIG_2_ERROR                                     0x0061
#define SYSTEM_SPI_CONFIG_3_ERROR                                     0x0062

#define SYSTEM_TIM_CONFIG_1_ERROR                                     0x0070
#define SYSTEM_TIM_CONFIG_2_ERROR                                     0x0071
#define SYSTEM_TIM_CONFIG_3_ERROR                                     0x0072
#define SYSTEM_TIM_CONFIG_4_ERROR                                     0x0073
#define SYSTEM_TIM_CONFIG_5_ERROR                                     0x0074
#define SYSTEM_TIM_CONFIG_6_ERROR                                     0x0075
#define SYSTEM_TIM_CONFIG_7_ERROR                                     0x0076
#define SYSTEM_TIM_CONFIG_8_ERROR                                     0x0077
#define SYSTEM_TIM_CONFIG_9_ERROR                                     0x0078
#define SYSTEM_TIM_CONFIG_10_ERROR                                    0x0079
#define SYSTEM_TIM_CONFIG_11_ERROR                                    0x007A
#define SYSTEM_TIM_CONFIG_12_ERROR                                    0x007B
#define SYSTEM_TIM_CONFIG_13_ERROR                                    0x007C
#define SYSTEM_TIM_CONFIG_14_ERROR                                     0x007E
#define SYSTEM_TIM_CONFIG_15_ERROR                                     0x007F
#define SYSTEM_TIM_CONFIG_16_ERROR                                     0x0080
#define SYSTEM_TIM_CONFIG_17_ERROR                                     0x0081
#define SYSTEM_TIM_CONFIG_18_ERROR                                     0x0082
#define SYSTEM_TIM_CONFIG_19_ERROR                                     0x0083
#define SYSTEM_TIM_CONFIG_20_ERROR                                     0x0084
#define SYSTEM_TIM_CONFIG_21_ERROR                                     0x0085
#define SYSTEM_TIM_CONFIG_22_ERROR                                     0x0086
#define SYSTEM_TIM_CONFIG_23_ERROR                                     0x0087
#define SYSTEM_TIM_CONFIG_24_ERROR                                     0x0088
#define SYSTEM_TIM_CONFIG_25_ERROR                                    0x0089
#define SYSTEM_TIM_CONFIG_26_ERROR                                    0x008A
#define SYSTEM_TIM_CONFIG_27_ERROR                                    0x008B
#define SYSTEM_TIM_CONFIG_28_ERROR                                    0x008C
#define SYSTEM_TIM_CONFIG_29_ERROR                                    0x008D
#define SYSTEM_TIM_CONFIG_30_ERROR                                    0x008E
#define SYSTEM_TIM_CONFIG_31_ERROR                                    0x008F

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SENSOR_RHT_INT_ADR 0x4A
#define SENR_RHT_EXT_ADR 0x4B
#define SPI4_SCK_AUX1_Pin GPIO_PIN_2
#define SPI4_SCK_AUX1_GPIO_Port GPIOE
#define SPI4_NSS2_AUX1_Pin GPIO_PIN_3
#define SPI4_NSS2_AUX1_GPIO_Port GPIOE
#define SPI4_NSS1_AUX1_Pin GPIO_PIN_4
#define SPI4_NSS1_AUX1_GPIO_Port GPIOE
#define SPI4_MISO_AUX1_Pin GPIO_PIN_5
#define SPI4_MISO_AUX1_GPIO_Port GPIOE
#define SPI4_MOSI_AUX1_Pin GPIO_PIN_6
#define SPI4_MOSI_AUX1_GPIO_Port GPIOE
#define SPI4_NSS3_AUX1_Pin GPIO_PIN_13
#define SPI4_NSS3_AUX1_GPIO_Port GPIOC
#define I2C2_SDA_EEPROM_Pin GPIO_PIN_0
#define I2C2_SDA_EEPROM_GPIO_Port GPIOF
#define I2C2_SCL_EEPROM_Pin GPIO_PIN_1
#define I2C2_SCL_EEPROM_GPIO_Port GPIOF
#define BTN01_UP_Pin GPIO_PIN_2
#define BTN01_UP_GPIO_Port GPIOF
#define BTN02_DOWN_Pin GPIO_PIN_3
#define BTN02_DOWN_GPIO_Port GPIOF
#define BTN03_REV_Pin GPIO_PIN_4
#define BTN03_REV_GPIO_Port GPIOF
#define LED7_SERVICE_Pin GPIO_PIN_5
#define LED7_SERVICE_GPIO_Port GPIOF
#define BTN04_TREND_Pin GPIO_PIN_6
#define BTN04_TREND_GPIO_Port GPIOF
#define BTN05_TILT_L_Pin GPIO_PIN_10
#define BTN05_TILT_L_GPIO_Port GPIOF
#define ADC1_AN10_Pin GPIO_PIN_0
#define ADC1_AN10_GPIO_Port GPIOC
#define BTN06_TILT_R_Pin GPIO_PIN_1
#define BTN06_TILT_R_GPIO_Port GPIOC
#define BTN07_TRASL_F_Pin GPIO_PIN_2
#define BTN07_TRASL_F_GPIO_Port GPIOC
#define ADC1_AN13_NTC_Pin GPIO_PIN_3
#define ADC1_AN13_NTC_GPIO_Port GPIOC
#define BTN08_TRASL_R_Pin GPIO_PIN_0
#define BTN08_TRASL_R_GPIO_Port GPIOA
#define RS485_AUX1_RTS_Pin GPIO_PIN_1
#define RS485_AUX1_RTS_GPIO_Port GPIOA
#define RS485_AUX1_TX_Pin GPIO_PIN_2
#define RS485_AUX1_TX_GPIO_Port GPIOA
#define RS485_AUX1_RX_Pin GPIO_PIN_3
#define RS485_AUX1_RX_GPIO_Port GPIOA
#define BTN09_BACKUP_Pin GPIO_PIN_4
#define BTN09_BACKUP_GPIO_Port GPIOA
#define BTN10_BACKDOWN_Pin GPIO_PIN_5
#define BTN10_BACKDOWN_GPIO_Port GPIOA
#define BTN11_THOR_UP_Pin GPIO_PIN_6
#define BTN11_THOR_UP_GPIO_Port GPIOA
#define BTN12_THOR_DOWN_Pin GPIO_PIN_7
#define BTN12_THOR_DOWN_GPIO_Port GPIOA
#define BTN13_LEG_UP_Pin GPIO_PIN_4
#define BTN13_LEG_UP_GPIO_Port GPIOC
#define BTN14_LEG_DOWN_Pin GPIO_PIN_5
#define BTN14_LEG_DOWN_GPIO_Port GPIOC
#define LED6_SERVICE_Pin GPIO_PIN_0
#define LED6_SERVICE_GPIO_Port GPIOB
#define LED34_LEV2_BAT_TIM3_4_Pin GPIO_PIN_1
#define LED34_LEV2_BAT_TIM3_4_GPIO_Port GPIOB
#define BTN16_LEG_R_Pin GPIO_PIN_2
#define BTN16_LEG_R_GPIO_Port GPIOB
#define BTN17_REFLEX_Pin GPIO_PIN_11
#define BTN17_REFLEX_GPIO_Port GPIOF
#define BTN18_CHAIR_Pin GPIO_PIN_12
#define BTN18_CHAIR_GPIO_Port GPIOF
#define BTN19_FLEX_Pin GPIO_PIN_13
#define BTN19_FLEX_GPIO_Port GPIOF
#define BTN20_LEVEL_Pin GPIO_PIN_14
#define BTN20_LEVEL_GPIO_Port GPIOF
#define BTN23_STORE_Pin GPIO_PIN_15
#define BTN23_STORE_GPIO_Port GPIOF
#define BTN24_M1M4_Pin GPIO_PIN_0
#define BTN24_M1M4_GPIO_Port GPIOG
#define BTN25_RECALL_Pin GPIO_PIN_1
#define BTN25_RECALL_GPIO_Port GPIOG
#define BTN26_M2M5_Pin GPIO_PIN_7
#define BTN26_M2M5_GPIO_Port GPIOE
#define LED5_SERVICE_Pin GPIO_PIN_8
#define LED5_SERVICE_GPIO_Port GPIOE
#define LED35_LEV3_BAT_TIM1_1_Pin GPIO_PIN_9
#define LED35_LEV3_BAT_TIM1_1_GPIO_Port GPIOE
#define LED9_SERVICE_Pin GPIO_PIN_10
#define LED9_SERVICE_GPIO_Port GPIOE
#define LED36_LEV4_BAT_TIM1_2_Pin GPIO_PIN_11
#define LED36_LEV4_BAT_TIM1_2_GPIO_Port GPIOE
#define BTN28_M3M6_Pin GPIO_PIN_12
#define BTN28_M3M6_GPIO_Port GPIOE
#define LED33_LEV1_BAT_TIM1_3_Pin GPIO_PIN_13
#define LED33_LEV1_BAT_TIM1_3_GPIO_Port GPIOE
#define USB_VBUS_EN_Pin GPIO_PIN_14
#define USB_VBUS_EN_GPIO_Port GPIOE
#define USB_OVERCURRENT_ALARM_Pin GPIO_PIN_15
#define USB_OVERCURRENT_ALARM_GPIO_Port GPIOE
#define RS232_DBG_TX_Pin GPIO_PIN_10
#define RS232_DBG_TX_GPIO_Port GPIOB
#define RS232_DBG_RX_Pin GPIO_PIN_11
#define RS232_DBG_RX_GPIO_Port GPIOB
#define BTN21_POWER_Pin GPIO_PIN_8
#define BTN21_POWER_GPIO_Port GPIOD
#define BTN27_SHIFT_Pin GPIO_PIN_9
#define BTN27_SHIFT_GPIO_Port GPIOD
#define BTN15_LEG_L_Pin GPIO_PIN_10
#define BTN15_LEG_L_GPIO_Port GPIOD
#define RS232_DBG_CTS_Pin GPIO_PIN_11
#define RS232_DBG_CTS_GPIO_Port GPIOD
#define RS232_DBG_RTS_Pin GPIO_PIN_12
#define RS232_DBG_RTS_GPIO_Port GPIOD
#define PWM4_TIM4_2_Pin GPIO_PIN_13
#define PWM4_TIM4_2_GPIO_Port GPIOD
#define BTN_GP4_Pin GPIO_PIN_14
#define BTN_GP4_GPIO_Port GPIOD
#define BTN_GP3_Pin GPIO_PIN_15
#define BTN_GP3_GPIO_Port GPIOD
#define BTN_GP2_Pin GPIO_PIN_2
#define BTN_GP2_GPIO_Port GPIOG
#define BTN_GP1_Pin GPIO_PIN_3
#define BTN_GP1_GPIO_Port GPIOG
#define BTN32_OPT4_Pin GPIO_PIN_4
#define BTN32_OPT4_GPIO_Port GPIOG
#define BTN31_OPT3_Pin GPIO_PIN_5
#define BTN31_OPT3_GPIO_Port GPIOG
#define BTN30_OPT2_Pin GPIO_PIN_6
#define BTN30_OPT2_GPIO_Port GPIOG
#define BTN29_OPT1_Pin GPIO_PIN_7
#define BTN29_OPT1_GPIO_Port GPIOG
#define RS485_CONTROL_RTS_Pin GPIO_PIN_8
#define RS485_CONTROL_RTS_GPIO_Port GPIOG
#define RS485_CONTROL_TX_Pin GPIO_PIN_6
#define RS485_CONTROL_TX_GPIO_Port GPIOC
#define RS485_CONTROL_RX_Pin GPIO_PIN_7
#define RS485_CONTROL_RX_GPIO_Port GPIOC
#define PWM3_TIM3_3_Pin GPIO_PIN_8
#define PWM3_TIM3_3_GPIO_Port GPIOC
#define I2C3_SDA_PCA9685_Pin GPIO_PIN_9
#define I2C3_SDA_PCA9685_GPIO_Port GPIOC
#define I2C3_SCL_PCA9685_Pin GPIO_PIN_8
#define I2C3_SCL_PCA9685_GPIO_Port GPIOA
#define USB_OTG_HS_VBUS_Pin GPIO_PIN_9
#define USB_OTG_HS_VBUS_GPIO_Port GPIOA
#define LED8_SERVICE_Pin GPIO_PIN_5
#define LED8_SERVICE_GPIO_Port GPIOD
#define RS485_CONTROL_CTS_Pin GPIO_PIN_13
#define RS485_CONTROL_CTS_GPIO_Port GPIOG
#define LED2_SERVICE_Pin GPIO_PIN_15
#define LED2_SERVICE_GPIO_Port GPIOG
#define PWM1_TIM3_2_Pin GPIO_PIN_5
#define PWM1_TIM3_2_GPIO_Port GPIOB
#define I2C1_SCL_SENSOR_Pin GPIO_PIN_6
#define I2C1_SCL_SENSOR_GPIO_Port GPIOB
#define I2C1_SDA_SENSOR_Pin GPIO_PIN_7
#define I2C1_SDA_SENSOR_GPIO_Port GPIOB
#define LED1_SERVICE_Pin GPIO_PIN_8
#define LED1_SERVICE_GPIO_Port GPIOB
#define PWM2_TIM4_4_Pin GPIO_PIN_9
#define PWM2_TIM4_4_GPIO_Port GPIOB
#define LED3_SERVICE_Pin GPIO_PIN_0
#define LED3_SERVICE_GPIO_Port GPIOE
#define LED4_SERVICE_Pin GPIO_PIN_1
#define LED4_SERVICE_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */
#define PWM_FAN1_CHANNEL                        0x00000000U                          /*!< Capture/compare channel 1 identifier      */
#define PWM_FAN2_CHANNEL                        0x00000004U
#define PWM_LED6_CHANNEL                        0x00000004U                          /*!< Capture/compare channel 2 identifier      */
#define PWM_DOSER1_CHANNEL                      0x00000004U
#define PWM_DOSER2_CHANNEL                      0x00000008U
#define PWM_DOSER3_CHANNEL                      0x0000000CU
#define PWM_ILLUMINA_CHANNEL                    0x00000004U
#define PWM_VIBRO_CHANNEL                       0x00000008U

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
