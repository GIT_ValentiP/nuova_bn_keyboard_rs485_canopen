/*
 ******************************************************************************
 *  @file      : globals.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 16 set 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_GLOBALS_H_
#define INC_GLOBALS_H_


#define FW_VERSION          0u
#define FW_SUBVERSION      30u



#define __HBM_AD105x //BALANCE HBM AD105x

/* Includes ----------------------------------------------------------*/
#include "main.h"
/* Typedef -----------------------------------------------------------*/

// types
typedef struct system_timer_s
{
	double tick_1ms;
	double tick_10ms;              //
    double tick_100ms;            //
    double seconds;               //
    double mins;                  //
    double hours;                 //
} sys_timer_t;

//
typedef union{
	struct bit_s{
			uint16_t b0:1;
			uint16_t b1:1;
			uint16_t b2:1;
			uint16_t b3:1;
			uint16_t b4:1;
			uint16_t b5:1;
			uint16_t b6:1;
			uint16_t b7:1;
			uint16_t b8:1;
			uint16_t b9:1;
			uint16_t b10:1;
			uint16_t b11:1;
			uint16_t b12:1;
			uint16_t b13:1;
			uint16_t b14:1;
			uint16_t b15:1;
	}bit;

	    uint16_t word16;
}word_bit_t;

typedef union{
	struct byte_bit_s{
			uint8_t b0:1;
			uint8_t b1:1;
			uint8_t b2:1;
			uint8_t b3:1;
			uint8_t b4:1;
			uint8_t b5:1;
			uint8_t b6:1;
			uint8_t b7:1;
	}bit;
	    uint8_t bytes;
}byte_bit_t;

typedef union{
	struct cat{
			uint16_t communication:1;
			uint16_t load_cell:1;
			uint16_t motor:1;
			uint16_t sensor:1;
			uint16_t workflow:1;
			uint16_t general:1;
			uint16_t empty6:1;
			uint16_t empty7:1;
	}cat;

	    uint8_t err_cat_byte;
}error_cat_t;


typedef union{
	struct flag_s{
			uint16_t selftest:1;
			uint16_t b1:1;
			uint16_t b2:1;
			uint16_t b3:1;
			uint16_t b4:1;
			uint16_t b5:1;
			uint16_t b6:1;
			uint16_t b7:1;
			uint16_t b8:1;
			uint16_t b9:1;
			uint16_t b10:1;
			uint16_t b11:1;
			uint16_t b12:1;
			uint16_t b13:1;
			uint16_t b14:1;
			uint16_t b15:1;
	}flag;

	    uint16_t all;
}service_bit_t;


typedef union{
	struct{
			uint8_t status:4;
			uint8_t nblink:4;
	}part;

	    uint8_t all;
}led_tasto_cmd_t;

typedef union{
	struct{
			uint8_t led_25:1;
			uint8_t led_50:1;
			uint8_t led_75:1;
		    uint8_t led_100:1;
			uint8_t led_free5:1;
			uint8_t led_free6:1;
			uint8_t led_free7:1;
			uint8_t charge:1;
	}led;

	    uint8_t all;
}led_bat_cmd_t;


typedef enum {
			NOTHING_TO_READ,
			FW_REL_READ,
			REVO_STATUS_READ
}RS485_PARAM_READ_enum_t;


typedef enum {
			NTC_ADC1_CH13 =0,
			VREF_ADC1_CH17=1,
			ENC_ADC1_CH10 =2,
			//GENERIC_ADC3_CH15=3,
			NUM_MAX_ADC_CH=3,
}adc_ch_enum_t;

typedef enum {
			INT_TEMP_SENS =0,
			EXT_TEMP_SENS =1,
			NTC_TEMP_SENS =2,
			NUM_MAX_TEMP_SENSOR=3,
}temperature_sensor_enum_t;





typedef enum {
			ERROR_NONE             =0x0000,
			ERROR_COMMUNICATION    =0x0100,
			ERROR_LOADCELL         =0x0200,
			ERROR_MOTOR            =0x0400,
			ERROR_SENSOR           =0x0800,
			ERROR_WORKFLOW         =0x1000,
			ERROR_GENERAL          =0x2000,//not completed
}REVO_ERROR_enum_t;



typedef struct {
	uint8_t  type_sens;
	int16_t  temperature;
	float temperature_degree;
	float humidity_real;
	uint16_t humidity;
	uint16_t error;
}REVO_temp_sensor;

typedef struct {
	ADC_TypeDef *ADCIstance;
	uint32_t  ADC_ch;
	uint32_t  id_rank;
	uint32_t  sampling_time;
	uint16_t num_sample;
	uint32_t sample_raw_sum;
	uint16_t raw_value;
	float    filtered_value;
	uint32_t  millivolt;
	uint16_t error;
}REVO_ADC_ch;





/**
 * Structure defining a handle describing a PCA9685 device.
 */
typedef struct {

	/**
	 * The handle to the I2C bus for the device.
	 */
	I2C_HandleTypeDef *i2c_handle;

	/**
	 * The I2C device address.
	 * @see{PCA9865_I2C_DEFAULT_DEVICE_ADDRESS}
	 */
	uint16_t device_address;

	/**
	 * Set to true to drive inverted.
	 */
	bool inverted;

} pca9685_handle_t;


/*Keyboard*/
#define NUM_MAX_LEDS          32
#define BLINK_LED_TIMING       4
#define TOGGLE_PIN_LED_SLOW      30 //period 1 toggle blinking led in 100ms
#define TOGGLE_PIN_LED_MEDIUM    10 //period 1 toggle blinking led in 100ms
#define TOGGLE_PIN_LED_FAST       5 //period 1 toggle blinking led in 100ms
#define TOGGLE_PIN_LED_VERY_FAST  2 //period 1 toggle blinking led in 100ms

#define GOTO_SLEEP_TIME        120//seconds
#define LED_CHARGE_ID            21//22
#define LED_LOGO_ID              21//0
#define LED_POWER_ID             20
#define KEY_SHIFT_POWER_LED_ID   20//0
#define KEY_SHIFT_ID             26//0
                                                   //  21098765432109876543210987654321
#define NBN_MASK_ENABLED_KEYS_CSM131     0x0FDFFFFF//0b00001000010011111000000000000000//Nessun tasto
#define NBN_MASK_ENABLED_KEYS_CSM125     0xFFDFFFFF//0b00001000010011111000000000000000//Nessun tasto
#define NBN_MASK_LOGIC_KEYS_CSM125       0x00100000//0b00001000010011111000000000000000//Nessun tasto


#define NBN_BYTE_PADDING          0x00 //padding
/* Variables' number */
//#define EE_NB_OF_VAR             ((uint8_t)0x09)
#define BUZZER_BEEP10MS      35//time ON beep Buzzer in 10ms
#define FADING_TIME_100MS  1//5//15
#define DUTY_FADING_MAX_LEV   0.84f//0.85f
#define DUTY_FADING_STEP      0.04f
#define DUTY_FADING_MIN_LEV   0.00f
#define FADING_OFF_PERIOD_SEC  10//sec
typedef union{
	struct {
			uint32_t btn1:1;
			uint32_t btn2:1;
			uint32_t btn3:1;
			uint32_t btn4:1;
			uint32_t btn5:1;
			uint32_t btn6:1;
			uint32_t btn7:1;
			uint32_t btn8:1;
			uint32_t btn9:1;
			uint32_t btn10:1;
			uint32_t btn11:1;
			uint32_t btn12:1;
			uint32_t btn13:1;
			uint32_t btn14:1;
			uint32_t btn15:1;
			uint32_t btn16:1;
			uint32_t btn17:1;
			uint32_t btn18:1;
			uint32_t btn19:1;
			uint32_t btn20:1;
			uint32_t btn21:1;
			uint32_t btn22:1;
			uint32_t btn23:1;
			uint32_t btn24:1;
			uint32_t btn25:1;
			uint32_t btn26:1;
			uint32_t btn27:1;
			uint32_t btn28:1;
			uint32_t btn29:1;
			uint32_t btn30:1;
			uint32_t btn31:1;
			uint32_t btn32:1;

	}btn;

	    uint32_t all32;
}Keys_Func_t;





typedef union{
	struct {
			uint32_t up:1;
			uint32_t down:1;
			uint32_t rev:1;
			uint32_t trend:1;
			uint32_t ltilt:1;
			uint32_t rtilt:1;
			uint32_t ftrasl:1;
			uint32_t rtrasl:1;
			uint32_t backup:1;
			uint32_t backdown:1;
			uint32_t thorup:1;
			uint32_t thordown:1;
			uint32_t legsup:1;
			uint32_t legsdown:1;
			uint32_t legsleft:1;
			uint32_t legsright:1;
			uint32_t reflex:1;
			uint32_t chair:1;
			uint32_t flex:1;
			uint32_t level:1;
			uint32_t power:1;
			uint32_t btn22:1;
			uint32_t store:1;
			uint32_t m1m4:1;
			uint32_t recall:1;
			uint32_t m2m5:1;
			uint32_t shift:1;
			uint32_t m3m6:1;
			uint32_t btn29:1;
			uint32_t btn30:1;
			uint32_t btn31:1;
			uint32_t btn32:1;

	}btn;

	    uint32_t all32;
}Keys_NBN_Func_t;


typedef enum {
			KEYB_NOT_CONFIG      =0x00,
			KEYB_READY           =0x01,
			KEYB_RESET_TO_DEFAULT=0x02,
			KEYB_ERROR           =0x03,
			KEYB_STANDBY         =0x04,
			KEYB_SLEEP           =0x05,
			KEYB_CONFIG          =0x06,
			KEYB_WAKEUP          =0x07,
}KEYBOARD_STATUS_enum_t;

typedef enum {
           KEY_PRESSED=0x01,
           KEY_RELEASED=0x00,
}KEY_STATUS_enum_t;

typedef enum {
           PROTOCOL_RS485   =0x01,
           PROTOCOL_CANOpen =0x02,
}PROTOCOL_TYPE_enum_t;

typedef enum {
           BAUD_9600     =96,
		   BAUD_19200    =192,
		   BAUD_38400    =384,
		   BAUD_115K     =1152,
		   BAUD_250K     =2496,
		   BAUD_500K     =4992,
}BAUDRATE_TYPE_enum_t;

/*
typedef enum {
	KEY_NONE    = 0x00,//Nessun tasto
	KEY_M1UP    = 0x14,//M1 Avanti
	KEY_M2UP    = 0x27,//M2 Avanti
	KEY_M3UP    = 0x31,//0x13,//M3 Avanti
	KEY_M4UP    = 0x32,//0x14,//M4 Avanti
	KEY_M5UP_LR = 0x13,//0x15,//M5 Avanti (left & right)
	KEY_M6UP    = 0x16,//M6 Avanti
	KEY_M7UP_LR = 0x3C,//0x17,//M7 Avanti (left & right)
	KEY_M7UP_LO = 0x18,//M7 Avanti (left only)
	KEY_M7UP_RO = 0x19,//M7 Avanti (right only)
	KEY_M5UP_LO = 0x1a,//M3 Avanti (left only)
	KEY_M5UP_RO = 0x1b,//M3 Avanti (right only)
	KEY_M1DN    = 0x21,//0x21,//M1 Indietro
	KEY_M2DN    = 0x17,//0x22,//M2 Indietro
	KEY_M3DN    = 0x3A,//0x23,//M3 Indietro
	KEY_M4DN    = 0x33,//0x24,//M4 Indietro
	KEY_M5DN_LR = 0x23,//0x25,//M5 Indietro (left & right)
	KEY_M6DN    = 0x26,//M6 Indietro
	KEY_M7DN_LR = 0x3B,//0x27,//M7 Indietro (left & right)
	KEY_M7DN_LO = 0x28,//M7 Indietro (left only)
	KEY_M7DN_RO = 0x29,//M7 Indietro (right only)
	KEY_M5DN_LO = 0x2a,//M3 indietro (left only)
	KEY_M5DN_RO = 0x2b,//M3 indietro (right only)
	KEY_ROMBO   = 0x11,//0x14,//0x31,//Rombo Tasto Store KEY_STORE KEY_ROMBO
	KEY_RECALL  = 0x24,//0x32,//KEY_PUNTO
	KEY_ZERO    = 0x15,//0x13,//0x33,//Zero Tasto per Level
	KEY_RMEMA 	= 0x34,//Recall + Mem A / A2
	KEY_RMEMB 	= 0x35,//Recall + Mem B / B2
	KEY_RMEMC 	= 0x36,//Recall + Mem C / C2
	KEY_SMEMA 	= 0x37,//Store + Mem A / A2
	KEY_SMEMB   = 0x38,//Store + Mem B / B2
	KEY_SMEMC   = 0x39,//Store + Mem C / C2
	KEY_REFLEX =0x25,//0x23,//0x3A,//Reflex
	KEY_CHAIR  =0x22,//0x17,//0x3B,//Chair
	KEY_FLEX   =0x12,//0x27,//0x3C,//Flex //RECALL+TASTI MOTORE
	KEY_M1_ZR  =0x41,//M1 in posizione Zero
	KEY_M2_ZR  =0x42,//M2 in posizione Zero
	KEY_M3_ZR  =0x43,//M3 in posizione Zero
	KEY_M4_ZR  =0x44,//M4 in posizione Zero
	KEY_M5_ZR  =0x45,//M5 in posizione Zero
	KEY_M6_ZR  =0x46,//M6 in posizione Zero
	KEY_M7_ZR  =0x47,//M7 in posizione Zero
	KEY_JOY_UP_SX =0x48      ,//up + left
	KEY_JOY_UP_DX =0x49      , //up + right
	KEY_JOY_DN_DX =0x4a      ,//down + right
	KEY_JOY_DN_SX =0x4b      ,//down + left
	KEY_RMEMD  =0x54,//Recall + Mem D / D2
	KEY_RMEME  =0x55,//Recall + Mem E / E2
	KEY_RMEMF  =0x56,//Recall + Mem F / F2
	KEY_SMEMD  =0x57,//Store + Mem D / D2
	KEY_SMEME  =0x58,//Store + Mem E / E2
	KEY_SMEMF  =0x59,//Store + Mem F / F2
	KEY_SHIFT  =0x3F,//0x3F,
	KEY_ALL    =0xFF,
}KEYBOARD_FUNC_enum_t;
*/
/* KEY CODE NUOVA BN ORIGINAL */
typedef enum {
	KEY_NONE    = 0x00,//Nessun tasto
	KEY_M1UP    = 0x11,//M1 Avanti
	KEY_M2UP    = 0x12,//M2 Avanti
	KEY_M3UP    = 0x13,//M3 Avanti
	KEY_M4UP    = 0x14,//M4 Avanti
	KEY_M5UP_LR = 0x15,//M5 Avanti (left & right)
	KEY_M6UP    = 0x16,//M6 Avanti
	KEY_M7UP_LR = 0x17,//M7 Avanti (left & right)
	KEY_M7UP_LO = 0x18,//M7 Avanti (left only)
	KEY_M7UP_RO = 0x19,//M7 Avanti (right only)
	KEY_M5UP_LO = 0x1a,//M3 Avanti (left only)
	KEY_M5UP_RO = 0x1b,//M3 Avanti (right only)
	KEY_M1DN    = 0x21,//M1 Indietro
	KEY_M2DN    = 0x22,//M2 Indietro
	KEY_M3DN    = 0x23,//M3 Indietro
	KEY_M4DN    = 0x24,//M4 Indietro
	KEY_M5DN_LR = 0x25,//M5 Indietro (left & right)
	KEY_M6DN    = 0x26,//M6 Indietro
	KEY_M7DN_LR = 0x27,//M7 Indietro (left & right)
	KEY_M7DN_LO = 0x28,//M7 Indietro (left only)
	KEY_M7DN_RO = 0x29,//M7 Indietro (right only)
	KEY_M5DN_LO = 0x2a,//M3 indietro (left only)
	KEY_M5DN_RO = 0x2b,//M3 indietro (right only)
	KEY_ROMBO   = 0x31,//Rombo Tasto Store KEY_STORE KEY_ROMBO
	KEY_RECALL  = 0x32,//KEY_PUNTO
	KEY_ZERO    = 0x33,//Zero Tasto per Level
	KEY_RMEMA 	= 0x34,//Recall + Mem A / A2
	KEY_RMEMB 	= 0x35,//Recall + Mem B / B2
	KEY_RMEMC 	= 0x36,//Recall + Mem C / C2
	KEY_SMEMA 	= 0x37,//Store + Mem A / A2
	KEY_SMEMB   = 0x38,//Store + Mem B / B2
	KEY_SMEMC   = 0x39,//Store + Mem C / C2
	KEY_REFLEX =0x3A,//Reflex
	KEY_CHAIR  =0x3B,//Chair
	KEY_FLEX   =0x3C,//Flex //RECALL+TASTI MOTORE
	KEY_M1_ZR  =0x41,//M1 in posizione Zero
	KEY_M2_ZR  =0x42,//M2 in posizione Zero
	KEY_M3_ZR  =0x43,//M3 in posizione Zero
	KEY_M4_ZR  =0x44,//M4 in posizione Zero
	KEY_M5_ZR  =0x45,//M5 in posizione Zero
	KEY_M6_ZR  =0x46,//M6 in posizione Zero
	KEY_M7_ZR  =0x47,//M7 in posizione Zero
	KEY_JOY_UP_SX =0x48      ,//up + left
	KEY_JOY_UP_DX =0x49      , //up + right
	KEY_JOY_DN_DX =0x4a      ,//down + right
	KEY_JOY_DN_SX =0x4b      ,//down + left
	KEY_RMEMD  =0x54,//Recall + Mem D / D2
	KEY_RMEME  =0x55,//Recall + Mem E / E2
	KEY_RMEMF  =0x56,//Recall + Mem F / F2
	KEY_SMEMD  =0x57,//Store + Mem D / D2
	KEY_SMEME  =0x58,//Store + Mem E / E2
	KEY_SMEMF  =0x59,//Store + Mem F / F2
	KEY_SHIFT  =0x3F,
	KEY_POWER  =0x61,
	KEY_TASTO1 =0x71,
	KEY_TASTO2 =0x72,
	KEY_TASTO3 =0x81,
	KEY_TASTO4 =0x82,
	KEY_ALL    =0xFF,
}KEYBOARD_FUNC_enum_t;


                         //    30        20        10        1
typedef enum {           //   21098765432109876543210987654321
	KEY_NONE_BITMAP       = 0b00000000000000000000000000000000,//Nessun tasto
	KEY_M1UP_BITMAP       = 0b00000000000000000000000000000001,//M1 Avanti
	KEY_M2UP_BITMAP       = 0b00000000000000000000000000000100,//M2 Avanti
	KEY_M3UP_BITMAP       = 0b00000000000000000000000000010000,//M3 Avanti
	KEY_M4UP_BITMAP       = 0b00000000000000000000000001000000,//M4 Avanti
	KEY_M5UP_BITMAP       = 0b00000000000000000000000100000000,//M5 Avanti (left & right)
	KEY_M6UP_BITMAP       = 0b00000000000000000000010000000000,//M6 Avanti
	KEY_M7UP_LR_BITMAP    = 0b00000000000000000001000000000000,//M7 Avanti (left & right)
	KEY_M7UP_LO_BITMAP    = 0b00000000000000000101000000000000,//M7 Avanti (left only)
	KEY_M7UP_RO_BITMAP    = 0b00000000000000001001000000000000,//M7 Avanti (right only)
	KEY_M5UP_LO_BITMAP    = 0b00000000000000000100000100000000,//M3 Avanti (left only)
	KEY_M5UP_RO_BITMAP    = 0b00000000000000001000000100000000,//M3 Avanti (right only)
	KEY_M1DN_BITMAP       = 0b00000000000000000000000000000010,//M1 Indietro
	KEY_M2DN_BITMAP       = 0b00000000000000000000000000001000,//M2 Indietro
	KEY_M3DN_BITMAP       = 0b00000000000000000000000000100000,//M3 Indietro
	KEY_M4DN_BITMAP       = 0b00000000000000000000000010000000,//M4 Indietro
	KEY_M5DN_LR_BITMAP    = 0b00000000000000000000001000000000,//M5 Indietro (left & right)
	KEY_M6DN_BITMAP       = 0b00000000000000000000100000000000,//M6 Indietro
	KEY_M7DN_LR_BITMAP    = 0b00000000000000000010000000000000,//M7 Indietro (left & right)
	KEY_M7DN_LO_BITMAP    = 0b00000000000000000110000000000000,//M7 Indietro (left only)
	KEY_M7DN_RO_BITMAP    = 0b00000000000000001010000000000000,//M7 Indietro (right only)
	KEY_M5DN_LO_BITMAP    = 0b00000000000000000100001000000000,//M3 indietro (left only)
	KEY_M5DN_RO_BITMAP    = 0b00000000000000001000001000000000,//M3 indietro (right only)
	KEY_ROMBO_BITMAP      = 0b00000000010000000000000000000000,//Rombo Tasto Store KEY_STORE KEY_ROMBO
	KEY_RECALL_BITMAP     = 0b00000001000000000000000000000000,//KEY_PUNTO
	KEY_ZERO_BITMAP       = 0b00000000000010000000000000000000,//Zero Tasto per Level
	KEY_RMEMA_BITMAP      = 0b00000001100000000000000000000000,//Recall + Mem A / A2
	KEY_RMEMB_BITMAP      = 0b00000011000000000000000000000000,//Recall + Mem B / B2
	KEY_RMEMC_BITMAP      = 0b00001001000000000000000000000000,//Recall + Mem C / C2
	KEY_SMEMA_BITMAP      = 0b00000000110000000000000000000000,//Store + Mem A / A2
	KEY_SMEMB_BITMAP      = 0b00000010010000000000000000000000,//Store + Mem B / B2
	KEY_SMEMC_BITMAP      = 0b00001000010000000000000000000000,//Store + Mem C / C2
	KEY_REFLEX_BITMAP     = 0b00000000000000010000000000000000,//Reflex
	KEY_CHAIR_BITMAP      = 0b00000000000000100000000000000000,//Chair
	KEY_FLEX_BITMAP       = 0b00000000000001000000000000000000,//Flex //RECALL+TASTI MOTORE
	KEY_M1_ZR_BITMAP      = 0b00000001000000000000000000000001,//M1 in posizione Zero
	KEY_M1_ZR1_BITMAP     = 0b00000001000000000000000000000010,//M1 in posizione Zero
	KEY_M2_ZR_BITMAP      = 0b00000001000000000000000000000100,//M2 in posizione Zero
	KEY_M2_ZR1_BITMAP     = 0b00000001000000000000000000001000,//M2 in posizione Zero
	KEY_M3_ZR_BITMAP      = 0b00000001000000000000000000010000,//M3 in posizione Zero
	KEY_M3_ZR1_BITMAP     = 0b00000001000000000000000000100000,//M3 in posizione Zero
	KEY_M4_ZR_BITMAP      = 0b00000001000000000000000001000000,//M4 in posizione Zero
	KEY_M4_ZR1_BITMAP     = 0b00000001000000000000000010000000,//M4 in posizione Zero
	KEY_M5_ZR_BITMAP      = 0b00000001000000000000000100000000,//M5 in posizione Zero
	KEY_M5_ZR1_BITMAP     = 0b00000001000000000000001000000000,//M5 in posizione Zero
	KEY_M6_ZR_BITMAP      = 0b00000001000000000000010000000000,//M6 in posizione Zero
	KEY_M6_ZR1_BITMAP     = 0b00000001000000000000100000000000,//M6 in posizione Zero
	KEY_M7_ZR_BITMAP      = 0b00000001000000000001000000000000,//M7 in posizione Zero
	KEY_M7_ZR1_BITMAP     = 0b00000001000000000010000000000000,//M7 in posizione Zero
	KEY_JOY_UP_SX_BITMAP  = 0b00010000000000000000000000000000,//up + left
	KEY_JOY_UP_DX_BITMAP  = 0b00100000000000000000000000000000, //up + right
	KEY_JOY_DN_DX_BITMAP  = 0b01000000000000000000000000000000,//down + right
	KEY_JOY_DN_SX_BITMAP  = 0b10000000000000000000000000000000,//down + left
	KEY_RMEMD_BITMAP      = 0b00000001100000000000000000000000,//Recall + Mem D / D2
	KEY_RMEME_BITMAP      = 0b00000011000000000000000000000000,//Recall + Mem E / E2
	KEY_RMEMF_BITMAP      = 0b00001001000000000000000000000000,//Recall + Mem F / F2
	KEY_SMEMD_BITMAP      = 0b00000000110000000000000000000000,//Store + Mem D / D2
	KEY_SMEME_BITMAP      = 0b00000010010000000000000000000000,//Store + Mem E / E2
	KEY_SMEMF_BITMAP      = 0b00001000010000000000000000000000,//Store + Mem F / F2
	KEY_SHIFT_BITMAP      = 0b00000100000000000000000000000000,//Original Shift
	KEY_LOCK_UNLCK_BITMAP = 0b00000000000100000000000000000000,//LOCK UNLOCK button /Power
	KEY_TASTO1_BITMAP     = 0b00010000000000000000000000000000,//
	KEY_TASTO2_BITMAP     = 0b00100000000000000000000000000000,//
	KEY_TASTO3_BITMAP     = 0b01000000000000000000000000000000,//
	KEY_TASTO4_BITMAP     = 0b10000000000000000000000000000000,//

}NBN_tasti_bitmap_enum_t;

typedef enum {
	KEY_CMD_RD_TASTI          = 0x50,//Read Tasti
	KEY_CMD_SET_LED_ON        = 0x51,//Set led fissi
	KEY_CMD_SET_LED_TOGGLE    = 0x52,//Set led blink
	KEY_CMD_BEEP              = 0x53,//Set beep buzzer
	KEY_CMD_RD_MODEL          = 0x54,//Set/read model type
	KEY_CMD_SET_LED_BKL       = 0x55,//Set all Backlight leds
	KEY_CMD_SET_LED_TASTO     = 0x56,//Set led status: On,Off,Blin,blink level,num blink
    KEY_CMD_SET_LED_BAT       = 0x57,//Set battery level
	KEY_CMD_SET_LED_CHARGE    = 0x58,//Set led charge on/off
	KEY_CMD_NEW1              = 0x59,//TBD
	KEY_CMD_SET_ADR_SLAVE     = 0x5A,//Set new address for SLAVE on the bus 0x30,0x31,0x32,0x33
	KEY_CMD_SET_PROTOCOL      = 0x5B,//Set new protocol communication RS485,CANOpen
	KEY_CMD_SET_BAUDRATE      = 0x5C,//Set new baudrate
    KEY_CMD_SET_PARAM1        = 0x5D,//Set new time in seconds goto sleep
	KEY_CMD_RD_FW             = 0x5E,//Read FW version and subversion
}KEYBOARD_CMD_enum_t;

typedef enum {
			KEYB_CSM131=0x01,//20 tasti
			KEYB_CSM133=0x02,//26 tasti
			KEYB_CSM133_EX=0x03,//27 tasti (POWER)
			KEYB_NOT_SET=0xFFFF,
}KEYBOARD_CONFIG_enum_t;


typedef enum {
			EE_VAR_FW_ADR                   =0x2000,
			EE_VAR_MODEL_ADR                =0x2001,
			EE_VAR_NUMKEY_ADR               =0x2002,
			EE_VAR_KEY1_16_EN_ADR           =0x2003,
			EE_VAR_KEY17_32_EN_ADR          =0x2004,
			EE_VAR_SLAVE_ADR                =0x2005,
			EE_VAR_PROTOCOL_ADR             =0x2006,
			EE_VAR_BAUD_ADR                 =0x2007,
			EE_VAR_PARAM3_ADR               =0x2008,
			EE_NB_OF_VAR                    =0x0009,
}KEYBOARD_EEPROM_PARAM_AD_enum_t;





typedef enum{
	SET_LED_ON=0x01,
	SET_LED_OFF=0x02,
	SET_LED_BLINK=0x03,
	SET_LED_MAX =0x04
}LED_status_t;

typedef enum{
	BLINK_LED_SLOW    =0x01,
	BLINK_LED_MEDIUM  =0x02,
	BLINK_LED_FAST    =0x03,
	BLINK_LED_VERY_FAST=0x04,
}LED_Blink_freq_t;

typedef enum{
	LED_TASTO_TYPE=0x01,
	LED_BKL_TYPE=0x02,
	LED_BOTH_TYPE=0x03,
}LED_type_t;
typedef enum{
	BAT_LEV_0  =0x00,
	BAT_LEV_25 =0x01,
	BAT_LEV_50 =0x02,
	BAT_LEV_75 =0x03,
	BAT_LEV_100=0x04,
}BATTERY_status_t;

typedef struct {
	LED_status_t status;
	uint16_t time_blink;//millisecond
	 uint8_t num_blink;
	uint16_t countdown_blink;
	   float duty;
	 uint8_t id_PCA;
	 uint8_t id_key;
	 uint8_t blink_lev;
}Led_Func_t;



typedef struct{
	KEYBOARD_STATUS_enum_t status;
	KEYBOARD_CONFIG_enum_t model;
	word_bit_t keys_en[2];
	Keys_NBN_Func_t keys_status;
	Keys_NBN_Func_t keys_status_old;
	Keys_NBN_Func_t keys_enabled;
	Keys_NBN_Func_t keys_logic;//1: logic positive  0: logic negative
	byte_bit_t led_on_off;
	byte_bit_t led_blink;
	uint8_t key_code;
	uint8_t shift_key;
	uint8_t lock_unlock_key;
	uint8_t keys_stroke;
	 Led_Func_t led_bkl[NUM_MAX_LEDS];
	 Led_Func_t led_color[NUM_MAX_LEDS];
	 Led_Func_t led_all[NUM_MAX_LEDS];
	 uint16_t blink_counter[BLINK_LED_TIMING];//counter for countdown period 4 level blink
	 uint16_t blink_timing[BLINK_LED_TIMING]; //
	 uint8_t  blink_toggle[BLINK_LED_TIMING]; //toggle status when countdown period is over
	 uint16_t sleep_time;
	 uint16_t gotosleep;
	 uint16_t buzzer_time;
	 uint16_t buzzer_beep;
	 uint16_t fading_time;
	 uint16_t fading_off_time;
	 float  fading_duty;
	 uint8_t  fading_inc_dec;
   BATTERY_status_t bat_lev;
   led_bat_cmd_t bat_led;
   LED_status_t led_charge;
   KEYBOARD_FUNC_enum_t key_cmd;
   uint16_t num_keys;
   PROTOCOL_TYPE_enum_t protocol;
   uint32_t baudrate;
   uint8_t slave;
}Keys_NBN_t;





/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

//GPIO_TypeDef* switch_port[NUM_MAX_SWITCHES]={SW1_LEFT_T1_GPIO_Port,SW1_CENTER_T2_GPIO_Port ,SW1_RIGHT_T3_GPIO_Port,SW2_LEFT_T4_GPIO_Port,SW2_CENTER_T5_GPIO_Port ,SW2_RIGHT_T6_GPIO_Port,SW3_LEFT_T7_GPIO_Port,SW3_CENTER_T8_GPIO_Port ,SW3_RIGHT_T9_GPIO_Port,SW4_LEFT_T10_GPIO_Port,SW4_CENTER_T11_GPIO_Port ,SW4_RIGHT_T12_GPIO_Port};
//uint16_t      switch_pin[NUM_MAX_SWITCHES] ={SW1_LEFT_T1_Pin,SW1_CENTER_T2_Pin ,SW1_RIGHT_T3_Pin,SW2_LEFT_T4_Pin,SW2_CENTER_T5_Pin ,SW2_RIGHT_T6_Pin,SW3_LEFT_T7_Pin,SW3_CENTER_T8_Pin ,SW3_RIGHT_T9_Pin,SW4_LEFT_T10_Pin,SW4_CENTER_T11_Pin ,SW4_RIGHT_T12_Pin};
/* Define ------------------------------------------------------------*/
#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif


#define BIT_0         (0x01U)
#define BIT_1         (0x02U)
#define BIT_2         (0x04U)
#define BIT_3         (0x08U)
#define BIT_4         (0x10U)
#define BIT_5         (0x20U)
#define BIT_6         (0x40U)
#define BIT_7         (0x80U)

#define BIT_8         (0x0100U)
#define BIT_9         (0x0200U)
#define BIT_10        (0x0400U)
#define BIT_11        (0x0800U)
#define BIT_12        (0x1000U)
#define BIT_13        (0x2000U)
#define BIT_14        (0x4000U)
#define BIT_15        (0x8000U)

#define BIT_16        (0x010000U)
#define BIT_17        (0x020000U)
#define BIT_18        (0x040000U)
#define BIT_19        (0x080000U)
#define BIT_20        (0x100000U)
#define BIT_21        (0x200000U)
#define BIT_22        (0x400000U)
#define BIT_23        (0x800000U)

#define BIT_24        (0x01000000U)
#define BIT_25        (0x02000000U)
#define BIT_26        (0x04000000U)
#define BIT_27        (0x08000000U)
#define BIT_28        (0x10000000U)
#define BIT_29        (0x20000000U)
#define BIT_30        (0x40000000U)
#define BIT_31        (0x80000000U)



#define RS485_TX_HMI_ENABLE    HAL_GPIO_WritePin(RS485_CONTROL_RTS_GPIO_Port,RS485_CONTROL_RTS_Pin,GPIO_PIN_SET)// HAL_GPIO_WritePin(RS485_HMI_RTS_GPIO_Port,RS485_HMI_RTS_Pin,GPIO_PIN_SET)
#define RS485_TX_HMI_DISABLE   HAL_GPIO_WritePin(RS485_CONTROL_RTS_GPIO_Port,RS485_CONTROL_RTS_Pin,GPIO_PIN_RESET)// HAL_GPIO_WritePin(RS485_HMI_RTS_GPIO_Port,RS485_HMI_RTS_Pin,GPIO_PIN_RESET)
#define RS485_RX_HMI_ENABLE    HAL_GPIO_WritePin(RS485_CONTROL_CTS_GPIO_Port,RS485_CONTROL_CTS_Pin,GPIO_PIN_RESET)// HAL_GPIO_WritePin(RS485_HMI_CTS_GPIO_Port,RS485_HMI_CTS_Pin,GPIO_PIN_RESET)
#define RS485_RX_HMI_DISABLE   HAL_GPIO_WritePin(RS485_CONTROL_CTS_GPIO_Port,RS485_CONTROL_CTS_Pin,GPIO_PIN_SET)// HAL_GPIO_WritePin(RS485_HMI_CTS_GPIO_Port,RS485_HMI_CTS_Pin,GPIO_PIN_SET)

/*RS485  DEFINE*/
#define REVO3_HMI_RS485_ADDR                               16
#define NBN_KEYBOARD_RS485_ADDR                          0x31
#define RS485_HMI_BAUDRATE                               9600//19200//9600//115200
#define RS485_CMD_RD_TASTI                                 32
#define TIME_END_OF_FRAME_RS485_MS                         3//5//10//3//5//[ms] time elapsed from last byte received to estimate end of frame
#define LEN_MIN_FRAME_RS485                                4

/*TIMERS*/
#define TIMER_PRESCALER_TO_1MHZ                    48
#define TIMER_UP_RELOAD_TO_100us           99//(65536-100)
#define TIMER_UP_RELOAD_TO_1ms            999//(65536-1000)
#define TIMER_UP_RELOAD_TO_10ms          9999//(65536 -10000)


/*Error Management*/
#define NUM_ERROR_CATEGORY 10
/*Fan*/
#define FAN1_DUTY_STANDBY            20
#define FAN1_DUTY_GRINDING           30
#define FAN2_DUTY_STANDBY            40
#define FAN2_DUTY_GRINDING           55

/*Balance*/
#define  __BALANCE_NOT_CONNECTED   1

/*INVERTER*/
#define LEN_RX_RS232         6
#define LEN_TX_RS232         6
#define FIRST_CLEAN_CYCLE  0x01
#define END_CLEAN_CYCLE    0x02


/*BURRs Stepper*/
#define BURR_SCREW_PITCH_MICRON  1000  //micron
#define BURR_DISTANCE_COEFF      (2000/BURR_SCREW_PITCH_MICRON)

/*KEYBOARD*/
/* Define ------------------------------------------------------------*/
#define NUM_MAX_SWITCHES       32
#define DEBOUNCE_SW_REF1_ms    2500       /*in milliseconds*/
#define DEBOUNCE_SW_REF2_ms    3500       /*in milliseconds*/
#define DEBOUNCE_SW_REF3_ms    5000       /*in milliseconds*/
#define DEBOUNCE_SW_RELEASE_ms    5       /*in milliseconds*/



/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/
/*INVERTER COMMUNICATION  ModBus*/
uint8_t transmission_ready_Flag;
uint8_t messageOkFlag, messageErrFlag;
uint8_t retry_count;
uint16_t timeout, polling;
uint16_t T1_5; // inter character time out in microseconds
uint16_t T3_5; // frame delay in microseconds
uint32_t previousTimeout, previousPolling;
uint32_t timeout_TX_INV_mdb;




/* ----------------------- Static variables ---------------------------------*/

uint8_t buffer_RX_RS232[50];
uint8_t buffer_TX_RS232[10];

bool new_data_rx;
bool wait_reply;

/*Service*/
service_bit_t service;

/*Clock System timer Variables*/
sys_timer_t main_clock;

uint16_t cmd_rx_timeout;
uint16_t adr_modbus_reg;//Slave address in RS485 communication to HMI board


//REVO_Op_CMD_t revo_cmd_rd;

uint8_t  DP_SW_RS485_adr;//dipswitch to set address of POWER BOARD on ModBus HMI

uint32_t version_fw[3];


/*Time variables*/
uint32_t last_recv_mdb_time ;
uint8_t flag_10ms;
uint8_t flag_100ms;
uint8_t flag_1sec;
uint8_t flag_1min;
uint16_t timeout_zero_stepper;
uint16_t runtimer_ms;
uint16_t time_elapsed[8];
/**/


/*Error*/
REVO_ERROR_enum_t  revo_error_cat;
word_bit_t  errorList[NUM_ERROR_CATEGORY];
error_cat_t error_cat;
/*Sensor*/

REVO_temp_sensor temp_sens[NUM_MAX_TEMP_SENSOR];
uint32_t adc_raw_value[NUM_MAX_ADC_CH];
REVO_ADC_ch adc123_ch[NUM_MAX_ADC_CH];






/*LEDs*/
pca9685_handle_t pca9685_led[4];
uint8_t last_led_type;
uint8_t TOGGLE_PIN_LED;
/*Service*/
service_bit_t service;

/*Keyboard*/

uint16_t sw_debounce1_counter[NUM_MAX_SWITCHES];
uint16_t sw_debounce2_counter[NUM_MAX_SWITCHES];
uint16_t sw_debounce3_counter[NUM_MAX_SWITCHES];
uint16_t sw_deb_release_cnt[NUM_MAX_SWITCHES];

uint32_t switches_bitmap_actual;
uint32_t switches_bitmap_deb1;
uint32_t switches_bitmap_deb2;
uint32_t switches_bitmap_deb3;

unsigned int short_press_time;
unsigned int long_press_time;

//Keys_Func_t keyboard_raw;
Keys_Func_t keyboard_short;
Keys_Func_t keyboard_long;

Keys_NBN_t NBN_KB_CSM;

/*EEEPROM EMULATED*/

uint16_t VirtAddVarTab[EE_NB_OF_VAR];/* Virtual address defined by the user: 0xFFFF value is prohibited */
uint16_t VarDataTab[EE_NB_OF_VAR];// = {0, 0, 0};
/* Function prototypes -----------------------------------------------*/



#endif /* INC_GLOBALS_H_ */
