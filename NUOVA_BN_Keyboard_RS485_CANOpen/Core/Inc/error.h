/*
 ******************************************************************************
 *  @file      : error.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 20 gen 2020
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_ERROR_H_
#define INC_ERROR_H_


/* Includes ----------------------------------------------------------*/
#include "main.h"
#include "globals.h"

/* Typedef -----------------------------------------------------------*/

/* Define ------------------------------------------------------------*/


/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/



/* Function prototypes -----------------------------------------------*/
void error_set(REVO_ERROR_enum_t cat,uint8_t id_cat,uint8_t code);
void error_print(void);
void error_reset(void);


#endif /* INC_ERROR_H_ */
