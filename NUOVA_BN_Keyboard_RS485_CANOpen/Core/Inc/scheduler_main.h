/*
 ******************************************************************************
 *  @file      : scheduler_main.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 08 ott 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_SCHEDULER_MAIN_H_
#define INC_SCHEDULER_MAIN_H_


/* Includes ----------------------------------------------------------*/
#include "main.h"
#include "globals.h"
/* Typedef -----------------------------------------------------------*/

/* Define ------------------------------------------------------------*/


/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/



/* Function prototypes -----------------------------------------------*/
void scheduler_task(void);


#endif /* INC_SCHEDULER_MAIN_H_ */
