/*
 * mdb_registers.h
 *
 *  Created on: 04 ago 2019
 *      Author: VALENTI
 */

#ifndef INC_MODBUS_DMA_MDB_REGISTERS_H_
#define INC_MODBUS_DMA_MDB_REGISTERS_H_


#include "main.h"

#define MDB_OK						(0)
#define MDB_ERR_ILLEGAL_FUNCTION    (1)
#define MDB_ERR_ILLEGAL_DATAADDRESS (2)
#define MDB_ERR_ILLEGAL_DATAVALUE   (3)



/*MODBUS Registers*/
/*MODBUS HOLDING REGISTER ADDRESS*/

#define MDB_FW_REL_REG                                     0x100
#define MDB_FW_INV_REG                                     0x101
#define MDB_FW_LOADCELL_REG                                0x102




#define MDB_BOARD_CONFIG_REG                              0x1000
/*MDB ERROR */
#define MDB_ERROR_HMI_REG                                 0x0080
#define MDB_ERROR_DOSER_REG                               0x0081
#define MDB_ERROR_HOPPER_REG                              0x0082
#define MDB_ERROR_WEIGHT_REG                              0x0083
#define MDB_ERROR_FAN_REG                                 0x0084
#define MDB_ERROR_INVERTER_REG                            0x0085
#define MDB_ERROR_BURR_REG                                0x0086
#define MDB_ERROR_GENERAL_REG                             0x0087


/*MDB READ REGISTERs*/
#define MDB_BOARD_STATUS_REG                               0x0200
#define MDB_POS_SENSOR_REG                                 0x0201
#define MDB_HOP1_WEIGHT_REG                                0x0202
#define MDB_HOP2_WEIGHT_REG                                0x0203
#define MDB_HOP3_WEIGHT_REG                                0x0204
#define MDB_HOP1_TIME_REG                                  0x0205
#define MDB_HOP2_TIME_REG                                  0x0206
#define MDB_HOP3_TIME_REG                                  0x0207
#define MDB_EXT_TEMPERATURE_REG                            0x0208
#define MDB_EXT_HUMIDITY_REG                               0x0209
#define MDB_BURR_ENC_STEP_REG                              0x020A
#define MDB_NTC_GRIND_TEMP_REG                             0x020B
#define MDB_FAN1_RPM_REG                                   0x020C
#define MDB_FAN2_RPM_REG                                   0x020D
#define MDB_INT_TEMPERATURE_REG                            0x020E
#define MDB_INT_HUMIDITY_REG                               0x020F



/*MDB WRITE REGISTERs*/
#define MDB_OPMODE_REG                                     0x0300
#define MDB_SET_HOP1_WEIGHT_REG                            0x0301
#define MDB_SET_HOP2_WEIGHT_REG                            0x0302
#define MDB_SET_HOP3_WEIGHT_REG                            0x0303
#define MDB_SET_HOP1_TIME_REG                              0x0304
#define MDB_SET_HOP2_TIME_REG                              0x0305
#define MDB_SET_HOP3_TIME_REG                              0x0306
#define MDB_SET_HOP1_PWM_REG                               0x0307
#define MDB_SET_HOP2_PWM_REG                               0x0308
#define MDB_SET_HOP3_PWM_REG                               0x0309
#define MDB_OFSET_HOP1_REG                                 0x030A
#define MDB_OFSET_HOP2_REG                                 0x030B
#define MDB_OFSET_HOP3_REG                                 0x030C
/*GRINDER*/
#define MDB_SET_GRIND_SIZE_REG                             0x030D
#define MDB_SET_GRIND_SPEED_REG                            0x030E
#define MDB_SET_GRIND_TIME_REG                             0x030F
#define MDB_SET_GRIND_POWER_REG                            0x0310

#define MDB_OPMODE_CMD_REG                                 0x0400
#define MDB_SERVICE_CMD_REG                                0x0410
#define MDB_CAL_CMD_REG                                    0x0420
#define MDB_SET_VIBRO_REG                                  0x0500
#define MDB_SET_FAN_SPEED_REG                              0x0600
#define MDB_SET_TEMP_THRES1_REG                            0x0601
#define MDB_SET_TEMP_THRES2_REG                            0x0602

/*DOSER VIBRO PARAMETERS*/
#define MDB_SET_DOS1_FREQ_REG                              0x0701
#define MDB_SET_DOS2_FREQ_REG                              0x0702
#define MDB_SET_DOS3_FREQ_REG                              0x0703
#define MDB_SET_VIBRO_FREQ_REG                             0x0704
#define MDB_SET_DOS1_DUTY_REG                              0x0705
#define MDB_SET_DOS2_DUTY_REG                              0x0706
#define MDB_SET_DOS3_DUTY_REG                              0x0707
#define MDB_SET_VIBRO_DUTY_REG                             0x0708
#define MDB_SET_DIR_DOS1_REG                               0x0709
#define MDB_SET_DIR_DOS2_REG                               0x070A
#define MDB_SET_DIR_DOS3_REG                               0x070B
#define MDB_SET_DIR_VIBRO_REG                              0x070C
#define MDB_SET_STOP_DOS1_REG                              0x070D
#define MDB_SET_STOP_DOS2_REG                              0x070E
#define MDB_SET_STOP_DOS3_REG                              0x070F
#define MDB_SET_STOP_VIBRO_REG                             0x0710

/*FAN Parameters*/
#define MDB_SET_FAN1_FREQ_REG                              0x0720
#define MDB_SET_FAN2_FREQ_REG                              0x0721
#define MDB_SET_FAN1_DUTY_REG                              0x0722
#define MDB_SET_FAN2_DUTY_REG                              0x0723
#define MDB_SET_POWER_FAN1_REG                             0x0724
#define MDB_SET_POWER_FAN2_REG                             0x0725

/*HOPPER and BURR STEPPER  PARAMETERS*/
#define MDB_SET_HOP1_POS_ACT_REG                           0x0740
#define MDB_SET_HOP2_POS_ACT_REG                           0x0741
#define MDB_SET_HOP3_POS_ACT_REG                           0x0742
#define MDB_SET_BURR0_POS_ACT_REG                          0x0743
#define MDB_SET_HOP1_SPEED_ACT_REG                         0x0744
#define MDB_SET_HOP2_SPEED_ACT_REG                         0x0745
#define MDB_SET_HOP3_SPEED_ACT_REG                         0x0746
#define MDB_SET_BURR0_SPEED_ACT_REG                        0x0747
#define MDB_SET_HOP1_POS_REG                               0x0750
#define MDB_SET_HOP2_POS_REG                               0x0751
#define MDB_SET_HOP3_POS_REG                               0x0752
#define MDB_SET_BURR0_POS_REG                              0x0753
#define MDB_SET_HOP1_FREQ_REG                              0x0754
#define MDB_SET_HOP2_FREQ_REG                              0x0755
#define MDB_SET_HOP3_FREQ_REG                              0x0756
#define MDB_SET_BURR0_FREQ_REG                             0x0757
#define MDB_SET_HOP1_DIR_REG                               0x0758
#define MDB_SET_HOP2_DIR_REG                               0x0759
#define MDB_SET_HOP3_DIR_REG                               0x075A
#define MDB_SET_BURR0_DIR_REG                              0x075B

/*ILLUMINA LED*/
#define MDB_SET_ILLUMINA_DUTY_REG                          0x0760

/*End MODBUS Registers*/



/* Funzioni di lettura:
 * in: index, indice modbus (0-65535)
 * out: >=0 valore letto (0-65535)
 *      <0 errore modbus negato (es: -ILLEGAL_DATAADDRESS)
 */
int32_t mdb_read_status(uint16_t index);
int32_t mdb_read_coil(uint16_t index);
int32_t mdb_read_input_register(uint16_t index);
int32_t mdb_read_holding_register(uint16_t index);

/* Funzioni di scrittura:
 * in: index: indice modbus (0-65535)
 *     value: valore da scrivere (0-65535)
 * out: errore modbus negato (es: 0=ok, -ILLEGAL_DATAADDRESS)
 */
int32_t mdb_write_coil(uint16_t index, uint16_t value);
int32_t mdb_write_holding_register(uint16_t index, uint16_t value);



#endif /* INC_MODBUS_DMA_MDB_REGISTERS_H_ */
