
#ifndef  PCA9685_I2C_H
#define PCA9685_I2C_H

#include <stdbool.h>
#include "globals.h"

#ifndef PCA9685_I2C_TIMEOUT
#define PCA9685_I2C_TIMEOUT 1
#endif

#define PCA9865_I2C_DEFAULT_DEVICE_ADDRESS  0x82

#define PCA9865_I2C_DEFAULT_DEVICE_ADDRESS1 0x84

#define PCA9865_I2C_DEFAULT_DEVICE_ADDRESS2 0x86//0x88

#define PCA9865_I2C_DEFAULT_DEVICE_ADDRESS3 0x88//0x90


/**
 * Registers addresses.
 */

//
//	Registers
//
#define PCA9685_SUBADR1 0x2
#define PCA9685_SUBADR2 0x3
#define PCA9685_SUBADR3 0x4

#define PCA9685_MODE1 		0x0
#define PCA9685_PRESCALE 	0xFE

#define PCA9685_LED0_ON_L 	0x6
#define PCA9685_LED0_ON_H	0x7
#define PCA9685_LED0_OFF_L	0x8
#define PCA9685_LED0_OFF_H 	0x9

#define PCA9685_ALLLED_ON_L 	0xFA
#define PCA9685_ALLLED_ON_H 	0xFB
#define PCA9685_ALLLED_OFF_L 	0xFC
#define PCA9685_ALLLED_OFF_H 	0xFD

#define PCA9685_MODE1_ALCALL_BIT	0
typedef enum
{
	PCA9685_MODE1_SUB1_BIT 	= 3,
	PCA9685_MODE1_SUB2_BIT	= 2,
	PCA9685_MODE1_SUB3_BIT	= 1
}SubaddressBit;
#define PCA9685_MODE1_SLEEP_BIT		4
#define PCA9685_MODE1_AI_BIT		5
#define PCA9685_MODE1_EXTCLK_BIT	6
#define PCA9685_MODE1_RESTART_BIT	7


#define PCA9685_MODE2_INVERT		4
#define PCA9685_MODE2_OCH		    3
#define PCA9685_MODE2_OUTDRV	    2
#define PCA9685_MODE2_OUTNE1	    1
#define PCA9685_MODE2_OUTNE0	    0

typedef enum
{
	PCA9685_OK 		= 0,
	PCA9685_ERROR	= 1
}PCA9685_STATUS;

typedef enum
{
	PCA9685_REGISTER_MODE1 = 0x00,
	PCA9685_REGISTER_MODE2 = 0x01,
	PCA9685_REGISTER_LED0_ON_L = 0x06,
	PCA9685_REGISTER_ALL_LED_ON_L = 0xfa,
	PCA9685_REGISTER_ALL_LED_ON_H = 0xfb,
	PCA9685_REGISTER_ALL_LED_OFF_L = 0xfc,
	PCA9685_REGISTER_ALL_LED_OFF_H = 0xfd,
	PCA9685_REGISTER_PRESCALER = 0xfe
} pca9685_register_t;

/**
 * Bit masks for the mode 1 register.
 */
typedef enum
{
	PCA9685_REGISTER_MODE1_SLEEP = (1u << 4u),
	PCA9685_REGISTER_MODE1_RESTART = (1u << 7u)
} pca9685_register_mode1_t;
/**
 * Initialises a PCA9685 device by resetting registers to known values, setting a PWM frequency of 1000Hz, turning
 * all channels off and waking it up.
 * @param handle Handle to a PCA9685 device.
 * @return True on success, false otherwise.
 */
bool pca9685_init(pca9685_handle_t *handle);

/**
 * Tests if a PCA9685 is sleeping.
 * @param handle Handle to a PCA9685 device.
 * @param sleeping Set to the sleeping state of the device.
 * @return True on success, false otherwise.
 */
bool pca9685_is_sleeping(pca9685_handle_t *handle, bool *sleeping);

/**
 * Puts a PCA9685 device into sleep mode.
 * @param handle Handle to a PCA9685 device.
 * @return True on success, false otherwise.
 */
bool pca9685_sleep(pca9685_handle_t *handle);

/**
 * Wakes a PCA9685 device up from sleep mode.
 * @param handle Handle to a PCA9685 device.
 * @return True on success, false otherwise.
 */
bool pca9685_wakeup(pca9685_handle_t *handle);

/**
 * Sets the PWM frequency of a PCA9685 device for all channels.
 * Asserts that the given frequency is between 24 and 1526 Hertz.
 * @param handle Handle to a PCA9685 device.
 * @param frequency PWM frequency to set in Hertz.
 * @return True on success, false otherwise.
 */
bool pca9685_set_pwm_frequency(pca9685_handle_t *handle, float frequency);

/**
 * Sets the PWM on and off times for a channel of a PCA9685 device.
 * Asserts that the given channel is between 0 and 15.
 * Asserts that the on and off times are between 0 and 4096.
 * @param handle Handle to a PCA9685 device.
 * @param channel Channel to set the times for.
 * @param on_time PWM on time of the channel.
 * @param off_time PWM off time of the channel.
 * @return True on success, false otherwise.
 */
bool pca9685_set_channel_pwm_times(pca9685_handle_t *handle, unsigned channel, unsigned on_time, unsigned off_time);

/**
 * Helper function to set the PWM duty cycle for a channel of a PCA9685 device. The duty cycle is either directly
 * converted to a 12-bit value used for the PWM timings, if logarithmic is set to false, or an 8-bit value which is then
 * transformed to a 12-bit value using a look up table for the PWM timings.
 * Asserts that the duty cycle is between 0 and 1.
 * @param handle Handle to a PCA9685 device.
 * @param channel Channel to set the duty cycle of.
 * @param duty_cycle Duty cycle to set.
 * @param logarithmic Set to true to apply logarithmic function.
 * @return True on success, false otherwise.
 */
bool pca9685_set_channel_duty_cycle(pca9685_handle_t *handle, unsigned channel, float duty_cycle, bool logarithmic);

bool pca9685_set_group_duty_cycle(pca9685_handle_t *handle, unsigned start,unsigned end, float duty_cycle, bool logarithmic);


void set_led_NBN(uint8_t led,LED_status_t status,uint16_t time_100ms,uint8_t n_blink,uint8_t blink_lev,uint8_t type );
void set_led_BLK_NBN(uint8_t led,LED_status_t status,uint16_t time_100ms,uint8_t n_blink,uint8_t blink_lev,uint8_t type );
void pca9685_begin(void);

uint8_t pca9685_set_led_tasto(uint8_t led,LED_status_t status,uint8_t n_blink,uint8_t blink_lev);
void set_all_led_BKL_NBN(uint8_t status,uint8_t n_blink,uint8_t blink_lev);
uint8_t set_led_tasto_retro_NBN(byte_bit_t key_code,uint8_t status,uint8_t n_blink,uint8_t blink_lev);
uint8_t set_led_tasto_NBN(uint8_t key_code,uint8_t status,uint8_t n_blink,uint8_t blink_lev);
void logo_fading(uint8_t status);
void pca9685_refresh_led(uint8_t type);
void pca9685_refresh_led_fast(void);
#endif
