/*
 ******************************************************************************
 *  @file      : input_keyboard.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 22 lug 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : SANREMO_Opera 
 *   
 *  @brief     : Cortex-M0 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_INPUT_KEYBOARD_H_
#define INC_INPUT_KEYBOARD_H_


/* Includes ----------------------------------------------------------*/
#include "main.h"
/* Typedef -----------------------------------------------------------*/
//typedef struct {
//		uint16_t sw1_l:1;
//		uint16_t sw1_c:1;
//		uint16_t sw1_r:1;
//		uint16_t sw2_l:1;
//		uint16_t sw2_c:1;
//		uint16_t sw2_r:1;
//		uint16_t sw3_l:1;
//		uint16_t sw3_c:1;
//		uint16_t sw3_r:1;
//		uint16_t sw4_l:1;
//		uint16_t sw4_c:1;
//		uint16_t sw4_r:1;
//		uint16_t sw5_l:1;
//		uint16_t sw5_c:1;
//		uint16_t sw5_r:1;
//		uint16_t sw_free1:1;
//} integer_bit_struct_t;
/**
 * @brief
 */

/**
 * @brief
 */

/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/


//GPIO_TypeDef* switch_port[NUM_MAX_SWITCHES]={SW1_LEFT_T1_GPIO_Port,SW1_CENTER_T2_GPIO_Port ,SW1_RIGHT_T3_GPIO_Port,SW2_LEFT_T4_GPIO_Port,SW2_CENTER_T5_GPIO_Port ,SW2_RIGHT_T6_GPIO_Port,SW3_LEFT_T7_GPIO_Port,SW3_CENTER_T8_GPIO_Port ,SW3_RIGHT_T9_GPIO_Port,SW4_LEFT_T10_GPIO_Port,SW4_CENTER_T11_GPIO_Port ,SW4_RIGHT_T12_GPIO_Port};
//const uint16_t      switch_pin[NUM_MAX_SWITCHES] ={SW1_LEFT_T1_Pin,SW1_CENTER_T2_Pin ,SW1_RIGHT_T3_Pin,SW2_LEFT_T4_Pin,SW2_CENTER_T5_Pin ,SW2_RIGHT_T6_Pin,SW3_LEFT_T7_Pin,SW3_CENTER_T8_Pin ,SW3_RIGHT_T9_Pin,SW4_LEFT_T10_Pin,SW4_CENTER_T11_Pin ,SW4_RIGHT_T12_Pin};

/* Function prototypes -----------------------------------------------*/
/**
 * @brief  Initialize function for input keyboard switches
 * @note   This function inizialize all variables to manage inputs switches
 * @param  None
 * @retval None
 */
void init_input_reayboard(void);

/**
 * @brief  Read and debounce function.
 * @note   This function have to be called in.
 * @param  None
 * @retval None
 */
void read_input_keyboard(void);

void keyboard_function_set(void);

void key_SHIFT_POWER(void);
/**
 * @brief  Read and debounce function.
 * @note   This function have to be called in.
 * @param  None
 * @retval None
 */
unsigned char keyboard_function_manager(void);




#endif /* INC_INPUT_KEYBOARD_H_ */
