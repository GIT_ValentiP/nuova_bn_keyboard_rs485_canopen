/*
 ******************************************************************************
 *  @file      : input_keyboard.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 22 lug 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : SANREMO_Opera 
 *   
 *  @brief     : Cortex-M0 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "input_keyboard.h"
#include "globals.h"
#include "main.h"
#include "./PCA9685_LED_DRIVER/pca9685.h"
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

GPIO_TypeDef* switch_port[NUM_MAX_SWITCHES]={
BTN01_UP_GPIO_Port                ,
BTN02_DOWN_GPIO_Port              ,
BTN03_REV_GPIO_Port       	    ,
BTN04_TREND_GPIO_Port         ,
BTN05_TILT_L_GPIO_Port    	,
BTN06_TILT_R_GPIO_Port    	,
BTN07_TRASL_F_GPIO_Port   	,
BTN08_TRASL_R_GPIO_Port   	,
BTN09_BACKUP_GPIO_Port    	,
BTN10_BACKDOWN_GPIO_Port  	,
BTN11_THOR_UP_GPIO_Port   	,
BTN12_THOR_DOWN_GPIO_Port ,
BTN13_LEG_UP_GPIO_Port    	,
BTN14_LEG_DOWN_GPIO_Port  	,
BTN15_LEG_L_GPIO_Port     	,
BTN16_LEG_R_GPIO_Port     	,
BTN17_REFLEX_GPIO_Port    	,
BTN18_CHAIR_GPIO_Port     	,
BTN19_FLEX_GPIO_Port      	    ,
BTN20_LEVEL_GPIO_Port     	,
BTN21_POWER_GPIO_Port     	,
BTN_GP1_GPIO_Port         		,
BTN23_STORE_GPIO_Port     	,
BTN24_M1M4_GPIO_Port      	    ,
BTN25_RECALL_GPIO_Port    	,
BTN26_M2M5_GPIO_Port      	    ,
BTN27_SHIFT_GPIO_Port     	,
BTN28_M3M6_GPIO_Port      	    ,
BTN29_OPT1_GPIO_Port      	    ,
BTN30_OPT2_GPIO_Port      		,
BTN31_OPT3_GPIO_Port      		,
BTN32_OPT4_GPIO_Port      		,
//BTN_GP1_GPIO_Port         		,
//BTN_GP2_GPIO_Port         		,
//BTN_GP3_GPIO_Port         		,
//BTN_GP4_GPIO_Port         	    ,

};



uint16_t      switch_pin[NUM_MAX_SWITCHES] ={
BTN01_UP_Pin        ,
BTN02_DOWN_Pin      ,
BTN03_REV_Pin       ,
BTN04_TREND_Pin     ,
BTN05_TILT_L_Pin    ,
BTN06_TILT_R_Pin    ,
BTN07_TRASL_F_Pin   ,
BTN08_TRASL_R_Pin   ,
BTN09_BACKUP_Pin    ,
BTN10_BACKDOWN_Pin  ,
BTN11_THOR_UP_Pin   ,
BTN12_THOR_DOWN_Pin ,
BTN13_LEG_UP_Pin    ,
BTN14_LEG_DOWN_Pin  ,
BTN15_LEG_L_Pin     ,
BTN16_LEG_R_Pin     ,
BTN17_REFLEX_Pin    ,
BTN18_CHAIR_Pin     ,
BTN19_FLEX_Pin      ,
BTN20_LEVEL_Pin     ,
BTN21_POWER_Pin     ,
BTN_GP1_Pin         ,
BTN23_STORE_Pin     ,
BTN24_M1M4_Pin      ,
BTN25_RECALL_Pin    ,
BTN26_M2M5_Pin      ,
BTN27_SHIFT_Pin     ,
BTN28_M3M6_Pin      ,
BTN29_OPT1_Pin      ,
BTN30_OPT2_Pin      ,
BTN31_OPT3_Pin      ,
BTN32_OPT4_Pin      ,
//BTN_GP1_Pin         ,
//BTN_GP2_Pin         ,
//BTN_GP3_Pin         ,
//BTN_GP4_Pin         ,

};
/* Private function prototypes -----------------------------------------------*/

/* Private user code ---------------------------------------------------------*/



/* Function prototypes -----------------------------------------------*/
/**
 * @brief  Initialize function for input keyboard switches
 * @note   This function inizialize all variables to manage inputs switches
 * @param  None
 * @retval None
 */
void init_input_reayboard(void){
unsigned char i;
	for(i=0;i<NUM_MAX_SWITCHES;i++){
		sw_debounce1_counter[i]=0;
		sw_debounce2_counter[i]=0;
		sw_debounce3_counter[i]=0;
		sw_deb_release_cnt[i]=0;
	}

	switches_bitmap_actual=0x00000000;
	switches_bitmap_deb1  =0x00100000;
	switches_bitmap_deb2  =0x00000000;
	switches_bitmap_deb3  =0x00000000;


	long_press_time=5;//5;//in 10ms unit//10;
	NBN_KB_CSM.keys_status_old.all32=0x00000000;
	NBN_KB_CSM.keys_status.all32=0x00000000;

}



/**
 * @brief  Read and debounce function.
 * @note   This function have to be called in.
 * @param  None
 * @retval None
 */
void read_input_keyboard(void){
uint32_t mask_bit,mask_bit_not,logic;
unsigned char id_btn;
GPIO_PinState status_btn;
//unsigned char status_btn_old;
GPIO_TypeDef *sw_port;
mask_bit=0x00000001;
mask_bit_not=~(mask_bit);


for(id_btn=0;id_btn < NUM_MAX_SWITCHES;id_btn++){
	  mask_bit=0x00000001;
	  mask_bit <<=id_btn;
	  mask_bit_not=~(mask_bit);
	 // status_btn_old =((switches_bitmap_actual & mask_bit)>>id_btn);
	  sw_port=switch_port[id_btn];
	  status_btn= HAL_GPIO_ReadPin(sw_port, switch_pin[id_btn]);
      logic=(NBN_KB_CSM.keys_logic.all32 & mask_bit);
      if(logic==0){
		  if (status_btn == GPIO_PIN_RESET){// button pressed
				switches_bitmap_actual |=(mask_bit);
				sw_debounce1_counter[id_btn]++;//for distinguish long press
				if (sw_debounce1_counter[id_btn] >  long_press_time ){//override debounce 1
				  sw_debounce1_counter[id_btn]= long_press_time ;
				  switches_bitmap_deb1 |=(mask_bit);
				  keyboard_long.all32 |=(mask_bit);       //register long press
				}
				sw_deb_release_cnt[id_btn]=0;
		  }else{// button id_btn released or not pressed
				 /*Short or long press*/
				 sw_debounce1_counter[id_btn]=0;//long/short press counter
				 switches_bitmap_deb1   &=(mask_bit_not);
				 switches_bitmap_actual &=(mask_bit_not);//release bit of id_btn in bitmap
				 /*Debounce also for release*/
				sw_deb_release_cnt[id_btn]++;
				if(sw_deb_release_cnt[id_btn]==(DEBOUNCE_SW_RELEASE_ms)){//override debounce of released status,button return to idle 0 state
					  sw_deb_release_cnt[id_btn]=(DEBOUNCE_SW_RELEASE_ms);
				}//end Debounce_release
		  }//end release
      }else{
		  if (status_btn == GPIO_PIN_SET){// button pressed
				switches_bitmap_actual |=(mask_bit);
				sw_debounce1_counter[id_btn]++;//for distinguish long press
				if (sw_debounce1_counter[id_btn] >  long_press_time ){//override debounce 1
				  sw_debounce1_counter[id_btn]= long_press_time ;
				  switches_bitmap_deb1 |=(mask_bit);
				  keyboard_long.all32 |=(mask_bit);       //register long press
				}
				sw_deb_release_cnt[id_btn]=0;
		  }else{// button id_btn released or not pressed
				 /*Short or long press*/
				 sw_debounce1_counter[id_btn]=0;//long/short press counter
				 switches_bitmap_deb1   &=(mask_bit_not);
				 switches_bitmap_actual &=(mask_bit_not);//release bit of id_btn in bitmap
				 /*Debounce also for release*/
				sw_deb_release_cnt[id_btn]++;
				if(sw_deb_release_cnt[id_btn]==(DEBOUNCE_SW_RELEASE_ms)){//override debounce of released status,button return to idle 0 state
					  sw_deb_release_cnt[id_btn]=(DEBOUNCE_SW_RELEASE_ms);
				}//end Debounce_release
		  }//end release
      }
 }//end_for

NBN_KB_CSM.keys_status_old.all32=NBN_KB_CSM.keys_status.all32;
NBN_KB_CSM.keys_status.all32= (switches_bitmap_deb1 & NBN_KB_CSM.keys_enabled.all32);//switches_bitmap_actual ;

}
/**
 * @brief  Indicate the switch pressed
 * @note   This function have to be called in.
 * @param  None
 * @retval None
 */
void keyboard_function_set(void){


}


/**
 * @brief  Switch SHIFT/POWER for RETROCOMPATIBILI Keyboard Model:0x01
 * @note   This function have to be called in.
 * @param  None
 * @retval None
 */
void key_SHIFT_POWER(void){

	uint8_t id;
	uint8_t key_pres=0;
	//sostituire con tasto SHIFT in Hardware definitivo
	//key_pres=((NBN_KB_CSM.keys_status.btn.power==1)&&(NBN_KB_CSM.keys_status_old.btn.power==0));//detect press status

	if (NBN_KB_CSM.model==KEYB_CSM131){//RETROCOMPATIBILITA' Keyboard
		key_pres=((NBN_KB_CSM.keys_status.btn.power==1)&&(NBN_KB_CSM.keys_status_old.btn.power==0));//detect press status
		if (key_pres!=0){//Behavior as selector when pressed and released  change status
			NBN_KB_CSM.lock_unlock_key=((NBN_KB_CSM.lock_unlock_key==KEY_PRESSED)?KEY_RELEASED:KEY_PRESSED);
			if(NBN_KB_CSM.lock_unlock_key==KEY_PRESSED){
				NBN_KB_CSM.led_all[KEY_SHIFT_POWER_LED_ID].status=SET_LED_ON;
				NBN_KB_CSM.led_all[KEY_SHIFT_POWER_LED_ID].num_blink=0;//everlasting blinking
				NBN_KB_CSM.led_all[KEY_SHIFT_POWER_LED_ID].blink_lev=0;
				set_all_led_BKL_NBN(SET_LED_ON,0,0);

				NBN_KB_CSM.fading_duty=DUTY_FADING_MIN_LEV;
				NBN_KB_CSM.fading_inc_dec=1;
				NBN_KB_CSM.fading_time=FADING_TIME_100MS;

				NBN_KB_CSM.sleep_time=NBN_KB_CSM.gotosleep;
			}else{
				NBN_KB_CSM.led_all[KEY_SHIFT_POWER_LED_ID].status=SET_LED_OFF;
				NBN_KB_CSM.led_all[KEY_SHIFT_POWER_LED_ID].num_blink=0;//everlasting blinking
				NBN_KB_CSM.led_all[KEY_SHIFT_POWER_LED_ID].blink_lev=0;
				set_all_led_BKL_NBN(SET_LED_OFF,0,0);

				NBN_KB_CSM.fading_duty=DUTY_FADING_MIN_LEV;
				NBN_KB_CSM.fading_inc_dec=1;
				NBN_KB_CSM.fading_time=0;//FADING_TIME_100MS;

				NBN_KB_CSM.sleep_time=0;
			}
		}
		if(NBN_KB_CSM.lock_unlock_key==KEY_PRESSED){
		       if (NBN_KB_CSM.key_code!=KEY_NONE){
		    	   NBN_KB_CSM.sleep_time=NBN_KB_CSM.gotosleep;//refresh sleep counter
		       }
		}
		if(NBN_KB_CSM.sleep_time==0){//WAKEUP_TIME ELAPSED
			    NBN_KB_CSM.lock_unlock_key=KEY_RELEASED;
			    id=KEY_SHIFT_POWER_LED_ID;
				NBN_KB_CSM.led_all[id].status=SET_LED_OFF;
				NBN_KB_CSM.led_all[id].num_blink=0;//everlasting blinking
				NBN_KB_CSM.led_all[id].blink_lev=0;
				set_all_led_BKL_NBN(SET_LED_OFF,0,0);
//				NBN_KB_CSM.fading_duty=100;
//				NBN_KB_CSM.fading_inc_dec=0;
//				NBN_KB_CSM.fading_time=FADING_TIME_100MS;
				if (NBN_KB_CSM.key_code!=KEY_SHIFT)	{
				   NBN_KB_CSM.key_code=KEY_NONE;
				}
		}
    }else{

    	if(NBN_KB_CSM.lock_unlock_key==KEY_PRESSED){
			NBN_KB_CSM.led_all[KEY_SHIFT_POWER_LED_ID].status=SET_LED_ON;
			NBN_KB_CSM.led_all[KEY_SHIFT_POWER_LED_ID].num_blink=0;//everlasting blinking
			NBN_KB_CSM.led_all[KEY_SHIFT_POWER_LED_ID].blink_lev=0;

		}else{
			NBN_KB_CSM.led_all[KEY_SHIFT_POWER_LED_ID].status=SET_LED_OFF;
			NBN_KB_CSM.led_all[KEY_SHIFT_POWER_LED_ID].num_blink=0;//everlasting blinking
			NBN_KB_CSM.led_all[KEY_SHIFT_POWER_LED_ID].blink_lev=0;

			if (NBN_KB_CSM.key_code!=KEY_POWER)	{
				NBN_KB_CSM.key_code=KEY_NONE;
			}
		}

    }
	if(NBN_KB_CSM.lock_unlock_key==KEY_PRESSED){
	    /*Tasto SHIFT Toggle status*/
		key_pres=((NBN_KB_CSM.keys_status.btn.shift==0)&&(NBN_KB_CSM.keys_status_old.btn.shift==1));//detect press status
		if (key_pres!=0){//Behavior as selector when pressed and released  change status
			NBN_KB_CSM.shift_key=((NBN_KB_CSM.shift_key==KEY_PRESSED)?KEY_RELEASED:KEY_PRESSED);
			if(NBN_KB_CSM.shift_key==KEY_PRESSED){
				NBN_KB_CSM.led_all[KEY_SHIFT_ID].status=SET_LED_ON;
				NBN_KB_CSM.led_all[KEY_SHIFT_ID].num_blink=0;//everlasting blinking
				NBN_KB_CSM.led_all[KEY_SHIFT_ID].blink_lev=0;
			}else{
				NBN_KB_CSM.led_all[KEY_SHIFT_ID].status=SET_LED_OFF;
				//NBN_KB_CSM.sleep_time=0;
			}
		}
	}else{
		NBN_KB_CSM.shift_key=KEY_RELEASED;
		NBN_KB_CSM.led_all[KEY_SHIFT_ID].status=SET_LED_OFF;
	}

}

/**
 * @brief  Indicate the switch pressed
 * @note   This function have to be called in.
 * @param  None
 * @retval None
 */
unsigned char keyboard_function_manager(void){
unsigned char ret = KEY_NONE; //0;
NBN_KB_CSM.keys_stroke=0;
/*
 * 	KEY_NONE    = 0x00,//Nessun tasto
	KEY_M1UP    = 0x11,//M1 Avanti
	KEY_M2UP    = 0x12,//M2 Avanti
	KEY_M3UP    = 0x13,//M3 Avanti
	KEY_M4UP    = 0x14,//M4 Avanti
	KEY_M5UP_LR = 0x15,//M5 Avanti (left & right)
	KEY_M6UP    = 0x16,//M6 Avanti
	KEY_M7UP_LR = 0x17,//M7 Avanti (left & right)
	KEY_M7UP_LO = 0x18,//M7 Avanti (left only)
	KEY_M7UP_RO = 0x19,//M7 Avanti (right only)
	KEY_M5UP_LO = 0x1a,//M3 Avanti (left only)
	KEY_M5UP_RO = 0x1b,//M3 Avanti (right only)
	KEY_M1DN    = 0x21,//M1 Indietro
	KEY_M2DN    = 0x22,//M2 Indietro
	KEY_M3DN    = 0x23,//M3 Indietro
	KEY_M4DN    = 0x24,//M4 Indietro
	KEY_M5DN_LR = 0x25,//M5 Indietro (left & right)
	KEY_M6DN    = 0x26,//M6 Indietro
	KEY_M7DN_LR = 0x27,//M7 Indietro (left & right)
	KEY_M7DN_LO = 0x28,//M7 Indietro (left only)
	KEY_M7DN_RO = 0x29,//M7 Indietro (right only)
	KEY_M5DN_LO = 0x2a,//M3 indietro (left only)
	KEY_M5DN_RO = 0x2b,//M3 indietro (right only)
	KEY_ROMBO   = 0x31,//Rombo Tasto Store KEY_STORE KEY_ROMBO
	KEY_RECALL  = 0x32,//KEY_PUNTO
	KEY_ZERO    = 0x33,//Zero Tasto per Level
	KEY_RMEMA 	= 0x34,//Recall + Mem A / A2
	KEY_RMEMB 	= 0x35,//Recall + Mem B / B2
	KEY_RMEMC 	= 0x36,//Recall + Mem C / C2
	KEY_SMEMA 	= 0x37,//Store + Mem A / A2
	KEY_SMEMB   = 0x38,//Store + Mem B / B2
	KEY_SMEMC   = 0x39,//Store + Mem C / C2
	KEY_REFLEX =0x3A,//Reflex
	KEY_CHAIR  =0x3B,//Chair
	KEY_FLEX   =0x3C,//Flex //RECALL+TASTI MOTORE
	KEY_M1_ZR  =0x41,//M1 in posizione Zero
	KEY_M2_ZR  =0x42,//M2 in posizione Zero
	KEY_M3_ZR  =0x43,//M3 in posizione Zero
	KEY_M4_ZR  =0x44,//M4 in posizione Zero
	KEY_M5_ZR  =0x45,//M5 in posizione Zero
	KEY_M6_ZR  =0x46,//M6 in posizione Zero
	KEY_M7_ZR  =0x47,//M7 in posizione Zero
	KEY_JOY_UP_SX =0x48      ,//up + left
	KEY_JOY_UP_DX =0x49      , //up + right
	KEY_JOY_DN_DX =0x4a      ,//down + right
	KEY_JOY_DN_SX =0x4b      ,//down + left
	KEY_RMEMD  =0x54,//Recall + Mem D / D2
	KEY_RMEME  =0x55,//Recall + Mem E / E2
	KEY_RMEMF  =0x56,//Recall + Mem F / F2
	KEY_SMEMD  =0x57,//Store + Mem D / D2
	KEY_SMEME  =0x58,//Store + Mem E / E2
	KEY_SMEMF  =0x59,//Store + Mem F / F2

	if (NBN_KB_CSM.keys_status.btn.btn1==1){
		NBN_KB_CSM.keys_stroke++;
		ret=KEY_M1UP;
	}
	if (NBN_KB_CSM.keys_status.btn.btn2==1){
		NBN_KB_CSM.keys_stroke++;
		ret=2;
    }
	if (NBN_KB_CSM.keys_status.btn.btn3==1){
		NBN_KB_CSM.keys_stroke++;
		ret=3;
	}
	if (NBN_KB_CSM.keys_status.btn.btn4==1){
		NBN_KB_CSM.keys_stroke++;
  		ret=4;
	}
	if (NBN_KB_CSM.keys_status.btn.btn5==1){
		NBN_KB_CSM.keys_stroke++;
		ret=5;
	}
	if (NBN_KB_CSM.keys_status.btn.btn6==1){
		NBN_KB_CSM.keys_stroke++;
		ret=6;
	}
	if (NBN_KB_CSM.keys_status.btn.btn7==1){
		NBN_KB_CSM.keys_stroke++;
		ret=7;
	}
	if (NBN_KB_CSM.keys_status.btn.btn8==1){
			ret=8;
		}
	if (NBN_KB_CSM.keys_status.btn.btn9==1){
		NBN_KB_CSM.keys_stroke++;
		ret=9;
	}
	if (NBN_KB_CSM.keys_status.btn.btn10==1){
		NBN_KB_CSM.keys_stroke++;
		ret=10;
	}
	if (NBN_KB_CSM.keys_status.btn.btn11==1){
		NBN_KB_CSM.keys_stroke++;
		ret=11;
	}
	if (NBN_KB_CSM.keys_status.btn.btn12==1){
		NBN_KB_CSM.keys_stroke++;
		ret=12;
	}
	if (NBN_KB_CSM.keys_status.btn.btn13==1){
		NBN_KB_CSM.keys_stroke++;
		ret=13;
	}
	if (NBN_KB_CSM.keys_status.btn.btn14==1){
		NBN_KB_CSM.keys_stroke++;
		ret=14;
    }
	if (NBN_KB_CSM.keys_status.btn.btn15==1){
		NBN_KB_CSM.keys_stroke++;
		ret=15;

	}
	if (NBN_KB_CSM.keys_status.btn.btn16==1){
		NBN_KB_CSM.keys_stroke++;
		ret=16;
	}
	if (NBN_KB_CSM.keys_status.btn.btn17==1){
		NBN_KB_CSM.keys_stroke++;
		ret=17;
	}
	if (NBN_KB_CSM.keys_status.btn.btn18==1){
		NBN_KB_CSM.keys_stroke++;
		ret=18;
	}
	if (NBN_KB_CSM.keys_status.btn.btn19==1){
		NBN_KB_CSM.keys_stroke++;
		ret=19;
	}
	if (NBN_KB_CSM.keys_status.btn.btn20==1){
		NBN_KB_CSM.keys_stroke++;
		ret=20;
	}
	if (NBN_KB_CSM.keys_status.btn.btn21==1){
		NBN_KB_CSM.keys_stroke++;
		ret=21;
	}
	if (NBN_KB_CSM.keys_status.btn.btn22==1){
		NBN_KB_CSM.keys_stroke++;
		ret=22;
	}
	if (NBN_KB_CSM.keys_status.btn.btn23==1){
		NBN_KB_CSM.keys_stroke++;
		ret=23;
	}
	if (NBN_KB_CSM.keys_status.btn.btn24==1){
			ret=24;
		}
	if (NBN_KB_CSM.keys_status.btn.btn25==1){
		NBN_KB_CSM.keys_stroke++;
		ret=25;
	}
	if (NBN_KB_CSM.keys_status.btn.btn26==1){
		NBN_KB_CSM.keys_stroke++;
		ret=26;
	}
	if (NBN_KB_CSM.keys_status.btn.btn27==1){
		NBN_KB_CSM.keys_stroke++;
		ret=27;
	}

	if (NBN_KB_CSM.keys_status.btn.btn28==1){//BTN28_M3M6
		NBN_KB_CSM.keys_stroke++;
		ret=28;
	}

	if (NBN_KB_CSM.keys_status.btn.btn29==1){
		NBN_KB_CSM.keys_stroke++;
		ret=29;
	}
	if (NBN_KB_CSM.keys_status.btn.btn30==1){
		NBN_KB_CSM.keys_stroke++;
		ret=30;
	}
	if (NBN_KB_CSM.keys_status.btn.btn31==1){
		NBN_KB_CSM.keys_stroke++;
		ret=31;
	}
	if (NBN_KB_CSM.keys_status.btn.btn32==1){
		NBN_KB_CSM.keys_stroke++;
		ret=32;
	}
*/


	switch(NBN_KB_CSM.keys_status.all32){
			case(KEY_M1UP_BITMAP):
								   ret=KEY_M1UP;
								   break;
			case(KEY_M2UP_BITMAP):
								   ret=KEY_M2UP;
								   break;
			case(KEY_M3UP_BITMAP):
								   ret=KEY_M3UP;
								   break;
			case(KEY_M4UP_BITMAP):
								   ret=KEY_M4UP;
								   break;
			case(KEY_M5UP_BITMAP):
								   ret=KEY_M5UP_LR;
								   break;
			case(KEY_M6UP_BITMAP):
								   ret=KEY_M6UP;
								   break;
			case(KEY_M7UP_LR_BITMAP):
								   ret=KEY_M7UP_LR;
								   break;
			case(KEY_M7UP_LO_BITMAP):
								   ret=KEY_M7UP_LO;
								   break;
			case(KEY_M7UP_RO_BITMAP):
								   ret=KEY_M7UP_RO;
								   break;
			case(KEY_M5UP_LO_BITMAP):
								   ret=KEY_M5UP_LO;
								   break;
			case(KEY_M5UP_RO_BITMAP):
								   ret=KEY_M5UP_RO;
								   break;
			case(KEY_M1DN_BITMAP):
								   ret=KEY_M1DN;
								   break;
			case(KEY_M2DN_BITMAP):
								   ret=KEY_M2DN;
								   break;
			case(KEY_M3DN_BITMAP):
								   ret=KEY_M3DN;
								   break;
			case(KEY_M4DN_BITMAP):
								   ret=KEY_M4DN;
								   break;
			case(KEY_M5DN_LR_BITMAP):
								   ret=KEY_M5DN_LR;
								   break;
			case(KEY_M6DN_BITMAP):
								   ret=KEY_M6DN;
								   break;
			case(KEY_M7DN_LR_BITMAP):
								   ret=KEY_M7DN_LR;
								   break;
			case(KEY_M7DN_LO_BITMAP):
								   ret=KEY_M7DN_LO;
								   break;
			case(KEY_M7DN_RO_BITMAP):
								   ret=KEY_M7DN_RO;
								   break;
			case(KEY_M5DN_LO_BITMAP):
								   ret=KEY_M5DN_LO;
								   break;
			case(KEY_M5DN_RO_BITMAP):
								   ret=KEY_M5DN_RO;
								   break;
			case(KEY_ROMBO_BITMAP):
								   ret=KEY_ROMBO;
								   break;
			case(KEY_RECALL_BITMAP):
								   ret=KEY_RECALL;
								   break;
			case(KEY_ZERO_BITMAP):
											   ret=KEY_ZERO;
											   break;
			case(KEY_RMEMA_BITMAP):
											   ret=KEY_RMEMA;
			                                   if(NBN_KB_CSM.shift_key==KEY_PRESSED){
			                                	   ret=KEY_RMEMD;
			                                   }
											   break;
			case(KEY_RMEMB_BITMAP):
											   ret=KEY_RMEMB;
			                                   if(NBN_KB_CSM.shift_key==KEY_PRESSED){
												   ret=KEY_RMEME;
											   }
											   break;
			case(KEY_RMEMC_BITMAP):
											   ret=KEY_RMEMC;
			                                   if(NBN_KB_CSM.shift_key==KEY_PRESSED){
						                          ret=KEY_RMEMF;
						                       }
											   break;
			case(KEY_SMEMA_BITMAP):
											   ret=KEY_SMEMA;
			                                   if(NBN_KB_CSM.shift_key==KEY_PRESSED){
						                          ret=KEY_SMEMD;
						                       }
											   break;
			case(KEY_SMEMB_BITMAP):
											   ret=KEY_SMEMB;
			                                   if(NBN_KB_CSM.shift_key==KEY_PRESSED){
						                         ret=KEY_SMEME;
						                       }
											   break;
			case(KEY_SMEMC_BITMAP):
											   ret=KEY_SMEMC;
			                                   if(NBN_KB_CSM.shift_key==KEY_PRESSED){
						                         ret=KEY_SMEMF;
						                       }
											   break;
			case(KEY_REFLEX_BITMAP):
											   ret=KEY_REFLEX;
											   break;
			case(KEY_CHAIR_BITMAP):
											   ret=KEY_CHAIR;
											   break;
			case(KEY_FLEX_BITMAP):
											   ret=KEY_FLEX;
											   break;
			case(KEY_M1_ZR_BITMAP):
			case(KEY_M1_ZR1_BITMAP):
											   ret=KEY_M1_ZR;
											   break;
			case(KEY_M2_ZR_BITMAP):
			case(KEY_M2_ZR1_BITMAP):
											   ret=KEY_M2_ZR;
											   break;
			case(KEY_M3_ZR_BITMAP):
			case(KEY_M3_ZR1_BITMAP):
											   ret=KEY_M3_ZR;
											   break;
			case(KEY_M4_ZR_BITMAP):
			case(KEY_M4_ZR1_BITMAP):
											   ret=KEY_M4_ZR;
											   break;
			case(KEY_M5_ZR_BITMAP):
			case(KEY_M5_ZR1_BITMAP):
											   ret=KEY_M5_ZR;
											   break;
			case(KEY_M6_ZR_BITMAP):
			case(KEY_M6_ZR1_BITMAP):
											   ret=KEY_M6_ZR;
											   break;
			case(KEY_M7_ZR_BITMAP):
			case(KEY_M7_ZR1_BITMAP):
											   ret=KEY_M7_ZR;
											   break;

//			case(KEY_JOY_UP_SX_BITMAP):
//											   ret=KEY_JOY_UP_SX;
//											   break;
//			case(KEY_JOY_UP_DX_BITMAP):
//											   ret=KEY_JOY_UP_DX;
//											   break;
//			case(KEY_JOY_DN_DX_BITMAP):
//											   ret=KEY_JOY_DN_DX;
//											   break;
//			case(KEY_JOY_DN_SX_BITMAP):
//											   ret=KEY_JOY_DN_SX;
//											   break;
			case(KEY_TASTO1_BITMAP):
		                                       if (NBN_KB_CSM.model!=KEYB_CSM131){
											        ret=KEY_TASTO1;
		                                       }
											   break;
			case(KEY_TASTO2_BITMAP):
												if (NBN_KB_CSM.model!=KEYB_CSM131){
												   ret=KEY_TASTO2;
												}
											   break;
			case(KEY_TASTO3_BITMAP):
												if (NBN_KB_CSM.model!=KEYB_CSM131){
												   ret=KEY_TASTO3;
												}
											   break;
			case(KEY_TASTO4_BITMAP):
												if (NBN_KB_CSM.model!=KEYB_CSM131){
												   ret=KEY_TASTO4;
												}
											   break;
//			case(KEY_RMEMD_BITMAP):
//											   ret=KEY_RMEMD;
//											   break;
//			case(KEY_RMEME_BITMAP):
//											   ret=KEY_RMEME;
//											   break;
//			case(KEY_RMEMF_BITMAP):
//											   ret=KEY_RMEMF;
//											   break;
//			case(KEY_SMEMD_BITMAP):
//											   ret=KEY_SMEMD;
//											   break;
//			case(KEY_SMEME_BITMAP):
//											   ret=KEY_SMEME;
//											   break;
//			case(KEY_SMEMF_BITMAP):
//											   ret=KEY_SMEMF;
//											   break;
			case(KEY_SHIFT_BITMAP):
//		                                       if (NBN_KB_CSM.model==KEYB_CSM131){//RETROCOMPATIBILITA' Keyboard
//													ret = KEY_NONE;
//												}else{
//													ret=KEY_SHIFT;
//												}
		                                       ret=KEY_SHIFT;
											   break;
			case(KEY_LOCK_UNLCK_BITMAP):
												if (NBN_KB_CSM.model==KEYB_CSM131){//RETROCOMPATIBILITA' Keyboard
													ret=KEY_SHIFT;
												}else{
													ret = KEY_POWER;
												}
											   break;

	     default:
	    	     ret = KEY_NONE; //0;
	    	     break;
	}

	NBN_KB_CSM.key_code=ret;
	key_SHIFT_POWER();

    return (ret);

}
