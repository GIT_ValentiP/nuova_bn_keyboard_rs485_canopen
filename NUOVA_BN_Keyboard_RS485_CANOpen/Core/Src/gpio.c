/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"
/* USER CODE BEGIN 0 */
#include "globals.h"
extern TIM_HandleTypeDef htim4;
/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, SPI4_NSS2_AUX1_Pin|LED5_SERVICE_Pin|LED9_SERVICE_Pin|USB_VBUS_EN_Pin 
                          |LED3_SERVICE_Pin|LED4_SERVICE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(SPI4_NSS3_AUX1_GPIO_Port, SPI4_NSS3_AUX1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED7_SERVICE_GPIO_Port, LED7_SERVICE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(RS485_AUX1_RTS_GPIO_Port, RS485_AUX1_RTS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED6_SERVICE_Pin|LED1_SERVICE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, RS232_DBG_CTS_Pin|RS232_DBG_RTS_Pin|LED8_SERVICE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, RS485_CONTROL_RTS_Pin|RS485_CONTROL_CTS_Pin|LED2_SERVICE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : PEPin PEPin PEPin PEPin 
                           PEPin PEPin */
  GPIO_InitStruct.Pin = SPI4_NSS2_AUX1_Pin|LED5_SERVICE_Pin|LED9_SERVICE_Pin|USB_VBUS_EN_Pin 
                          |LED3_SERVICE_Pin|LED4_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = SPI4_NSS3_AUX1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(SPI4_NSS3_AUX1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PFPin PFPin PFPin PFPin 
                           PFPin PFPin PFPin PFPin 
                           PFPin PFPin */
  GPIO_InitStruct.Pin = BTN01_UP_Pin|BTN02_DOWN_Pin|BTN03_REV_Pin|BTN04_TREND_Pin 
                          |BTN05_TILT_L_Pin|BTN17_REFLEX_Pin|BTN18_CHAIR_Pin|BTN19_FLEX_Pin 
                          |BTN20_LEVEL_Pin|BTN23_STORE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = LED7_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED7_SERVICE_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PCPin PCPin PCPin PCPin */
  GPIO_InitStruct.Pin = BTN06_TILT_R_Pin|BTN07_TRASL_F_Pin|BTN13_LEG_UP_Pin|BTN14_LEG_DOWN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PAPin PAPin PAPin PAPin 
                           PAPin */
  GPIO_InitStruct.Pin = BTN08_TRASL_R_Pin|BTN09_BACKUP_Pin|BTN10_BACKDOWN_Pin|BTN11_THOR_UP_Pin 
                          |BTN12_THOR_DOWN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = RS485_AUX1_RTS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(RS485_AUX1_RTS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PBPin PBPin */
  GPIO_InitStruct.Pin = LED6_SERVICE_Pin|LED1_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = BTN16_LEG_R_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BTN16_LEG_R_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PGPin PGPin PGPin PGPin 
                           PGPin PGPin PGPin PGPin */
  GPIO_InitStruct.Pin = BTN24_M1M4_Pin|BTN25_RECALL_Pin|BTN_GP2_Pin|BTN_GP1_Pin 
                          |BTN32_OPT4_Pin|BTN31_OPT3_Pin|BTN30_OPT2_Pin|BTN29_OPT1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pins : PEPin PEPin */
  GPIO_InitStruct.Pin = BTN26_M2M5_Pin|BTN28_M3M6_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = USB_OVERCURRENT_ALARM_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USB_OVERCURRENT_ALARM_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PDPin PDPin PDPin PDPin 
                           PDPin */
  GPIO_InitStruct.Pin = BTN21_POWER_Pin|BTN27_SHIFT_Pin|BTN15_LEG_L_Pin|BTN_GP4_Pin 
                          |BTN_GP3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : PDPin PDPin PDPin */
  GPIO_InitStruct.Pin = RS232_DBG_CTS_Pin|RS232_DBG_RTS_Pin|LED8_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : PGPin PGPin PGPin */
  GPIO_InitStruct.Pin  = RS485_CONTROL_RTS_Pin|RS485_CONTROL_CTS_Pin|LED2_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;//GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = USB_OTG_HS_VBUS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USB_OTG_HS_VBUS_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 2 */
void led_service_init(void){



    HAL_GPIO_WritePin(LED1_SERVICE_GPIO_Port, LED1_SERVICE_Pin, GPIO_PIN_SET);//Pulsante Macine FORWARD

    HAL_GPIO_WritePin(LED2_SERVICE_GPIO_Port, LED2_SERVICE_Pin, GPIO_PIN_SET);//Pulsante Macine BACKWARD

	HAL_GPIO_WritePin(LED3_SERVICE_GPIO_Port, LED3_SERVICE_Pin, GPIO_PIN_SET);

	HAL_GPIO_WritePin(LED4_SERVICE_GPIO_Port, LED4_SERVICE_Pin, GPIO_PIN_SET);

	HAL_GPIO_WritePin(LED5_SERVICE_GPIO_Port, LED5_SERVICE_Pin, GPIO_PIN_SET);

    HAL_GPIO_WritePin(LED6_SERVICE_GPIO_Port, LED6_SERVICE_Pin, GPIO_PIN_SET);

	HAL_GPIO_WritePin(LED7_SERVICE_GPIO_Port, LED7_SERVICE_Pin, GPIO_PIN_SET);//ADC3_IN15 NTC

    HAL_GPIO_WritePin(LED8_SERVICE_GPIO_Port, LED8_SERVICE_Pin, GPIO_PIN_SET);

    HAL_GPIO_WritePin(LED9_SERVICE_GPIO_Port, LED9_SERVICE_Pin, GPIO_PIN_SET);


    /*ILUUMINA Led stripe*/

    htim4.Instance->ARR=990;
    htim4.Init.Period  =999;
   	htim4.Instance->CCR2=0;
    //illuminazione.pwm_duty=65;
    //illuminazione.cmd.b.DUTY=1;

}

void led_service_ON_OFF(unsigned char id_led,unsigned char status){
 GPIO_PinState set_status_led;
 set_status_led=(status>0 ?GPIO_PIN_SET: GPIO_PIN_RESET);
 if (id_led < 10){
	 switch	(id_led){
		 case(1):
						HAL_GPIO_WritePin(LED1_SERVICE_GPIO_Port, LED1_SERVICE_Pin, set_status_led);
						break;
		 case(2):
						HAL_GPIO_WritePin(LED2_SERVICE_GPIO_Port, LED2_SERVICE_Pin, set_status_led);
						break;
		 case(3):
						HAL_GPIO_WritePin(LED3_SERVICE_GPIO_Port, LED3_SERVICE_Pin, set_status_led);
						break;
		 case(4):
						HAL_GPIO_WritePin(LED4_SERVICE_GPIO_Port, LED4_SERVICE_Pin, set_status_led);
						break;
		 case(5):
						HAL_GPIO_WritePin(LED5_SERVICE_GPIO_Port, LED5_SERVICE_Pin, set_status_led);
						break;
		 case(6):
						HAL_GPIO_WritePin(LED6_SERVICE_GPIO_Port, LED6_SERVICE_Pin, set_status_led);
				    	break;
		 case(7):
						HAL_GPIO_WritePin(LED7_SERVICE_GPIO_Port, LED7_SERVICE_Pin, set_status_led); //NTC ADC3_IN15
						break;
		 case(8):
						HAL_GPIO_WritePin(LED8_SERVICE_GPIO_Port, LED8_SERVICE_Pin, set_status_led);
						break;
		 case(9):
						HAL_GPIO_WritePin(LED9_SERVICE_GPIO_Port, LED9_SERVICE_Pin, set_status_led);
						break;
	 }
 }



}


void illumina_manager(void){



}

void led_battery_init(void){
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOE, LED35_LEV3_BAT_TIM1_1_Pin|LED36_LEV4_BAT_TIM1_2_Pin|LED33_LEV1_BAT_TIM1_3_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, LED34_LEV2_BAT_TIM3_4_Pin|PWM1_TIM3_2_Pin , GPIO_PIN_RESET);
	/*Configure GPIO pins : PEPin PEPin PEPin PEPin PEPin PEPin */
	GPIO_InitStruct.Pin = LED35_LEV3_BAT_TIM1_1_Pin|LED36_LEV4_BAT_TIM1_2_Pin|LED33_LEV1_BAT_TIM1_3_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LED34_LEV2_BAT_TIM3_4_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = PWM1_TIM3_2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

void led_battery_set(void){
	/*Configure GPIO pin Output Level */
	if (NBN_KB_CSM.model>KEYB_CSM131){//RETROCOMPATIBILITA' Keyboard

//	   switch(NBN_KB_CSM.bat_lev){
//	      case(BAT_LEV_25):
//		          HAL_GPIO_WritePin(GPIOE, LED35_LEV3_BAT_TIM1_1_Pin|LED36_LEV4_BAT_TIM1_2_Pin, GPIO_PIN_RESET);
//		          HAL_GPIO_WritePin(GPIOE, LED33_LEV1_BAT_TIM1_3_Pin, GPIO_PIN_SET);
//			      HAL_GPIO_WritePin(GPIOB, LED34_LEV2_BAT_TIM3_4_Pin, GPIO_PIN_RESET);
//	    		  break;
//	      case(BAT_LEV_50):
//					HAL_GPIO_WritePin(GPIOE, LED35_LEV3_BAT_TIM1_1_Pin|LED36_LEV4_BAT_TIM1_2_Pin, GPIO_PIN_RESET);
//					HAL_GPIO_WritePin(GPIOE, LED33_LEV1_BAT_TIM1_3_Pin, GPIO_PIN_SET);
//					HAL_GPIO_WritePin(GPIOB, LED34_LEV2_BAT_TIM3_4_Pin, GPIO_PIN_SET);
//	    		  break;
//	      case(BAT_LEV_75):
//		            HAL_GPIO_WritePin(GPIOE, LED36_LEV4_BAT_TIM1_2_Pin, GPIO_PIN_RESET);
//					HAL_GPIO_WritePin(GPIOE, LED35_LEV3_BAT_TIM1_1_Pin|LED33_LEV1_BAT_TIM1_3_Pin, GPIO_PIN_SET);
//					HAL_GPIO_WritePin(GPIOB, LED34_LEV2_BAT_TIM3_4_Pin, GPIO_PIN_SET);
//	    		  break;
//	      case(BAT_LEV_100):
//					HAL_GPIO_WritePin(GPIOE, LED35_LEV3_BAT_TIM1_1_Pin|LED36_LEV4_BAT_TIM1_2_Pin|LED33_LEV1_BAT_TIM1_3_Pin, GPIO_PIN_SET);
//					HAL_GPIO_WritePin(GPIOB, LED34_LEV2_BAT_TIM3_4_Pin, GPIO_PIN_SET);
//					break;
//	      default://BAT_LEV_0  =0x00,
//
//	    	      HAL_GPIO_WritePin(GPIOE, LED35_LEV3_BAT_TIM1_1_Pin|LED36_LEV4_BAT_TIM1_2_Pin|LED33_LEV1_BAT_TIM1_3_Pin, GPIO_PIN_RESET);
//	    	  	  HAL_GPIO_WritePin(GPIOB, LED34_LEV2_BAT_TIM3_4_Pin, GPIO_PIN_RESET);
//
//	    	      break;
//	   }
       if(NBN_KB_CSM.bat_led.led.led_25==1){
    	   HAL_GPIO_WritePin(GPIOE, LED33_LEV1_BAT_TIM1_3_Pin, GPIO_PIN_SET);
       }else{
    	   HAL_GPIO_WritePin(GPIOE, LED33_LEV1_BAT_TIM1_3_Pin, GPIO_PIN_RESET);
       }
       if(NBN_KB_CSM.bat_led.led.led_50==1){
    	       HAL_GPIO_WritePin(GPIOB, LED34_LEV2_BAT_TIM3_4_Pin, GPIO_PIN_SET);
       }else{
    	       HAL_GPIO_WritePin(GPIOB, LED34_LEV2_BAT_TIM3_4_Pin, GPIO_PIN_RESET);
             }
       if(NBN_KB_CSM.bat_led.led.led_75==1){
    	       HAL_GPIO_WritePin(GPIOE, LED35_LEV3_BAT_TIM1_1_Pin, GPIO_PIN_SET);
       }else{
    	       HAL_GPIO_WritePin(GPIOE, LED35_LEV3_BAT_TIM1_1_Pin, GPIO_PIN_RESET);
             }
       if(NBN_KB_CSM.bat_led.led.led_100==1){
    	      HAL_GPIO_WritePin(GPIOE, LED36_LEV4_BAT_TIM1_2_Pin, GPIO_PIN_SET);
       }else{
    	      HAL_GPIO_WritePin(GPIOE, LED36_LEV4_BAT_TIM1_2_Pin, GPIO_PIN_RESET);
            }

	}
	if (NBN_KB_CSM.buzzer_time>0){
		HAL_GPIO_WritePin(GPIOB, PWM1_TIM3_2_Pin , GPIO_PIN_SET);
	}else{
		HAL_GPIO_WritePin(GPIOB, PWM1_TIM3_2_Pin , GPIO_PIN_RESET);
	}
}



/* USER CODE END 2 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
