/**
  ******************************************************************************
  * File Name          : ADC.c
  * Description        : This file provides code for the configuration
  *                      of the ADC instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "adc.h"

/* USER CODE BEGIN 0 */
#include "main.h"
#include "globals.h"

#define NTC_TEMP_DEG_START    -10

#define NTC_TEMP_DEG_END      120

#define NTC_NUM_VALUE_MAX     131

#define NTC_ADC_VAL_MIN        10
#define NTC_ADC_VAL_MAX      4085

const uint8_t NUM_ADC_CH_SAMPLE[]={5,3,3};

const  uint16_t NTC_TEMP_DEG_ADC_VAL[]={
4037, 4034, 4031, 4028, 4025, 4021, 4018, 4014, 4010, 4006, 4002, 3997, 3993, 3988, 3983, 3978, 3973, 3967, 3961, 3955,
3949, 3943, 3936, 3929, 3922, 3915, 3907, 3899, 3891, 3883, 3874, 3865, 3856, 3846, 3836, 3826, 3816, 3805, 3794, 3783,
3771, 3759, 3746, 3734, 3721, 3707, 3693, 3679, 3665, 3650, 3635, 3619, 3603, 3587, 3570, 3553, 3535, 3518, 3499, 3481,
3462, 3443, 3423, 3403, 3382, 3362, 3340, 3319, 3297, 3275, 3252, 3230, 3206, 3183, 3159, 3135, 3110, 3086, 3061, 3035,
3010, 2984, 2958, 2932, 2905, 2878, 2851, 2824, 2797, 2769, 2742, 2714, 2686, 2658, 2630, 2601, 2573, 2544, 2516, 2487,
2459, 2430, 2401, 2373, 2344, 2315, 2287, 2258, 2230, 2201, 2173, 2144, 2116, 2088, 2060, 2032, 2005, 1977, 1950, 1922,
1895, 1868, 1842, 1815, 1789, 1763, 1737, 1711, 1686, 1661, 1636,
};

/* USER CODE END 0 */

ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

/* ADC1 init function */
void MX_ADC1_Init(void)
{
  ADC_ChannelConfTypeDef sConfig = {0};
  ADC_InjectionConfTypeDef sConfigInjected = {0};

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 3;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_VREFINT;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_10;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Rank = ADC_REGULAR_RANK_3;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configures for the selected ADC injected channel its corresponding rank in the sequencer and its sample time 
  */
  sConfigInjected.InjectedChannel = ADC_CHANNEL_10;
  sConfigInjected.InjectedRank = ADC_INJECTED_RANK_1;
  sConfigInjected.InjectedNbrOfConversion = 2;
  sConfigInjected.InjectedSamplingTime = ADC_SAMPLETIME_3CYCLES;
  sConfigInjected.ExternalTrigInjecConvEdge = ADC_EXTERNALTRIGINJECCONVEDGE_NONE;
  sConfigInjected.ExternalTrigInjecConv = ADC_INJECTED_SOFTWARE_START;
  sConfigInjected.AutoInjectedConv = DISABLE;
  sConfigInjected.InjectedDiscontinuousConvMode = DISABLE;
  sConfigInjected.InjectedOffset = 0;
  if (HAL_ADCEx_InjectedConfigChannel(&hadc1, &sConfigInjected) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configures for the selected ADC injected channel its corresponding rank in the sequencer and its sample time 
  */
  sConfigInjected.InjectedRank = ADC_INJECTED_RANK_2;
  if (HAL_ADCEx_InjectedConfigChannel(&hadc1, &sConfigInjected) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_ADC_MspInit(ADC_HandleTypeDef* adcHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(adcHandle->Instance==ADC1)
  {
  /* USER CODE BEGIN ADC1_MspInit 0 */

  /* USER CODE END ADC1_MspInit 0 */
    /* ADC1 clock enable */
    __HAL_RCC_ADC1_CLK_ENABLE();
  
    __HAL_RCC_GPIOC_CLK_ENABLE();
    /**ADC1 GPIO Configuration    
    PC0     ------> ADC1_IN10
    PC3     ------> ADC1_IN13 
    */
    GPIO_InitStruct.Pin = ADC1_AN10_Pin|ADC1_AN13_NTC_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /* ADC1 DMA Init */
    /* ADC1 Init */
    hdma_adc1.Instance = DMA2_Stream4;
    hdma_adc1.Init.Channel = DMA_CHANNEL_0;
    hdma_adc1.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_adc1.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_adc1.Init.MemInc = DMA_MINC_ENABLE;
    hdma_adc1.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
    hdma_adc1.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
    hdma_adc1.Init.Mode = DMA_NORMAL;
    hdma_adc1.Init.Priority = DMA_PRIORITY_LOW;
    hdma_adc1.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
    if (HAL_DMA_Init(&hdma_adc1) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(adcHandle,DMA_Handle,hdma_adc1);

  /* USER CODE BEGIN ADC1_MspInit 1 */

  /* USER CODE END ADC1_MspInit 1 */
  }
}

void HAL_ADC_MspDeInit(ADC_HandleTypeDef* adcHandle)
{

  if(adcHandle->Instance==ADC1)
  {
  /* USER CODE BEGIN ADC1_MspDeInit 0 */

  /* USER CODE END ADC1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_ADC1_CLK_DISABLE();
  
    /**ADC1 GPIO Configuration    
    PC0     ------> ADC1_IN10
    PC3     ------> ADC1_IN13 
    */
    HAL_GPIO_DeInit(GPIOC, ADC1_AN10_Pin|ADC1_AN13_NTC_Pin);

    /* ADC1 DMA DeInit */
    HAL_DMA_DeInit(adcHandle->DMA_Handle);

    /* ADC1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(ADC_IRQn);
  /* USER CODE BEGIN ADC1_MspDeInit 1 */

  /* USER CODE END ADC1_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */
void adc_init(void){

ADC_ChannelConfTypeDef sConfig = {0};

uint8_t id;

/** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
	  */
	 hadc1.Instance = ADC1;
	 hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
	 hadc1.Init.Resolution = ADC_RESOLUTION_12B;
	 hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;//ADC_SCAN_DISABLE;//
	 hadc1.Init.ContinuousConvMode =ENABLE;
	 hadc1.Init.DiscontinuousConvMode = DISABLE;
	 hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	 hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;//ADC_EXTERNALTRIGCONV_T1_CC1;//
	 hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	 hadc1.Init.NbrOfConversion = NUM_MAX_ADC_CH;
	 hadc1.Init.DMAContinuousRequests = ENABLE;//DISABLE;//
	 hadc1.Init.EOCSelection = ADC_EOC_SEQ_CONV;//ADC_EOC_SINGLE_CONV;//DISABLE;//

	  if (HAL_ADC_Init(&hadc1) != HAL_OK)
	  {
		  Error_Handler();//Error_Handler(SYSTEM_ADC_CONFIG_1_ERROR);
	  }

    for(id=0;id<NUM_MAX_ADC_CH;id++){
		switch(id){
		case(NTC_ADC1_CH13):
							 adc123_ch[id].ADCIstance=ADC1;
							 adc123_ch[id].ADC_ch=ADC_CHANNEL_13;
							 adc123_ch[id].id_rank=ADC_REGULAR_RANK_1;
							 adc123_ch[id].sampling_time=ADC_SAMPLETIME_3CYCLES;
							 break;

		case(VREF_ADC1_CH17):
							 adc123_ch[id].ADCIstance=ADC1;
							 adc123_ch[id].ADC_ch=ADC_CHANNEL_17;
							 adc123_ch[id].id_rank=ADC_REGULAR_RANK_2;
							 adc123_ch[id].sampling_time=ADC_SAMPLETIME_3CYCLES;
							 break;


		case(ENC_ADC1_CH10):
							 adc123_ch[id].ADCIstance=ADC1;
							 adc123_ch[id].ADC_ch=ADC_CHANNEL_10;
							 adc123_ch[id].id_rank=ADC_REGULAR_RANK_3;
							 adc123_ch[id].sampling_time=ADC_SAMPLETIME_3CYCLES;
							 break;
		default:
							break;
		}
		adc123_ch[id].filtered_value = 0;
		adc123_ch[id].num_sample = 0;
		adc123_ch[id].raw_value  = 0;
		adc123_ch[id].sample_raw_sum = 0;
		/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
		*/
		sConfig.Channel = adc123_ch[id].ADC_ch;
		sConfig.Rank =  adc123_ch[id].id_rank;
		sConfig.SamplingTime =  adc123_ch[id].sampling_time ;
		if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
		{
			Error_Handler();//Error_Handler(SYSTEM_ADC_CONFIG_2_ERROR);
		}
     }//end_for




	  HAL_ADC_MspInit(&hadc1);

	  /*##-3- Start the conversion process #######################################*/
	                                                         //
	   if(HAL_ADC_Start_DMA(&hadc1, (uint32_t*)&adc_raw_value, NUM_MAX_ADC_CH) != HAL_OK)
	   {
		 /* Start Conversation Error */
		   Error_Handler();//Error_Handler(SYSTEM_ADC_CONFIG_3_ERROR);
	   }

	   /* DMA2_Stream4_IRQn interrupt configuration */
		HAL_NVIC_SetPriority(DMA2_Stream4_IRQn, 4, 0);
		HAL_NVIC_EnableIRQ(DMA2_Stream4_IRQn);

}


void adc_update(void){
	uint8_t id;
	HAL_ADC_Stop_DMA(&hadc1);

	for(id=0;id<NUM_MAX_ADC_CH;id++){
		switch(id){
			case(NTC_ADC1_CH13):
								 //adc123_ch[id].filtered_value = 0;
								 //adc123_ch[id].num_sample++;
								 if(adc123_ch[id].num_sample==1){
									 if((adc123_ch[id].filtered_value >NTC_ADC_VAL_MIN)&&(adc123_ch[id].filtered_value <NTC_ADC_VAL_MAX)){
										 int16_t temp_deg=NTC_TEMP_DEG_START;
										 //int16_t start_deg;
										 uint8_t cell_deg=0;
										 while(cell_deg<NTC_NUM_VALUE_MAX){

											 if(NTC_TEMP_DEG_ADC_VAL[cell_deg] < adc123_ch[id].filtered_value){
												 temp_sens[NTC_TEMP_SENS].temperature=temp_deg;
												 cell_deg=NTC_NUM_VALUE_MAX;
											 }
											 cell_deg++;
											 temp_deg++;
										 }
									 }
								 }
								 break;

			//case(VREF_ADC1_CH17):      break;
			//case(ENC_ADC1_CH10):break;
			default:
								break;
		}

	  //if(id < 3){
		 adc123_ch[id].raw_value  =adc_raw_value[id];
		 adc123_ch[id].sample_raw_sum += adc_raw_value[id];
		 if( ++adc123_ch[id].num_sample>(NUM_ADC_CH_SAMPLE[id]-1)){
			 adc123_ch[id].filtered_value =  (adc123_ch[id].sample_raw_sum/NUM_ADC_CH_SAMPLE[id]) ;
			 adc123_ch[id].millivolt=(((uint32_t)adc123_ch[id].filtered_value*3300)/4095);
			 adc123_ch[id].sample_raw_sum = 0;
			 adc123_ch[id].num_sample =0;
		 }
	  //}

	}
	/*##-3- Start the conversion process #######################################*/
														 //NUM_MAX_ADC_CH
	if(HAL_ADC_Start_DMA(&hadc1, (uint32_t*)&adc_raw_value, NUM_MAX_ADC_CH) != HAL_OK)
	{
	 /* Start Conversation Error */
		Error_Handler();//Error_Handler(SYSTEM_ADC_CONFIG_3_ERROR);
	}
}

/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
