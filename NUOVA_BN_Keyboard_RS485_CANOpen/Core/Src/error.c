/*
 ******************************************************************************
 *  @file      : error.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 20 gen 2020
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "error.h"
#include "usart.h"
#include "tim.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private user code ---------------------------------------------------------*/

void error_set(REVO_ERROR_enum_t cat,uint8_t id_cat,uint8_t code){
//	REVO_ERROR_enum_t  revo_error_cat;
//	word_bit_t  errorList[NUM_ERROR_CATEGORY];
//	error_cat_t error_cat;
	uint8_t id_err;
	uint16_t err_details;
	switch(cat){
	  case(ERROR_COMMUNICATION) :
			                     id_err=code;
		                         err_details=(0x0001<<id_err);
		                         errorList[0].word16 |=err_details;
		                         error_cat.cat.communication=1;
			                     break;
	  case(ERROR_LOADCELL) :
	 			                     id_err=code-10;
	 		                         err_details=(0x0001<<id_err);
	 		                         errorList[1].word16 |=err_details;
	 		                         error_cat.cat.load_cell=1;
	 			                     break;
	  case(ERROR_MOTOR) :
	 			                    if(code<36){
	 			                     id_err=code-20;
	 		                         err_details=(0x0001<<id_err);
	 		                         errorList[2].word16 |=err_details;
	 			                    }else{
	 			                    	 id_err=code-36;
										 err_details=(0x0001<<id_err);
										 errorList[3].word16 |=err_details;
	 			                    }
	 		                         error_cat.cat.motor=1;
	 			                     break;
	  case(ERROR_SENSOR) :
	 			                     id_err=code-40;
	 		                         err_details=(0x0001<<id_err);
	 		                         errorList[5].word16 |=err_details;
	 		                         error_cat.cat.sensor=1;
	 			                     break;
	  case(ERROR_WORKFLOW) :
	 			                     id_err=code-50;
	 		                         err_details=(0x0001<<id_err);
	 		                         errorList[6].word16 |=err_details;
	 		                         error_cat.cat.workflow=1;
	 			                     break;


	  default:
		             break;
	}

}





void error_reset(){

	error_cat.err_cat_byte=0x00;
	errorList[0].word16=0x0000;
	errorList[1].word16=0x0000;
	errorList[2].word16=0x0000;
	errorList[3].word16=0x0000;
	errorList[4].word16=0x0000;
	errorList[5].word16=0x0000;
	errorList[6].word16=0x0000;

}


void error_print(void){
	uint16_t id,mask;

	if (error_cat.err_cat_byte !=0){
		if (error_cat.cat.communication==1){
			printf("\n\rError COMMUNICATION:");
			mask=0x0001;
			for(id=0;id<16;id++){
				if((errorList[0].word16 & mask)!=0x0000){
					printf(" %d",id);
				}
				mask<<=1;
				printf(" , ");
			}
		}
		if (error_cat.cat.load_cell==1){
				printf("\n\rError LOAD CELL:");
				mask=0x0001;
				for(id=0;id<16;id++){
					if((errorList[1].word16 & mask)!=0x0000){
						printf("%d",(10+id));
					}
					mask<<=1;
					printf(" , ");
		        }
	    }

		if (error_cat.cat.motor==1){
				printf("\n\rError MOTOR:");
				mask=0x0001;
				for(id=0;id<16;id++){
					if((errorList[2].word16 & mask)!=0x0000){
						printf("%d",(20+id));
					}
					mask<<=1;
					printf(" , ");
				}
		}
		if (error_cat.cat.sensor==1){
					printf("\n\rError SENSORs:");
					mask=0x0001;
					for(id=0;id<16;id++){
						if((errorList[5].word16 & mask)!=0x0000){
							printf(" %d",(40+id));
						}
						mask<<=1;
						printf(" , ");
					}
				}
				if (error_cat.cat.workflow==1){
						printf("\n\rError PROCESSING:");
						mask=0x0001;
						for(id=0;id<16;id++){
							if((errorList[6].word16 & mask)!=0x0000){
								printf("%d",(50+id));
							}
							mask<<=1;
							printf(" , ");
				        }
			    }

				if (error_cat.cat.general==1){
						printf("\n\rError GENERIC:");
						mask=0x0001;
						for(id=0;id<16;id++){
							if((errorList[0].word16 & mask)!=0x0000){
								printf("%d",(70+id));
							}
							mask<<=1;
							printf(" , ");
						}
				}

	}else{
		printf("\n\rNo Error");
	}
}
