/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "can.h"
#include "dma.h"
#include "fatfs.h"
#include "i2c.h"
#include "iwdg.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "usb_host.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "mdb_com.h"
//#include "ifx9201sg_doser.h"
//#include "fan.h"
//#include "st_l6474_stepper.h"
//#include "balance.h"
//#include "grinder.h"
#include "sts3x.h"
#include "sht3x.h"
#include "scheduler_main.h"
#include "./PCA9685_LED_DRIVER/pca9685.h"
#include "input_keyboard.h"
#include "eeprom.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
//__attribute__((section(".projectvars"))) const uint32_t  VERSION_NUMBER = ((FW_VERSION<<8)| FW_SUBVERSION);
//__attribute__((section(".projectvars"))) const uint32_t  CRC_PROGRAM= 0x000000000;
//__attribute__((section(".projectvars"))) const uint16_t  BUILD_ID=0xA5A5;
//__attribute__((section(".projectvars"))) const  uint8_t  OTHER_VAR=0x00;
#define FLASH_APP_ADDRESS 0x8040000

//#define HSE_SOURCE_CLK_16MHZ
#define HSE_SOURCE_CLK_12MHZ
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
static volatile uint16_t gLastError;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_NVIC_Init(void);
void MX_USB_HOST_Process(void);

/* USER CODE BEGIN PFP */
void CPU_CACHE_Disable(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#ifdef BOOTLOADER_ACTIVE
/*If BOOTLOADER ACTIVE CHANGE VALUE in file system_stm32f7xx.c*/
#define VECT_TAB_OFFSET  0x40000 //
#endif
uint8_t i;


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
#ifdef BOOTLOADER_ACTIVE
	HAL_DeInit();
	SCB->VTOR = FLASH_APP_ADDRESS; /* Vector Table Relocation in Internal FLASH */
#endif
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

	/* Disable the CPU Cache */
	 // CPU_CACHE_Disable();

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
 // MX_ADC1_Init();
 // MX_I2C1_Init();
 // MX_I2C2_Init();
  MX_I2C3_Init();
//  MX_SPI4_Init();
  //MX_TIM1_Init();PWM LED BATTERY
  //MX_TIM3_Init();PWM LED BATTERY
  MX_TIM4_Init();
//  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_USART6_UART_Init();
  MX_IWDG_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_TIM10_Init();
  MX_TIM11_Init();
  MX_TIM13_Init();
  MX_TIM14_Init();
//  MX_FATFS_Init();
//  MX_USB_HOST_Init();
  MX_TIM5_Init();
//  MX_CAN1_Init();
//  MX_CAN2_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */
  MX_USART3_UART_Init();
  /*LED Init*/
  // Initialise driver (performs basic setup).
  HAL_IWDG_Refresh(&hiwdg);
  pca9685_begin();
  pca9685_init(&pca9685_led[0]);
  pca9685_refresh_led(LED_TASTO_TYPE);
  pca9685_refresh_led(LED_BKL_TYPE);

  led_battery_init();
  led_service_init();
  /* Unlock the Flash Program Erase controller */
  HAL_FLASH_Unlock();
  /* EEPROM Init */
  HAL_IWDG_Refresh(&hiwdg);
  EE_SetDeafultValues();
  if( EE_Init() != EE_OK){
      Error_Handler();
  }
  HAL_IWDG_Refresh(&hiwdg);
  EE_ReadInitValues();
  /*KEYBOARD Init*/
	init_input_reayboard();
	NBN_KB_CSM.sleep_time =  0;// GOTO_SLEEP_TIME;
	NBN_KB_CSM.lock_unlock_key=KEY_RELEASED;
	NBN_KB_CSM.shift_key=KEY_RELEASED;
	NBN_KB_CSM.protocol = PROTOCOL_RS485;
	if (NBN_KB_CSM.model==KEYB_CSM131){//RETROCOMPATIBILITA' Keyboard
	   NBN_KB_CSM.keys_enabled.all32=NBN_MASK_ENABLED_KEYS_CSM131;
	   NBN_KB_CSM.keys_logic.all32  =NBN_MASK_LOGIC_KEYS_CSM125;
	}else{//All KEYS are enabled
	   NBN_KB_CSM.keys_enabled.all32=NBN_MASK_ENABLED_KEYS_CSM125;
	   NBN_KB_CSM.keys_logic.all32  =NBN_MASK_LOGIC_KEYS_CSM125;
	}
	NBN_KB_CSM.led_all[LED_CHARGE_ID].status =SET_LED_OFF;
	NBN_KB_CSM.bat_lev=BAT_LEV_0;
    NBN_KB_CSM.fading_off_time=0;

	// Set PWM frequency.
	// The frequency must be between 24Hz and 1526Hz.
//	pca9685_set_pwm_frequency(&pca9685_led[0], 100.0f);
//	pca9685_set_pwm_frequency(&pca9685_led[1], 100.0f);
//	pca9685_set_pwm_frequency(&pca9685_led[2], 100.0f);
//	pca9685_set_pwm_frequency(&pca9685_led[3], 100.0f);
	// Set the channel on and off times.
	// The channel must be >= 0 and < 16.
	// The on and off times must be >= 0 and < 4096.
	// In this example, the duty cycle is (off time - on time) / 4096 = (2048 - 0) / 4096 = 50%
	//pca9685_set_channel_pwm_times(&pca9685_led, 0, 0, 2048);

	// If you do not want to set the times by hand you can directly set the duty cycle. The last parameter
	// lets you use a logarithmic scale for LED dimming applications.
	//pca9685_set_channel_duty_cycle(pca9685_led, 1, 0.5f, false);
	//pca9685_set_group_duty_cycle(&pca9685_led[0],0,12, 1.00f, false);

    /*TIMERs Init*/
	  //TIM_PWM_Init();
      HAL_IWDG_Refresh(&hiwdg);
	  Main_Clock_Init();
	  HAL_TIM_Base_MspInit(&htim14);
	  HAL_TIM_Base_MspInit(&htim13);
	  flag_1min=0;

	  /*MODBUS HMI Init*/
	  version_fw[0]= ((FW_VERSION<<8)| FW_SUBVERSION);//VERSION_NUMBER;
	  version_fw[1]= 0x0115;//FW2 version
	  version_fw[2]= 0;//FW3 version

	  printf("\n\r *** NUOVA BN FW Ver: %3d.%3d ***\n\r",FW_VERSION,FW_SUBVERSION);
	  DP_SW_RS485_adr=NBN_KEYBOARD_RS485_ADDR;//REVO3_HMI_RS485_ADDR;//address  to identify  Keyboard on RS485 ModBus
	  MX_USART6_UART_Init();
	  HAL_IWDG_Refresh(&hiwdg);
	  mdb_serial_setSlaveAddress(DP_SW_RS485_adr);
	  mdb_comm_Initialize(); //Modbus Communication Init
	  RS485_TX_HMI_DISABLE;
	  RS485_RX_HMI_ENABLE;
	  HAL_NVIC_SetPriority(DMA2_Stream1_IRQn, 0, 0); //HMI UART6 RX DMA
	  HAL_NVIC_EnableIRQ(DMA2_Stream1_IRQn);
	  HAL_NVIC_SetPriority(DMA2_Stream6_IRQn, 0, 0);//HMI UART6 TX DMA
	  HAL_NVIC_EnableIRQ(DMA2_Stream6_IRQn);
	  /* USART6_IRQn interrupt configuration */
	  HAL_NVIC_SetPriority(USART6_IRQn, 0, 0);
	  HAL_NVIC_EnableIRQ(USART6_IRQn);

  /*Main status*/

	  last_led_type=LED_TASTO_TYPE;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {

	//printf("\n\rSTART MAIN LOOP");

	//printf("\n\rRead sensor");
    //read_sensor_hall();
	  HAL_IWDG_Refresh(&hiwdg);
    //printf("\n\rModbus processed");
	  runtimer_ms=0;
      mdb_comm_processMessage();/*Modbus poll update in each run*/
      time_elapsed[0]=runtimer_ms;
//    printf("\n\rMain scheduler Task");
//    scheduler_task();//MAIN SCHEDULER ENGINE

    //printf("\n\rLeds:service and strip set");

      logo_fading (NBN_KB_CSM.led_bkl[LED_LOGO_ID].status);
      time_elapsed[1]=runtimer_ms;

	  if (flag_10ms==1){
		  time_elapsed[2]=runtimer_ms;
		  read_input_keyboard();
		  keyboard_function_manager();
		  time_elapsed[3]=runtimer_ms;
	 	  flag_10ms=0;
	  }

	  if (flag_100ms==1){
		  time_elapsed[4]=runtimer_ms;
		  last_led_type=((last_led_type==LED_TASTO_TYPE)?LED_BKL_TYPE:LED_TASTO_TYPE);
		  pca9685_refresh_led(last_led_type);
		  //pca9685_refresh_led_fast();
		  time_elapsed[5]=runtimer_ms;
		  led_battery_set();
		  //printf("Start led:%d\n\r",time_elapsed[4]);
		  //printf("End led:%d\n\r",time_elapsed[5]);
		  flag_100ms=0;
	  }
    //if(revo_state==REVO_READY_TO_USE){//(revo_board_conf !=REVO_NOT_SET_CONFIG){
	  if (flag_1sec==1){
			//printf("\n\r BurrAngle :%d� Zero:%d�",encoder_burr.angle_pos,encoder_burr.angle_zero_pos);
			//printf("\n\r Burr Pos.:%d[um] Delta Pos.:%d[um]",hopper_burr[BURR_0].burr_distance_rd,encoder_burr.micron_moved_pos);//encoder_burr.pre_grind_pos);
			//sensor_sts3x_update();
			//sensor_sht3x_update();
			//adc_update();
			//printf("NTC Temp.:%d�C \n",temp_sens[NTC_TEMP_SENS].temperature);//printf("NTC Temp.:%d�C NTC_ADC:%4ld mV \n",temp_sens[NTC_TEMP_SENS].temperature,adc123_ch[NTC_ADC1_CH13].millivolt);
			//printf("Status:%d Command:%d \n",revo_state,revo_op_cmd.cmd_reg16);
			//printf("VREF:%4ld mV \n",adc123_ch[VREF_ADC1_CH17].millivolt);
			//printf("ENC_ADC:%4ld mV \n",adc123_ch[ENC_ADC1_CH10].millivolt);
			//printf("FAN2:%d \n",fan[FAN_2].pwm_duty[0]);
			//error_print();
		    //printf("KeyCode:0x%X  NumKeys Stroke:%d \n\r",NBN_KB_CSM.key_code,NBN_KB_CSM.keys_stroke);
		    //printf("Key Bitmap:0x%lX \n\r",NBN_KB_CSM.keys_status.all32);
		    //printf("Rise(1)/Fall(0):%d \n\r",NBN_KB_CSM.fading_inc_dec);
		    //printf("Duty:%.2f \n\r",NBN_KB_CSM.fading_duty);
			//led_service_ON_OFF(4,TOGGLE_PIN_LED);
			flag_1sec=0;
	  }
    //}
    /* USER CODE END WHILE */
    //MX_USB_HOST_Process();

    /* USER CODE BEGIN 3 */
	// printf("Loop:%d\n\r",runtimer_ms);
	// printf("RS485:%d\n\r",time_elapsed[0]);
	// printf("Fade:%d\n\r",time_elapsed[1]);
	// printf("Start Keys:%d\n\r",time_elapsed[2]);
	// printf("End Keys:%d\n\r",time_elapsed[3]);


  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure LSE Drive Capability 
  */
  HAL_PWR_EnableBkUpAccess();
  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
#ifdef HSE_SOURCE_CLK_16MHZ
  RCC_OscInitStruct.PLL.PLLM = 8;
#endif
#ifdef HSE_SOURCE_CLK_12MHZ
  RCC_OscInitStruct.PLL.PLLM = 6;
#endif
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_USART3
                              |RCC_PERIPHCLK_USART6|RCC_PERIPHCLK_I2C1
                              |RCC_PERIPHCLK_I2C2|RCC_PERIPHCLK_I2C3
                              |RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Usart6ClockSelection = RCC_USART6CLKSOURCE_PCLK2;
  PeriphClkInitStruct.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInitStruct.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
  PeriphClkInitStruct.I2c3ClockSelection = RCC_I2C3CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48SOURCE_PLL;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* DMA1_Stream5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);
  /* DMA2_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream1_IRQn);
  /* RCC_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(RCC_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(RCC_IRQn);
  /* ADC_IRQn interrupt configuration */
 // HAL_NVIC_SetPriority(ADC_IRQn, 0, 0);
 // HAL_NVIC_EnableIRQ(ADC_IRQn);
  /* TIM1_UP_TIM10_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(TIM1_UP_TIM10_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(TIM1_UP_TIM10_IRQn);
  /* TIM1_TRG_COM_TIM11_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(TIM1_TRG_COM_TIM11_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(TIM1_TRG_COM_TIM11_IRQn);
  /* TIM1_CC_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(TIM1_CC_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(TIM1_CC_IRQn);
  /* TIM3_IRQn interrupt configuration */
 // HAL_NVIC_SetPriority(TIM3_IRQn, 0, 0);
 // HAL_NVIC_EnableIRQ(TIM3_IRQn);
  /* USART6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART6_IRQn);
  /* USART2_IRQn interrupt configuration */
 // HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
 // HAL_NVIC_EnableIRQ(USART2_IRQn);
  /* USART3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART3_IRQn);
  /* SPI4_IRQn interrupt configuration */
 // HAL_NVIC_SetPriority(SPI4_IRQn, 0, 0);
 // HAL_NVIC_EnableIRQ(SPI4_IRQn);
  /* TIM8_UP_TIM13_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM8_UP_TIM13_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM8_UP_TIM13_IRQn);
  /* TIM8_TRG_COM_TIM14_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM8_TRG_COM_TIM14_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM8_TRG_COM_TIM14_IRQn);
  /* TIM6_DAC_IRQn interrupt configuration */
 // HAL_NVIC_SetPriority(TIM6_DAC_IRQn, 0, 0);
 // HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);
  /* TIM7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM7_IRQn);
}

/* USER CODE BEGIN 4 */
void CPU_CACHE_Disable(void){

	 /* Disable I-Cache */
	 //SCB_CleanInvalidateDCache();//SCB_DisableICache();

	  /* Disable D-Cache */
	  SCB_DisableDCache();
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
