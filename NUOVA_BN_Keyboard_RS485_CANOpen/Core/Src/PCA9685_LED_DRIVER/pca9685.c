/* Includes ------------------------------------------------------------------*/

#include "./PCA9685_LED_DRIVER/pca9685.h"
#include <math.h>
#include <assert.h>
#include <string.h>
#include "i2c.h"


/* Private constants  ---------------------------------------------------------*/
static const uint16_t BLINK_DEFAULT_PERIOD100ms[] = {
TOGGLE_PIN_LED_SLOW ,     //30 period 1 toggle blinking led in 100ms
TOGGLE_PIN_LED_MEDIUM ,   //10 period 1 toggle blinking led in 100ms
TOGGLE_PIN_LED_FAST   ,     // 5 period 1 toggle blinking led in 100ms
TOGGLE_PIN_LED_VERY_FAST,  // 2 period 1 toggle blinking led in 100ms
};



/* Private constants ---------------------------------------------------------*/



#define PCA9685_SET_BIT_MASK(BYTE, MASK)      ((BYTE) |= (uint8_t)(MASK))
#define PCA9685_CLEAR_BIT_MASK(BYTE, MASK)    ((BYTE) &= (uint8_t)(~(uint8_t)(MASK)))
#define PCA9685_READ_BIT_MASK(BYTE, MASK)     ((BYTE) & (uint8_t)(MASK))


/**
 * Logarithmic dimming table, mapping from 255 inputs to 12 bit PWM values.
 */
static const uint16_t CIEL_8_12[] = {
		0, 2, 4, 5, 7, 9, 11, 12, 14, 16, 18, 20, 21, 23, 25, 27, 28, 30, 32, 34, 36, 37, 39, 41, 43, 45, 47, 49, 52,
		54, 56, 59, 61, 64, 66, 69, 72, 75, 77, 80, 83, 87, 90, 93, 97, 100, 103, 107, 111, 115, 118, 122, 126, 131,
		135, 139, 144, 148, 153, 157, 162, 167, 172, 177, 182, 187, 193, 198, 204, 209, 215, 221, 227, 233, 239, 246,
		252, 259, 265, 272, 279, 286, 293, 300, 308, 315, 323, 330, 338, 346, 354, 362, 371, 379, 388, 396, 405, 414,
		423, 432, 442, 451, 461, 471, 480, 490, 501, 511, 521, 532, 543, 554, 565, 576, 587, 599, 610, 622, 634, 646,
		658, 670, 683, 696, 708, 721, 734, 748, 761, 775, 789, 802, 817, 831, 845, 860, 875, 890, 905, 920, 935, 951,
		967, 983, 999, 1015, 1032, 1048, 1065, 1082, 1099, 1117, 1134, 1152, 1170, 1188, 1206, 1225, 1243, 1262, 1281,
		1301, 1320, 1340, 1359, 1379, 1400, 1420, 1441, 1461, 1482, 1504, 1525, 1547, 1568, 1590, 1613, 1635, 1658,
		1681, 1704, 1727, 1750, 1774, 1798, 1822, 1846, 1871, 1896, 1921, 1946, 1971, 1997, 2023, 2049, 2075, 2101,
		2128, 2155, 2182, 2210, 2237, 2265, 2293, 2322, 2350, 2379, 2408, 2437, 2467, 2497, 2527, 2557, 2587, 2618,
		2649, 2680, 2712, 2743, 2775, 2807, 2840, 2872, 2905, 2938, 2972, 3006, 3039, 3074, 3108, 3143, 3178, 3213,
		3248, 3284, 3320, 3356, 3393, 3430, 3467, 3504, 3542, 3579, 3617, 3656, 3694, 3733, 3773, 3812, 3852, 3892,
		3932, 3973, 4013, 4055, 4095
};

PCA9685_STATUS PCA9685_SetBit(pca9685_handle_t *handle,uint8_t Register, uint8_t Bit, uint8_t Value);
PCA9685_STATUS PCA9685_SleepMode(uint8_t Enable);
PCA9685_STATUS PCA9685_RestartMode(uint8_t Enable);
PCA9685_STATUS PCA9685_AutoIncrement(uint8_t Enable);
PCA9685_STATUS PCA9685_SubaddressRespond(SubaddressBit Subaddress, uint8_t Enable);
PCA9685_STATUS PCA9685_AllCallRespond(uint8_t Enable);

void set_status_led(uint8_t index,uint8_t status,uint8_t n_blink,uint8_t blink_lev);
void set_status_BKL_led(uint8_t index,uint8_t status,uint8_t n_blink,uint8_t blink_lev);

static bool pca9685_write_u8(pca9685_handle_t *handle, uint8_t address, uint8_t value)
{
	uint8_t data[] = {address, value};
	return HAL_I2C_Master_Transmit(handle->i2c_handle, handle->device_address, data, 2, PCA9685_I2C_TIMEOUT) == HAL_OK;
}

static bool pca9685_write_data(pca9685_handle_t *handle, uint8_t address, uint8_t *data, size_t length)
{
    if (length == 0 || length > 4) {
        return false;
    }

    uint8_t transfer[5];
    transfer[0] = address;

    memcpy(&transfer[1], data, length);

    return HAL_I2C_Master_Transmit(handle->i2c_handle, handle->device_address, transfer, length + 1, PCA9685_I2C_TIMEOUT) == HAL_OK;
}

static bool pca9685_read_u8(pca9685_handle_t *handle, uint8_t address, uint8_t *dest)
{
	if (HAL_I2C_Master_Transmit(handle->i2c_handle, handle->device_address, &address, 1, PCA9685_I2C_TIMEOUT) != HAL_OK) {
		return false;
	}

	return HAL_I2C_Master_Receive(handle->i2c_handle, handle->device_address, dest, 1, PCA9685_I2C_TIMEOUT) == HAL_OK;
}

PCA9685_STATUS PCA9685_SetBit(pca9685_handle_t *handle,uint8_t Register, uint8_t Bit, uint8_t Value)
{
	uint8_t tmp;
	if(Value) Value = 1;

	if(HAL_OK != pca9685_read_u8(handle,Register, &tmp))
	{
		return PCA9685_ERROR;
	}
	tmp &= ~((1<<PCA9685_MODE1_RESTART_BIT)|(1<<Bit));
	tmp |= (Value&1)<<Bit;

	if(HAL_OK != pca9685_write_u8(handle,Register,tmp))
	{
		return PCA9685_ERROR;
	}

	return PCA9685_OK;
}

PCA9685_STATUS PCA9685_SleepMode(uint8_t Enable)
{
	return PCA9685_SetBit(pca9685_led,PCA9685_MODE1, PCA9685_MODE1_SLEEP_BIT, Enable);
}

PCA9685_STATUS PCA9685_RestartMode(uint8_t Enable)
{
	return PCA9685_SetBit(pca9685_led,PCA9685_MODE1, PCA9685_MODE1_RESTART_BIT, Enable);
}

PCA9685_STATUS PCA9685_AutoIncrement(uint8_t Enable)
{
	return PCA9685_SetBit(pca9685_led,PCA9685_MODE1, PCA9685_MODE1_AI_BIT, Enable);
}

PCA9685_STATUS PCA9685_SubaddressRespond(SubaddressBit Subaddress, uint8_t Enable)
{
	return PCA9685_SetBit(pca9685_led,PCA9685_MODE1, Subaddress, Enable);
}

PCA9685_STATUS PCA9685_AllCallRespond(uint8_t Enable)
{
	return PCA9685_SetBit(pca9685_led,PCA9685_MODE1, PCA9685_MODE1_ALCALL_BIT, Enable);
}

bool pca9685_init(pca9685_handle_t *handle)
{
	// Create the handle for the driver.

	uint8_t mode1_reg_default_value ;
	uint8_t mode2_reg_default_value ;
	uint8_t id=0;
	pca9685_handle_t handle1 ;
//	pca9685_handle_t handle1 = {
//	    .i2c_handle = &hi2c3,
//	    .device_address = PCA9865_I2C_DEFAULT_DEVICE_ADDRESS,
//	    .inverted = true
//	};
//	handle1->i2c_handle = &hi2c3;
//	handle1->device_address = PCA9865_I2C_DEFAULT_DEVICE_ADDRESS;
//	handle1->inverted = true;

	pca9685_led[0].i2c_handle = &hi2c3;
	pca9685_led[0].device_address = PCA9865_I2C_DEFAULT_DEVICE_ADDRESS;
	pca9685_led[0].inverted = true;

	pca9685_led[1].i2c_handle = &hi2c3;
	pca9685_led[1].device_address = PCA9865_I2C_DEFAULT_DEVICE_ADDRESS1;
	pca9685_led[1].inverted = true;

	pca9685_led[2].i2c_handle = &hi2c3;
	pca9685_led[2].device_address = PCA9865_I2C_DEFAULT_DEVICE_ADDRESS2;
	pca9685_led[2].inverted = true;

	pca9685_led[3].i2c_handle = &hi2c3;
	pca9685_led[3].device_address = PCA9865_I2C_DEFAULT_DEVICE_ADDRESS3;
	pca9685_led[3].inverted = true;

	assert(pca9685_led[0].i2c_handle != NULL);
	assert(pca9685_led[1].i2c_handle != NULL);
	assert(pca9685_led[2].i2c_handle != NULL);
	assert(pca9685_led[3].i2c_handle != NULL);

	HAL_I2C_MspInit(&hi2c3);

	bool success = true;
    for(id=0;id<4;id++){
    	handle1.i2c_handle = &hi2c3;
    	handle1.device_address = pca9685_led[id].device_address;
    	handle1.inverted = true;
		// Set mode registers to default values (Auto-Increment, Sleep, Open-Drain).
		mode1_reg_default_value = 0b00110000u;
		mode2_reg_default_value = 0b00000000u;

		if (handle1.inverted) {
			mode2_reg_default_value |= 0b00010000u;
		}

		success &= pca9685_write_u8(&handle1, PCA9685_REGISTER_MODE1, mode1_reg_default_value);
		success &= pca9685_write_u8(&handle1, PCA9685_REGISTER_MODE2, mode2_reg_default_value);

		// Turn all channels off to begin with.
		uint8_t data[4] = { 0x00, 0x00, 0x00, 0x10 };
		success &= pca9685_write_data(&handle1, PCA9685_REGISTER_ALL_LED_ON_L, data, 4);

		success &= pca9685_set_pwm_frequency(&handle1, 1000);
		success &= pca9685_wakeup(&handle1);
    }
	return success;
}

bool pca9685_is_sleeping(pca9685_handle_t *handle, bool *sleeping)
{
	bool success = true;

	// Read the current state of the mode 1 register.
	uint8_t mode1_reg;
	success &= pca9685_read_u8(handle, PCA9685_REGISTER_MODE1, &mode1_reg);

	// Check if the sleeping bit is set.
	*sleeping = PCA9685_READ_BIT_MASK(mode1_reg, PCA9685_REGISTER_MODE1_SLEEP);

	return success;
}

bool pca9685_sleep(pca9685_handle_t *handle)
{
	bool success = true;

	// Read the current state of the mode 1 register.
	uint8_t mode1_reg;
	success &= pca9685_read_u8(handle, PCA9685_REGISTER_MODE1, &mode1_reg);

	// Don't write the restart bit back and set the sleep bit.
	PCA9685_CLEAR_BIT_MASK(mode1_reg, PCA9685_REGISTER_MODE1_RESTART);
	PCA9685_SET_BIT_MASK(mode1_reg, PCA9685_REGISTER_MODE1_SLEEP);
	success &= pca9685_write_u8(handle, PCA9685_REGISTER_MODE1, mode1_reg);

	return success;
}

bool pca9685_wakeup(pca9685_handle_t *handle)
{
	bool success = true;

	// Read the current state of the mode 1 register.
	uint8_t mode1_reg;
	success &= pca9685_read_u8(handle, PCA9685_REGISTER_MODE1, &mode1_reg);

	bool restart_required = PCA9685_READ_BIT_MASK(mode1_reg, PCA9685_REGISTER_MODE1_RESTART);

	// Clear the restart bit for now and clear the sleep bit.
	PCA9685_CLEAR_BIT_MASK(mode1_reg, PCA9685_REGISTER_MODE1_RESTART);
	PCA9685_CLEAR_BIT_MASK(mode1_reg, PCA9685_REGISTER_MODE1_SLEEP);
	success &= pca9685_write_u8(handle, PCA9685_REGISTER_MODE1, mode1_reg);

	if (restart_required) {

		// Oscillator requires at least 500us to stabilise, so wait 1ms.
		HAL_Delay(1);

		PCA9685_SET_BIT_MASK(mode1_reg, PCA9685_REGISTER_MODE1_RESTART);
		success &= pca9685_write_u8(handle, PCA9685_REGISTER_MODE1, mode1_reg);
	}

	return success;
}

bool pca9685_set_pwm_frequency(pca9685_handle_t *handle, float frequency)
{
	assert(frequency >= 24);
	assert(frequency <= 1526);

	bool success = true;

	// Calculate the prescaler value (see datasheet page 25)
	uint8_t prescaler = (uint8_t)roundf(25000000.0f / (4096 * frequency)) - 1;

	bool already_sleeping;
	success &= pca9685_is_sleeping(handle, &already_sleeping);

	// The prescaler can only be changed in sleep mode.
	if (!already_sleeping) {
		success &= pca9685_sleep(handle);
	}

	// Write the new prescaler value.
	success &= pca9685_write_u8(handle, PCA9685_REGISTER_PRESCALER, prescaler);

	// If the device wasn't sleeping, return from sleep mode.
	if (!already_sleeping) {
		success &= pca9685_wakeup(handle);
	}

	return success;
}

bool pca9685_set_channel_pwm_times(pca9685_handle_t *handle, unsigned channel, unsigned on_time, unsigned off_time)
{
	assert(channel >= 0);
	assert(channel < 16);

	assert(on_time >= 0);
	assert(on_time <= 4096);

	assert(off_time >= 0);
	assert(off_time <= 4096);

	uint8_t data[4] = { on_time, on_time >> 8u, off_time, off_time >> 8u };
	return pca9685_write_data(handle, PCA9685_REGISTER_LED0_ON_L + channel * 4, data, 4);
}

bool pca9685_set_channel_duty_cycle(pca9685_handle_t *handle, unsigned channel, float duty_cycle, bool logarithmic)
{
	assert(duty_cycle >= 0.0);
	assert(duty_cycle <= 1.0);

	if (duty_cycle == 0.0) {
		return pca9685_set_channel_pwm_times(handle, channel, 0, 4096); // Special value for always off
	} else if (duty_cycle == 1.0) {
		return pca9685_set_channel_pwm_times(handle, channel, 4096, 0); // Special value for always on
	} else {

		unsigned required_on_time;

		if (logarithmic) {
			required_on_time = CIEL_8_12[(unsigned)roundf(255 * duty_cycle)];
		} else {
			required_on_time = (unsigned)roundf(4095 * duty_cycle);
		}

		// Offset on and off times depending on channel to minimise current spikes.
		unsigned on_time = (channel == 0) ? 0 : (channel * 256) - 1;
		unsigned off_time = (on_time + required_on_time) & 0xfffu;

		return pca9685_set_channel_pwm_times(handle, channel, on_time, off_time);
	}
}


bool pca9685_set_group_duty_cycle(pca9685_handle_t *handle, unsigned start,unsigned end, float duty_cycle, bool logarithmic)
{
	assert(duty_cycle >= 0.0);
	assert(duty_cycle <= 1.0);
    unsigned channel;
    unsigned on_time;
    unsigned off_time;
    bool ret=1;//true;
    unsigned required_on_time;
    for(channel=start;channel<(end+1);channel++){
		if (duty_cycle == 0.0) {
			ret=1;//ret && pca9685_set_channel_pwm_times(handle, channel, 0, 4096); // Special value for always off
		} else if (duty_cycle == 1.0) {
			ret=1;//ret && pca9685_set_channel_pwm_times(handle, channel, 4096, 0); // Special value for always on
		} else {

			if (logarithmic) {
				required_on_time = CIEL_8_12[(unsigned)roundf(255 * duty_cycle)];
			} else {
				required_on_time = (unsigned)roundf(4095 * duty_cycle);
			}

			// Offset on and off times depending on channel to minimise current spikes.
			on_time = (channel == 0) ? 0 : (channel * 256) - 1;
			off_time = (on_time + required_on_time) & 0xfffu;

			ret=ret && pca9685_set_channel_pwm_times(handle, channel, on_time, off_time);
		}
   }
   return(ret);
}



void set_led_NBN(uint8_t led,LED_status_t status,uint16_t time_100ms,uint8_t n_blink,uint8_t blink_lev,uint8_t type ){
  uint8_t id ;
  float duty;
  pca9685_handle_t *handle;

  switch(led){
	   case(0):
		        id=0;
	            handle=&pca9685_led[0];
			    break;
	   case(1):
	  		    id=1;
	            handle=&pca9685_led[0];
	  			break;
	   case(2):
				id=2;
				handle=&pca9685_led[0];
				break;
	   case(3):
				id=3;
				handle=&pca9685_led[0];
				break;
	   case(4):
				id=4;
				handle=&pca9685_led[0];
				break;
	   case(5):
				id=5;
				handle=&pca9685_led[0];
				break;
	   case(6):
				id=6;
				handle=&pca9685_led[0];
				break;
	   case(7):
				id=7;
				handle=&pca9685_led[0];
				break;
	   case(8):
				id=8;
				handle=&pca9685_led[0];
				break;
	   case(9):
				id=9;
				handle=&pca9685_led[0];
				break;
	   case(10):
				id=10;
				handle=&pca9685_led[0];
				break;
	   case(11):
				id=11;
				handle=&pca9685_led[0];
				break;
	   case(12):
				id=12;
				handle=&pca9685_led[0];
				break;
	   case(13):
				id=13;
				handle=&pca9685_led[0];
				break;
	   case(14):
				id=14;
				handle=&pca9685_led[0];
				break;
	   case(15):
				id=15;
				handle=&pca9685_led[0];
				break;
	   case(16):
				id=16;
				handle=&pca9685_led[1];
				break;
	   case(17):
				id=17;
				handle=&pca9685_led[1];
				break;
	   case(18):
				id=18;
				handle=&pca9685_led[1];
				break;
	   case(19):
				id=19;
				handle=&pca9685_led[1];
				break;
		case(20):
				id=20;
				handle=&pca9685_led[1];
				break;
		case(21):
				id=21;
				handle=&pca9685_led[1];
				break;
		case(22):
				id=22;
				handle=&pca9685_led[1];
				break;
		case(23):
				id=23;
				handle=&pca9685_led[1];
				break;
		case(24):
				id=24;
				handle=&pca9685_led[1];
				break;
		case(25):
			    id=25;
		     	handle=&pca9685_led[1];
			    break;
		case(26):
			    id=26;
			    handle=&pca9685_led[1];
			    break;
		case(27):
				id=27;
				handle=&pca9685_led[1];
				break;
		case(28):
				id=28;
				handle=&pca9685_led[1];
				break;
		case(29):
				id=29;
				handle=&pca9685_led[1];
				break;
		case(30):
				id=30;
				handle=&pca9685_led[1];
				break;
		case(31):
				id=31;
				handle=&pca9685_led[1];
				break;
	   default:
		        break;
	}


	//only led Button
	if(status==SET_LED_ON){
		  NBN_KB_CSM.led_all[id].status= SET_LED_ON;
		  duty=1.00f;
	 }else if(status==SET_LED_OFF){
			  NBN_KB_CSM.led_all[id].status= SET_LED_OFF;
			  duty=0.00f;
		  }else{//blink
			  NBN_KB_CSM.led_all[id].status= SET_LED_BLINK;
			  NBN_KB_CSM.led_all[id].blink_lev=blink_lev;
			  NBN_KB_CSM.led_all[id].num_blink=n_blink;
			  NBN_KB_CSM.blink_timing[blink_lev]= BLINK_DEFAULT_PERIOD100ms[blink_lev]; //time_100ms;
			  NBN_KB_CSM.led_all[id].time_blink=time_100ms;
			  duty=(float)(NBN_KB_CSM.blink_toggle[blink_lev]);
			  if(NBN_KB_CSM.led_all[id].num_blink!=0){
				 if (NBN_KB_CSM.led_all[id].countdown_blink>0){
					 NBN_KB_CSM.led_all[id].countdown_blink--;
				 }else{
					 duty=0.00f;
					 NBN_KB_CSM.led_all[id].status= SET_LED_OFF;
				 }
			  }
		  }
	 pca9685_set_channel_duty_cycle(handle,id%16,duty,false);


}


void set_led_BLK_NBN(uint8_t led,LED_status_t status,uint16_t time_100ms,uint8_t n_blink,uint8_t blink_lev,uint8_t type ){
  uint8_t id ;
  float duty;
  pca9685_handle_t *handle;

  switch(led){
	   case(0):
		        id=0;
	            handle=&pca9685_led[2];
			    break;
	   case(1):
	  		    id=1;
	            handle=&pca9685_led[2];
	  			break;
	   case(2):
				id=2;
				handle=&pca9685_led[2];
				break;
	   case(3):
				id=3;
				handle=&pca9685_led[2];
				break;
	   case(4):
				id=4;
				handle=&pca9685_led[2];
				break;
	   case(5):
				id=5;
				handle=&pca9685_led[2];
				break;
	   case(6):
				id=6;
				handle=&pca9685_led[2];
				break;
	   case(7):
				id=7;
				handle=&pca9685_led[2];
				break;
	   case(8):
				id=8;
				handle=&pca9685_led[2];
				break;
	   case(9):
				id=9;
				handle=&pca9685_led[2];
				break;
	   case(10):
				id=10;
				handle=&pca9685_led[2];
				break;
	   case(11):
				id=11;
				handle=&pca9685_led[2];
				break;
	   case(12):
				id=12;
				handle=&pca9685_led[2];
				break;
	   case(13):
				id=13;
				handle=&pca9685_led[2];
				break;
	   case(14):
				id=14;
				handle=&pca9685_led[2];
				break;
	   case(15):
				id=15;
				handle=&pca9685_led[2];
				break;
	   case(16):
				id=16;
				handle=&pca9685_led[3];
				break;
	   case(17):
				id=17;
				handle=&pca9685_led[3];
				break;
	   case(18):
				id=18;
				handle=&pca9685_led[3];
				break;
	   case(19):
				id=19;
				handle=&pca9685_led[3];
				break;
		case(20):
				id=20;
				handle=&pca9685_led[3];
				break;
		case(21):
				id=21;
				handle=&pca9685_led[3];
				break;
		case(22):
				id=22;
				handle=&pca9685_led[3];
				break;
		case(23):
				id=23;
				handle=&pca9685_led[3];
				break;
		case(24):
				id=24;
				handle=&pca9685_led[3];
				break;
		case(25):
			    id=25;
		     	handle=&pca9685_led[3];
			    break;
		case(26):
			    id=26;
			    handle=&pca9685_led[3];
			    break;
		case(27):
				id=27;
				handle=&pca9685_led[3];
				break;
		case(28):
				id=28;
				handle=&pca9685_led[3];
				break;
		case(29):
				id=29;
				handle=&pca9685_led[3];
				break;
		case(30):
				id=30;
				handle=&pca9685_led[3];
				break;
		case(31):
				id=31;
				handle=&pca9685_led[3];
				break;
	   default:
		        break;
	}

	if(type==LED_BKL_TYPE){//only backlight

		 if(status==SET_LED_ON){
		  	  //NBN_KB_CSM.led_all[id].status= SET_LED_ON;
		  	  duty=1.00f;
		  	  pca9685_set_channel_duty_cycle(handle,id%16,duty,false);
		 }else if(status==SET_LED_OFF){
				  //NBN_KB_CSM.led_all[id].status= SET_LED_OFF;
				  if((id==LED_POWER_ID)||(id==LED_LOGO_ID)){
					    duty=(float)(NBN_KB_CSM.fading_duty);
					  	pca9685_set_channel_duty_cycle(handle,id%16,duty, true);
				  }else{
					duty=0.00f;
					pca9685_set_channel_duty_cycle(handle,id%16,duty,false);
				  }
		      }else{//blink
		    	  //NBN_KB_CSM.led_all[id].status= SET_LED_BLINK;
		    	  //NBN_KB_CSM.blink_timing[id]= time_100ms;
		    	  duty=(float)(NBN_KB_CSM.blink_toggle[blink_lev]);
		    	  if(NBN_KB_CSM.led_bkl[id].num_blink!=0){
					 if (NBN_KB_CSM.led_bkl[id].countdown_blink>0){
						 //duty=(float)(NBN_KB_CSM.blink_toggle[blink_lev]);
						 NBN_KB_CSM.led_bkl[id].countdown_blink--;
					 }else{
						 duty=0.00f;
					 }
				  }
		    	  pca9685_set_channel_duty_cycle(handle,id%16,duty,false);
		  	  }

	}

}



/*
 * Set level for blink
 * Turn OFF all Led TASTO
 * Turn OFF all Led BACKLIGHT
 * */
void pca9685_begin(void){
uint8_t id;
	for(id=0;id<BLINK_LED_TIMING;id++){
		    NBN_KB_CSM.blink_timing[id]=BLINK_DEFAULT_PERIOD100ms[id];
			NBN_KB_CSM.blink_toggle[id]=0;
			NBN_KB_CSM.blink_counter[id]=NBN_KB_CSM.blink_timing[id];
    }
	for(id=0;id<NUM_MAX_LEDS;id++){
	//	if(id%2==0){
			NBN_KB_CSM.led_all[id].status=SET_LED_OFF;
			NBN_KB_CSM.led_all[id].time_blink=5;
			NBN_KB_CSM.led_all[id].num_blink=0;//everlasting blinking
			NBN_KB_CSM.led_all[id].countdown_blink=(NBN_KB_CSM.led_all[id].num_blink*NBN_KB_CSM.led_all[id].time_blink);
			NBN_KB_CSM.led_all[id].blink_lev=2;

			NBN_KB_CSM.led_bkl[id].status=SET_LED_OFF;
			NBN_KB_CSM.led_bkl[id].time_blink=5;
			NBN_KB_CSM.led_bkl[id].num_blink=0;//everlasting blinking
			NBN_KB_CSM.led_bkl[id].countdown_blink=(NBN_KB_CSM.led_bkl[id].num_blink*NBN_KB_CSM.led_bkl[id].time_blink);
			NBN_KB_CSM.led_bkl[id].blink_lev=2;
//		}else{
//			NBN_KB_CSM.led_all[id].status=SET_LED_BLINK;
//			NBN_KB_CSM.led_all[id].time_blink=10;
//			NBN_KB_CSM.led_all[id].num_blink=0;//everlasting blinking
//			NBN_KB_CSM.led_all[id].countdown_blink=(NBN_KB_CSM.led_all[id].num_blink*NBN_KB_CSM.led_all[id].time_blink);
//			NBN_KB_CSM.led_all[id].blink_lev=1;
//		}
	}
	NBN_KB_CSM.fading_duty=0.00f;
	NBN_KB_CSM.fading_inc_dec=1;
	NBN_KB_CSM.fading_time=0;
}

/*
 * Set status led tasto based on KEY CODE
 * */
uint8_t pca9685_set_led_tasto(uint8_t led,LED_status_t status,uint8_t n_blink,uint8_t blink_lev){
   uint8_t ret=1;//0;
   uint8_t id;
   uint16_t time_100ms=5;
   if(blink_lev<4){
    time_100ms=  BLINK_DEFAULT_PERIOD100ms[blink_lev];

   }
switch(led){
    case(KEY_M1UP):
    		               id=0;//
                           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
   		                   set_led_NBN(0,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M1UP;
						   break;

	case(KEY_M2UP ):
		                   id=2;//
		                   NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
		                   set_led_NBN(2,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M2UP;
						   break;

	case(KEY_M3UP ):
		                   id=4;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
			               set_led_NBN(4,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M3UP;
						   break;

	case(KEY_M4UP ):
		                   id=6;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						   set_led_NBN(6,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M4UP;
						   break;

	case(KEY_M5UP_LR):
							id=8;//
							NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
							set_led_NBN(8,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
							ret=1;//1;//KEY_M5UP_LR;
							break;
	case(KEY_M6UP ):
							id=10;//
							NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
							set_led_NBN(10,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
							ret=1;//1;//KEY_M6UP;
							break;
	case(KEY_M7UP_LR ):
							id=12;//
							NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
							set_led_NBN(12,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
							ret=1;//1;//KEY_M7UP_LR;
							break;
	case(KEY_M7UP_LO ):
		                   id=12;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						   set_led_NBN(12,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   id=14;//
						   NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
	                       set_led_NBN(14,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M7UP_LO;
						   break;
	case(KEY_M7UP_RO ):
		                   id=12;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
	           			   set_led_NBN(12,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
	           			   id=15;//
	           			   NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
	                       set_led_NBN(15,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M7UP_RO;
						   break;
	case(KEY_M5UP_LO ):
		                   id=8;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
		                   set_led_NBN(8,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
		                   id=14;//
		                   NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						   set_led_NBN(14,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M5UP_LO;
						   break;
	case(KEY_M5UP_RO ):
		                   id=8;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
		                   set_led_NBN(8,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
		                   id=15;//
		                   NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						   set_led_NBN(15,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M5UP_RO;
						   break;
	case(KEY_M1DN ):
		                   id=1;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						   set_led_NBN(1,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M1DN;
						   break;
	case(KEY_M2DN ):
		                   id=3;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					   	   set_led_NBN(3,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M2DN;
						   break;
	case(KEY_M3DN ):
		                   id=5;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					       set_led_NBN(5,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M3DN;
						   break;
	case(KEY_M4DN ):
		                   id=7;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						   set_led_NBN(7,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M4DN;
						   break;
	case(KEY_M5DN_LR ):
		                   id=9;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						   set_led_NBN(9,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M5DN_LR;
						   break;
	case(KEY_M6DN ):
		                   id=11;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						   set_led_NBN(11,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M6DN;
						   break;
	case(KEY_M7DN_LR ):
		                   id=13;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						   set_led_NBN(13,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M7DN_LR;
						   break;
	case(KEY_M7DN_LO ):
						id=13;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(13,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						id=14;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(14,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						ret=1;//1;//KEY_M7DN_LO;
						break;
	case(KEY_M7DN_RO ):
						id=13;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(13,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						id=15;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(15,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						ret=1;//1;//KEY_M7DN_RO;
						break;
	case(KEY_M5DN_LO ):
		                   id=9;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
		                   set_led_NBN(9,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
		                   id=14;//
		                   NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
	                       set_led_NBN(14,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_M5DN_LO;
						   break;
	case(KEY_M5DN_RO ):
						id=9;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(9,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						id=15;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(15,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						ret=1;//1;//KEY_M5DN_RO;
						break;
	case(KEY_ROMBO ):
						id=22;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(22,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						ret=1;//1;//KEY_ROMBO;
						break;
	case(KEY_RECALL ):
						id=24;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(24,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						ret=1;//1;//KEY_RECALL;
						break;
	case(KEY_ZERO ):
						id=19;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(19,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						ret=1;//1;//KEY_ZERO;
						break;
	case(KEY_RMEMA ):
						id=24;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(24,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						id=23;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(23,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						ret=1;//1;//KEY_RMEMA;
						break;
	case(KEY_RMEMB ):
						id=24;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(24,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						id=25;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(25,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						ret=1;//1;//KEY_RMEMB;
						break;
	case(KEY_RMEMC ):
						id=24;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(24,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						id=27;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(27,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						ret=1;//1;//KEY_RMEMC;
						break;
	case(KEY_SMEMA ):
		                   id=22;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
		                   set_led_NBN(22,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
		                   id=23;//
		                   NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
	                       set_led_NBN(23,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_SMEMA;
						   break;
	case(KEY_SMEMB ):
		                   id=22;//
				           NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
		                   set_led_NBN(22,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
		                   id=25;//
		                   NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
	                       set_led_NBN(25,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						   ret=1;//1;//KEY_SMEMB;
						   break;
	case(KEY_SMEMC ):
						id=22;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(22,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						id=27;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(27,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						ret=1;//1;//KEY_SMEMC;
						   break;
	case(KEY_REFLEX ):
						id=16;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(16,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						ret=1;//1;//KEY_REFLEX;
						break;
	case(KEY_CHAIR ):
						id=17;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(17,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						ret=1;//1;//KEY_CHAIR;
					    break;
	case(KEY_FLEX ):
						id=18;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(18,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						ret=1;//1;//KEY_FLEX;
					    break;
	case(KEY_M1_ZR ):
						id=24;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(24,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						id=0;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(0,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);

						ret=1;//1;//KEY_M1_ZR;
						break;
	case(KEY_M2_ZR ):
						id=24;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(24,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
						id=2;//
						NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
						set_led_NBN(2,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);

						ret=1;//1;//KEY_M2_ZR;
						break;
	case(KEY_M3_ZR ):
					id=24;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(24,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					id=4;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(4,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					//set_led_NBN(26,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_M3_ZR;
					break;
	case(KEY_M4_ZR ):
					id=24;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(24,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					id=6;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(6,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					//set_led_NBN(26,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_M4_ZR;
					break;
	case(KEY_M5_ZR ):
					id=24;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(24,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					id=8;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(8,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					//set_led_NBN(26,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_M5_ZR;
					break;
	case(KEY_M6_ZR ):
					id=24;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(24,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					id=10;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(10,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					//set_led_NBN(26,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_M6_ZR;
					break;
	case(KEY_M7_ZR ):
					id=24;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(24,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					id=12;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(12,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					//set_led_NBN(26,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_M7_ZR;
					break;
//	case(KEY_JOY_UP_SX ):
//	                  set_led_NBN(0,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
//					  ret=1;//1;//KEY_JOY_UP_SX;
//	  			      break;
//	case(KEY_JOY_UP_DX ):
// 		              set_led_NBN(0,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
//				      ret=1;//1;//KEY_JOY_UP_DX;
//					  break;
//	case(KEY_JOY_DN_DX ):
//		              set_led_NBN(0,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
//					  ret=1;//1;//KEY_JOY_DN_DX;
//					  break;
//	case(KEY_JOY_DN_SX ):
//		              set_led_NBN(0,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
//					  ret=1;//1;//KEY_JOY_DN_SX;
//	  			      break;
	case(KEY_TASTO1):
					id=28;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(28,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_JOY_DN_SX;
					break;
	case(KEY_TASTO2):
					id=29;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(29,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_JOY_DN_SX;
					break;
	case(KEY_TASTO3):
					id=30;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(30,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_JOY_DN_SX;
					break;
	case(KEY_TASTO4):
					id=31;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(31,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_JOY_DN_SX;
					break;
	case(KEY_RMEMD ):
					id=24;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(24,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					id=23;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(23,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					//set_led_NBN(26,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_RMEMD;
					break;
	case(KEY_RMEME ):
					id=24;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(24,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					id=25;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(25,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					//set_led_NBN(26,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_RMEME;
					break;
	case(KEY_RMEMF ):
					id=24;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(24,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					id=27;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(27,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					//set_led_NBN(26,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_RMEMF;
					break;
	case(KEY_SMEMD ):
					id=22;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(22,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					id=23;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(23,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					//set_led_NBN(26,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_SMEMD;
					break;
	case(KEY_SMEME ):
					id=22;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(22,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					id=25;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(25,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					//set_led_NBN(26,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_SMEME;
					break;
	case(KEY_SMEMF ):
					id=22;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(22,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					id=27;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(27,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					//set_led_NBN(26,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_SMEMF;
					break;
	case(KEY_SHIFT ):
					id=2;//
					NBN_KB_CSM.led_all[id].countdown_blink=(n_blink*time_100ms);
					set_led_NBN(26,status,time_100ms,n_blink,blink_lev,LED_TASTO_TYPE);
					ret=1;//1;//KEY_SHIFT;
					break;


	default:
		     break;
   }
  return(ret);
}

void set_status_led(uint8_t index,uint8_t status,uint8_t n_blink,uint8_t blink_lev){

	 if(status!=SET_LED_BLINK){
				 NBN_KB_CSM.led_all[index].status=status;//((NBN_KB_CSM.led_all[index].status==SET_LED_ON)?SET_LED_OFF:SET_LED_ON);
	 }else{
				   NBN_KB_CSM.led_all[index].status=SET_LED_BLINK;
				   NBN_KB_CSM.led_all[index].time_blink=blink_lev;
				   NBN_KB_CSM.led_all[index].num_blink =n_blink;//everlasting blinking
				   NBN_KB_CSM.led_all[index].countdown_blink=(NBN_KB_CSM.led_all[index].num_blink*NBN_KB_CSM.led_all[index].time_blink);
				   NBN_KB_CSM.led_all[index].blink_lev =BLINK_LED_FAST;
		  }
}

void set_status_BKL_led(uint8_t index,uint8_t status,uint8_t n_blink,uint8_t blink_lev){

	 if(status!=SET_LED_BLINK){
				 NBN_KB_CSM.led_bkl[index].status=status;//((NBN_KB_CSM.led_all[index].status==SET_LED_ON)?SET_LED_OFF:SET_LED_ON);
	 }else{
				   NBN_KB_CSM.led_bkl[index].status=SET_LED_BLINK;
				   NBN_KB_CSM.led_bkl[index].time_blink=blink_lev;
				   NBN_KB_CSM.led_bkl[index].num_blink =n_blink;//everlasting blinking
				   NBN_KB_CSM.led_bkl[index].countdown_blink=(NBN_KB_CSM.led_all[index].num_blink*NBN_KB_CSM.led_all[index].time_blink);
				   NBN_KB_CSM.led_bkl[index].blink_lev =BLINK_LED_FAST;
		  }
}

uint8_t set_led_tasto_retro_NBN(byte_bit_t key_code,uint8_t status,uint8_t n_blink,uint8_t blink_lev){

uint8_t ret;

    ret=0;
    if(status==SET_LED_BLINK){
    	NBN_KB_CSM.led_blink=key_code;
		if (key_code.bit.b0==1){
			   set_status_led(0,status,n_blink,blink_lev);
			   set_status_led(1,status,n_blink,blink_lev);
		}else{
			if( NBN_KB_CSM.led_on_off.bit.b0==0){
				set_status_led(0,SET_LED_OFF,n_blink,blink_lev);
				set_status_led(1,SET_LED_OFF,n_blink,blink_lev);
			}else{
				  set_status_led(0,SET_LED_ON,n_blink,blink_lev);
			 	  set_status_led(1,SET_LED_ON,n_blink,blink_lev);
			     }
		}

		if (key_code.bit.b1==1){
			set_status_led(2,status,n_blink,blink_lev);
			set_status_led(3,status,n_blink,blink_lev);
		}else{
			if( NBN_KB_CSM.led_on_off.bit.b1==0){
				set_status_led(2,SET_LED_OFF,n_blink,blink_lev);
				set_status_led(3,SET_LED_OFF,n_blink,blink_lev);
			}else{
				  set_status_led(2,SET_LED_ON,n_blink,blink_lev);
				  set_status_led(3,SET_LED_ON,n_blink,blink_lev);
				 }
		}

		if (key_code.bit.b2==1){
			set_status_led(4,status,n_blink,blink_lev);
			set_status_led(5,status,n_blink,blink_lev);
		}else{
			if( NBN_KB_CSM.led_on_off.bit.b2==0){
				set_status_led(4,SET_LED_OFF,n_blink,blink_lev);
				set_status_led(5,SET_LED_OFF,n_blink,blink_lev);
			}else{
				  set_status_led(4,SET_LED_ON,n_blink,blink_lev);
				  set_status_led(5,SET_LED_ON,n_blink,blink_lev);
				 }

		}

		if (key_code.bit.b3==1){
			set_status_led(6,status,n_blink,blink_lev);
			set_status_led(7,status,n_blink,blink_lev);
		}else{
			if( NBN_KB_CSM.led_on_off.bit.b3==0){
				set_status_led(6,SET_LED_OFF,n_blink,blink_lev);
				set_status_led(7,SET_LED_OFF,n_blink,blink_lev);
			}else{
				  set_status_led(6,SET_LED_ON,n_blink,blink_lev);
				  set_status_led(7,SET_LED_ON,n_blink,blink_lev);
				 }

		}

		if (key_code.bit.b4==1){
			set_status_led(8,status,n_blink,blink_lev);
			set_status_led(9,status,n_blink,blink_lev);
		}else{
			if( NBN_KB_CSM.led_on_off.bit.b4==0){
				set_status_led(8,SET_LED_OFF,n_blink,blink_lev);
				set_status_led(9,SET_LED_OFF,n_blink,blink_lev);
			}else{
				  set_status_led(8,SET_LED_ON,n_blink,blink_lev);
				  set_status_led(9,SET_LED_ON,n_blink,blink_lev);
				 }
		}

		if (key_code.bit.b5==1){
			set_status_led(10,status,n_blink,blink_lev);
			set_status_led(11,status,n_blink,blink_lev);
		}else{
			if( NBN_KB_CSM.led_on_off.bit.b5==0){
				set_status_led(10,SET_LED_OFF,n_blink,blink_lev);
				set_status_led(11,SET_LED_OFF,n_blink,blink_lev);
			}else{
				  set_status_led(10,SET_LED_ON,n_blink,blink_lev);
				  set_status_led(11,SET_LED_ON,n_blink,blink_lev);
				 }
		}

		if (key_code.bit.b6==1){
			set_status_led(12,status,n_blink,blink_lev);
			set_status_led(13,status,n_blink,blink_lev);
			set_status_led(14,status,n_blink,blink_lev);

		}else{
			if( NBN_KB_CSM.led_on_off.bit.b6==0){
				set_status_led(12,SET_LED_OFF,n_blink,blink_lev);
				set_status_led(13,SET_LED_OFF,n_blink,blink_lev);
				set_status_led(14,SET_LED_OFF,n_blink,blink_lev);
			}else{
				  set_status_led(12,SET_LED_ON,n_blink,blink_lev);
				  set_status_led(13,SET_LED_ON,n_blink,blink_lev);
				  set_status_led(14,SET_LED_ON,n_blink,blink_lev);
				 }
		}

		if (key_code.bit.b7==1){
			set_status_led(12,status,n_blink,blink_lev);
			set_status_led(13,status,n_blink,blink_lev);
			set_status_led(15,status,n_blink,blink_lev);
		}else{
			if( NBN_KB_CSM.led_on_off.bit.b7==0){
				set_status_led(15,SET_LED_OFF,n_blink,blink_lev);
				if ((NBN_KB_CSM.led_on_off.bit.b6==0)&&(key_code.bit.b6==0)){
					set_status_led(12,SET_LED_OFF,n_blink,blink_lev);
					set_status_led(13,SET_LED_OFF,n_blink,blink_lev);
				}
			}else{
				  set_status_led(15,SET_LED_ON,n_blink,blink_lev);
				  if (key_code.bit.b6==0){
						set_status_led(12,SET_LED_ON,n_blink,blink_lev);
						set_status_led(13,SET_LED_ON,n_blink,blink_lev);
				  }
				 }

		}

    }else{
    	   NBN_KB_CSM.led_on_off=key_code;
    	   if( NBN_KB_CSM.led_blink.bit.b0==0){
    		   if (key_code.bit.b0==0){
					set_status_led(0,SET_LED_OFF,n_blink,blink_lev);
					set_status_led(1,SET_LED_OFF,n_blink,blink_lev);
				}else{
					  set_status_led(0,SET_LED_ON,n_blink,blink_lev);
					  set_status_led(1,SET_LED_ON,n_blink,blink_lev);
					 }
			}

    	   if( NBN_KB_CSM.led_blink.bit.b1==0){
    		   if (key_code.bit.b1==0){
					set_status_led(2,SET_LED_OFF,n_blink,blink_lev);
					set_status_led(3,SET_LED_OFF,n_blink,blink_lev);
				}else{
					  set_status_led(2,SET_LED_ON,n_blink,blink_lev);
					  set_status_led(3,SET_LED_ON,n_blink,blink_lev);
					 }
			}

    	   if( NBN_KB_CSM.led_blink.bit.b2==0){
    		   if (key_code.bit.b2==0){
					set_status_led(4,SET_LED_OFF,n_blink,blink_lev);
					set_status_led(5,SET_LED_OFF,n_blink,blink_lev);
				}else{
					  set_status_led(4,SET_LED_ON,n_blink,blink_lev);
					  set_status_led(5,SET_LED_ON,n_blink,blink_lev);
					 }

			}

    	   if( NBN_KB_CSM.led_blink.bit.b3==0){
    		   if (key_code.bit.b3==0){
					set_status_led(6,SET_LED_OFF,n_blink,blink_lev);
					set_status_led(7,SET_LED_OFF,n_blink,blink_lev);
				}else{
					  set_status_led(6,SET_LED_ON,n_blink,blink_lev);
					  set_status_led(7,SET_LED_ON,n_blink,blink_lev);
					 }

			}

    	   if( NBN_KB_CSM.led_blink.bit.b4==0){
    		   if (key_code.bit.b4==0){
					set_status_led(8,SET_LED_OFF,n_blink,blink_lev);
					set_status_led(9,SET_LED_OFF,n_blink,blink_lev);
				}else{
					  set_status_led(8,SET_LED_ON,n_blink,blink_lev);
					  set_status_led(9,SET_LED_ON,n_blink,blink_lev);
					 }
			}

    	   if( NBN_KB_CSM.led_blink.bit.b5==0){
    		   if (key_code.bit.b5==0){
					set_status_led(10,SET_LED_OFF,n_blink,blink_lev);
					set_status_led(11,SET_LED_OFF,n_blink,blink_lev);
				}else{
					  set_status_led(10,SET_LED_ON,n_blink,blink_lev);
					  set_status_led(11,SET_LED_ON,n_blink,blink_lev);
					 }
			}

		   if( NBN_KB_CSM.led_blink.bit.b6==0){
				 if (key_code.bit.b6==0){
					set_status_led(14,SET_LED_OFF,n_blink,blink_lev);
					if( NBN_KB_CSM.led_blink.bit.b7==0){
						set_status_led(12,SET_LED_OFF,n_blink,blink_lev);
						set_status_led(13,SET_LED_OFF,n_blink,blink_lev);
					}
				}else{
					  set_status_led(14,SET_LED_ON,n_blink,blink_lev);
					  if( NBN_KB_CSM.led_blink.bit.b7==0){
						  set_status_led(12,SET_LED_ON,n_blink,blink_lev);
						  set_status_led(13,SET_LED_ON,n_blink,blink_lev);
					  }
					 }
			}

		    if( NBN_KB_CSM.led_blink.bit.b7==0){
				 if (key_code.bit.b7==0){
					set_status_led(15,SET_LED_OFF,n_blink,blink_lev);
					if((NBN_KB_CSM.led_on_off.bit.b6==0)&&( NBN_KB_CSM.led_blink.bit.b6==0)){
						set_status_led(12,SET_LED_OFF,n_blink,blink_lev);
						set_status_led(13,SET_LED_OFF,n_blink,blink_lev);
					}
				}else{
					  set_status_led(15,SET_LED_ON,n_blink,blink_lev);
					  if( NBN_KB_CSM.led_blink.bit.b6==0){
							set_status_led(12,SET_LED_ON,n_blink,blink_lev);
							set_status_led(13,SET_LED_ON,n_blink,blink_lev);
					  }
					 }

			}

         }
	ret=1;

	return(ret);
}

void set_all_led_BKL_NBN(uint8_t status,uint8_t n_blink,uint8_t blink_lev){
	uint8_t id,tot_bkl_led;

	if (NBN_KB_CSM.model==KEYB_CSM131){//RETROCOMPATIBILITA' Keyboard
		tot_bkl_led=NUM_MAX_LEDS;//22;
	}else{
		tot_bkl_led=NUM_MAX_LEDS;
	}
		for(id=0;id<tot_bkl_led;id++){
				NBN_KB_CSM.led_bkl[id].status=status;
				NBN_KB_CSM.led_bkl[id].time_blink=5;
				NBN_KB_CSM.led_bkl[id].num_blink=n_blink;//
				NBN_KB_CSM.led_bkl[id].countdown_blink=(NBN_KB_CSM.led_bkl[id].num_blink*NBN_KB_CSM.led_bkl[id].time_blink);
				NBN_KB_CSM.led_bkl[id].blink_lev=blink_lev;
		}

}

uint8_t set_led_tasto_NBN(uint8_t key_code,uint8_t status,uint8_t n_blink,uint8_t blink_lev){

uint8_t ret,index;

    ret=1;
    index=0;


	switch(key_code){
			case(KEY_M1UP):
								   index=1;//KEY_M1UP;
								   break;
			case(KEY_M2UP):
								   index=2;//KEY_M2UP;
								   break;
			case(KEY_M3UP):
								   index=3;//KEY_M3UP;
								   break;
			case(KEY_M4UP):
								   index=4;//KEY_M4UP;
								   break;
			case(KEY_M5UP_LR):
								   index=5;//KEY_M5UP_LR;
								   break;
			case(KEY_M6UP):
								   index=6;//KEY_M6UP;
								   break;
			case(KEY_M7UP_LR):
								   index=15;//KEY_M7UP_LR;
								   break;
			case(KEY_M7UP_LO):
								   index=8;//KEY_M7UP_LO;
								   break;
			case(KEY_M7UP_RO):
								   index=9;//KEY_M7UP_RO;
								   break;
			case(KEY_M5UP_LO):
								   index=10;//KEY_M5UP_LO;
								   break;
			case(KEY_M5UP_RO):
								   index=11;//KEY_M5UP_RO;
								   break;
			case(KEY_M1DN):
								   index=12;//KEY_M1DN;
								   break;
			case(KEY_M2DN):
								   index=13;//KEY_M2DN;
								   break;
			case(KEY_M3DN):
								   index=15;//KEY_M3DN;
								   break;
			case(KEY_M4DN):
								   index=15;//KEY_M4DN;
								   break;
			case(KEY_M5DN_LR):
								   index=16;//KEY_M5DN_LR;
								   break;
			case(KEY_M6DN):
								   index=1;//KEY_M6DN;
								   break;
			case(KEY_M7DN_LR):
								   index=15;//KEY_M7DN_LR;
								   break;
			case(KEY_M7DN_LO):
								   index=15;//KEY_M7DN_LO;
								   break;
			case(KEY_M7DN_RO):
								   index=15;//KEY_M7DN_RO;
								   break;
			case(KEY_M5DN_LO):
								   index=1;//KEY_M5DN_LO;
								   break;
			case(KEY_M5DN_RO):
								   index=1;//KEY_M5DN_RO;
								   break;
			case(KEY_ROMBO):
								   index=0;//KEY_ROMBO;
								   break;
			case(KEY_RECALL):
								   index=0;//KEY_RECALL;
								   break;
			case(KEY_ZERO):
											   index=15;//KEY_ZERO;
											   break;
			case(KEY_RMEMA):
											   index=15;//KEY_RMEMA;
											   break;
			case(KEY_RMEMB):
											   index=15;//KEY_RMEMB;
											   break;
			case(KEY_RMEMC):
											   index=15;//KEY_RMEMC;
											   break;
			case(KEY_SMEMA):
											   index=15;//KEY_SMEMA;
											   break;
			case(KEY_SMEMB):
											   index=15;//KEY_SMEMB;
											   break;
			case(KEY_SMEMC):
											   index=15;//KEY_SMEMC;
											   break;
			case(KEY_REFLEX):
											   index=7;//KEY_REFLEX;
											   break;
			case(KEY_CHAIR):
											   index=11;//KEY_CHAIR;
											   break;
			case(KEY_FLEX):
											   index=15;//KEY_FLEX;
											   break;
			case(KEY_M1_ZR):
											   index=1;//KEY_M1_ZR;
											   break;
			case(KEY_M2_ZR):
											   index=1;//KEY_M2_ZR;
											   break;
			case(KEY_M3_ZR):
											   index=1;//KEY_M3_ZR;
											   break;
			case(KEY_M4_ZR):
											   index=1;//KEY_M4_ZR;
											   break;
			case(KEY_M5_ZR):
											   index=1;//KEY_M5_ZR;
											   break;
			case(KEY_M6_ZR):
											   index=1;//KEY_M6_ZR;
											   break;
			case(KEY_M7_ZR):
											   index=1;//KEY_M7_ZR;
											   break;
			case(KEY_JOY_UP_SX):
											   index=0;//KEY_JOY_UP_SX;
											   break;
			case(KEY_JOY_UP_DX):
											   index=0;//KEY_JOY_UP_DX;
											   break;
			case(KEY_JOY_DN_DX):
											   index=0;//KEY_JOY_DN_DX;
											   break;
			case(KEY_JOY_DN_SX):
											   index=0;//KEY_JOY_DN_SX;
											   break;
			case(KEY_RMEMD):
											   index=15;//KEY_RMEMD;
											   break;
			case(KEY_RMEME):
											   index=15;//KEY_RMEME;
											   break;
			case(KEY_RMEMF):
											   index=15;//KEY_RMEMF;
											   break;
			case(KEY_SMEMD):
											   index=15;//KEY_SMEMD;
											   break;
			case(KEY_SMEME):
											   index=15;//KEY_SMEME;
											   break;
			case(KEY_SMEMF):
											   index=15;//KEY_SMEMF;
											   break;
			case(KEY_SHIFT):
											   index=0;//KEY_SHIFT;
											   break;

			case(KEY_ALL):
					           index=NUM_MAX_LEDS;
					           break;
			case(KEY_NONE):
					          index=0;
					          break;
	     default:
	    	     index=0;
	    	     ret = KEY_NONE; //0;
	    	     break;
	}

	if(index>(NUM_MAX_LEDS-1)){
		for(index=0;index<NUM_MAX_LEDS;index++){
			NBN_KB_CSM.led_all[index].status=SET_LED_ON;
		}
	}else if(index==0){
			for(index=0;index<NUM_MAX_LEDS;index++){
				NBN_KB_CSM.led_all[index].status=SET_LED_OFF;
		    }
	      }else{
	    	  if(status!=SET_LED_BLINK){
			     NBN_KB_CSM.led_all[index].status=SET_LED_ON;//((NBN_KB_CSM.led_all[index].status==SET_LED_ON)?SET_LED_OFF:SET_LED_ON);
	    	  }else{
	    		   NBN_KB_CSM.led_all[index].status=SET_LED_BLINK;
				   NBN_KB_CSM.led_all[index].time_blink=blink_lev;
				   NBN_KB_CSM.led_all[index].num_blink =n_blink;//everlasting blinking
				   NBN_KB_CSM.led_all[index].countdown_blink=(NBN_KB_CSM.led_all[index].num_blink*NBN_KB_CSM.led_all[index].time_blink);
				   NBN_KB_CSM.led_all[index].blink_lev =BLINK_LED_FAST;
	    	  }
		  }
	return(ret);
}


void logo_fading(uint8_t status){
	if(status==SET_LED_OFF){
	  if(NBN_KB_CSM.fading_off_time==0){
		    NBN_KB_CSM.fading_off_time=0;
			if (NBN_KB_CSM.fading_time==0){
			  if(NBN_KB_CSM.fading_inc_dec==1){//rising
				if(NBN_KB_CSM.fading_duty< DUTY_FADING_MAX_LEV){
					NBN_KB_CSM.fading_duty+=DUTY_FADING_STEP;
				}else {
					NBN_KB_CSM.fading_duty=(DUTY_FADING_MAX_LEV);//+DUTY_FADING_STEP);
					NBN_KB_CSM.fading_inc_dec=0;

				}
			   }else{//falling
				   if(NBN_KB_CSM.fading_duty>DUTY_FADING_STEP){
						NBN_KB_CSM.fading_duty-=DUTY_FADING_STEP;
					}else {
						NBN_KB_CSM.fading_duty=0.00f;
						NBN_KB_CSM.fading_inc_dec=1;
						NBN_KB_CSM.fading_off_time=FADING_OFF_PERIOD_SEC;
					}
			   }

			  NBN_KB_CSM.fading_time=FADING_TIME_100MS;
		   }
	  }else{
		  NBN_KB_CSM.fading_duty=0.00f;
		  NBN_KB_CSM.fading_inc_dec=1;
	  }
	  set_led_BLK_NBN(LED_POWER_ID,SET_LED_OFF,0,0,0,LED_BKL_TYPE );
	  set_led_BLK_NBN(LED_LOGO_ID ,SET_LED_OFF,0,0,0,LED_BKL_TYPE );
	}else{
		 set_led_BLK_NBN(LED_POWER_ID,SET_LED_ON,0,0,0,LED_BKL_TYPE );
		 set_led_BLK_NBN(LED_LOGO_ID ,SET_LED_ON,0,0,0,LED_BKL_TYPE );
	}
}

void pca9685_refresh_led(uint8_t type){
uint8_t id;
 if (type==LED_TASTO_TYPE){
	 for(id=0;id<NUM_MAX_LEDS;id++){
		  if((id!=LED_POWER_ID)&&(id!=LED_LOGO_ID)&&(NBN_KB_CSM.led_all[id].status!=SET_LED_OFF)){
				 set_led_BLK_NBN(id,SET_LED_OFF,0,0,0,LED_BKL_TYPE );
		  }
		  set_led_NBN(id,NBN_KB_CSM.led_all[id].status,NBN_KB_CSM.led_all[id].time_blink,NBN_KB_CSM.led_all[id].num_blink,NBN_KB_CSM.led_all[id].blink_lev,LED_TASTO_TYPE );

	 }
 }else{
	 for(id=0;id<NUM_MAX_LEDS;id++){
		  if((id!=LED_POWER_ID)&&(id!=LED_LOGO_ID)){
			 // if((id<16)&&(NBN_KB_CSM.model==KEYB_CSM131)){//RETROCOMPATIBILITA' Keyboard{
					  if(NBN_KB_CSM.led_all[id].status!=SET_LED_OFF){
						  set_led_BLK_NBN(id,SET_LED_OFF,0,0,0,LED_BKL_TYPE );
					  }else{
						  set_led_BLK_NBN(id,NBN_KB_CSM.led_bkl[id].status,NBN_KB_CSM.led_bkl[id].time_blink,NBN_KB_CSM.led_bkl[id].num_blink,NBN_KB_CSM.led_bkl[id].blink_lev,LED_BKL_TYPE );
					  }
			 // }else{

				//  set_led_BLK_NBN(id,NBN_KB_CSM.led_bkl[id].status,NBN_KB_CSM.led_bkl[id].time_blink,NBN_KB_CSM.led_bkl[id].num_blink,NBN_KB_CSM.led_bkl[id].blink_lev,LED_BKL_TYPE );
			 // }
		 }
	 }
 }
}


void pca9685_refresh_led_fast(void){
uint8_t id,pca_id;
float duty;
pca9685_handle_t *handle;
uint8_t blink_lev;
LED_status_t status;
bool set;

	  //set_led_NBN(id,NBN_KB_CSM.led_all[id].status,NBN_KB_CSM.led_all[id].time_blink,NBN_KB_CSM.led_all[id].num_blink,NBN_KB_CSM.led_all[id].blink_lev,LED_TASTO_TYPE );
	  for(pca_id=0;pca_id<4;pca_id++){
		  handle=&pca9685_led[pca_id];
		  for(id=0;id<16;id++){
			  set=false;
			  switch(pca_id){
				case(0):
					  status=NBN_KB_CSM.led_all[id].status;
					  blink_lev=NBN_KB_CSM.led_all[id].blink_lev;
					  set=true;
					  break;
				case(1):
					  status=NBN_KB_CSM.led_all[id+16].status;
					  blink_lev=NBN_KB_CSM.led_all[id+16].blink_lev;
					  set=true;
					  break;
				case(2):

					  status=NBN_KB_CSM.led_bkl[id].status;
					  //blink_lev=NBN_KB_CSM.led_all[id].blink_lev;
					  if(NBN_KB_CSM.model==KEYB_CSM131){//RETROCOMPATIBILITA' Keyboard{
						  if(NBN_KB_CSM.led_all[id].status!=SET_LED_OFF){
							  status=SET_LED_OFF;
						  }
					  }
					  set=true;
					  break;
				case(3):
					  if((id !=(LED_POWER_ID-16))&&(id!=(LED_LOGO_ID-16))){
						  status=NBN_KB_CSM.led_bkl[id+16].status;
						  //blink_lev=NBN_KB_CSM.led_all[id+16].blink_lev;
						  if(NBN_KB_CSM.model==KEYB_CSM131){//RETROCOMPATIBILITA' Keyboard{
							  if(NBN_KB_CSM.led_all[id+16].status!=SET_LED_OFF){
								  status=SET_LED_OFF;
							  }
						  }
						  set=true;
					  }
					  break;
				default:
					  break;
			  }
	          if(set==true){
				  if(status==SET_LED_ON){
					duty=1.00f;
				  }else if(status==SET_LED_OFF){
							  duty=0.00f;
						  }else{//blink
							  if(pca_id<2){
								  duty=(float)(NBN_KB_CSM.blink_toggle[blink_lev]);
								  if(NBN_KB_CSM.led_all[id+pca_id*16].num_blink!=0){
									 if (NBN_KB_CSM.led_all[id+pca_id*16].countdown_blink>0){
										 NBN_KB_CSM.led_all[id+pca_id*16].countdown_blink--;
									 }else{
										 duty=0.00f;
									 }
								  }
							  }
						  }
				  pca9685_set_channel_duty_cycle(handle,id%16,duty,false);
	          }
	     }

    }
}



