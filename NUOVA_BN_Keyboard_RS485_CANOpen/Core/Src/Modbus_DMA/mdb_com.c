/*
 * mdb_com.c
 *
 *  Created on: 04 ago 2019
 *      Author: VALENTI
 */

/*=== Include files ==================================================================================================*/
#include "mdb_com.h"
#include "globals.h"




extern UART_HandleTypeDef huart8;
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;
extern UART_HandleTypeDef huart6;
extern DMA_HandleTypeDef hdma_usart1_rx; //Inverter RS485 opto Uart
extern DMA_HandleTypeDef hdma_usart1_tx; //Inverter RS485 opto Uart
extern DMA_HandleTypeDef hdma_usart2_rx; //Balance RS485 Uart
extern DMA_HandleTypeDef hdma_usart2_tx;
extern DMA_HandleTypeDef hdma_usart3_rx; //
extern DMA_HandleTypeDef hdma_usart3_tx;
extern DMA_HandleTypeDef hdma_usart6_rx; //HMI RS485 Uart
extern DMA_HandleTypeDef hdma_usart6_tx;
/*=== Constant definitions (File scope) ==============================================================================*/

#define MDB_USE_USART6

#ifdef MDB_USE_USART6

#define MDB_BAUD               (9600U)//(115200U)
#define MDB_USART               (huart6)
#define MDB_USART_GPIO         RS485_HMI_TX_GPIO_Port
//#define MDB_USART_CLK          (RCC_APB1Periph_USART2)
//#define MDB_USART_GPIO_CLK     (RCC_APB2Periph_GPIOA)
#define MDB_USART_RxPin        RS485_HMI_RX_Pin
#define MDB_USART_TxPin        RS485_HMI_TX_Pin
//#define MDB_USART_Remap        (GPIO_FullRemap_USART2)
//#define MDB_GPIO_MODE          (GPIO_Mode_AF_PP)
#define MDB_DMA_TX_CHANNEL     DMA_CHANNEL_5
#define MDB_DMA_RX_CHANNEL     DMA_CHANNEL_5
#define MDB_DMA_STREAM_TX      DMA2_Stream6
#define MDB_DMA_STREAM_RX      DMA2_Stream1
#define MDB_DMA_TX             hdma_usart6_tx
#define MDB_DMA_RX             hdma_usart6_rx
#define MDB_DMA_TX_FLAG        (DMA_FLAG_TCIF1_5)
#define MDB_DMA_RX_FLAG        (DMA_FLAG_TCIF1_5)

#elif defined( MDB_USE_USART1 )

#define MDB_BAUD               (9600)
#define MDB_USART              (huart1)
#define MDB_USART_GPIO         RS485_OPTO_MACINA_TX_GPIO_Port
//#define MDB_USART_CLK          (RCC_APB2Periph_USART1)
//#define MDB_USART_GPIO_CLK     (RCC_APB2Periph_GPIOA)
#define MDB_USART_RxPin        RS485_OPTO_MACINA_RX_Pin
#define MDB_USART_TxPin        RS485_OPTO_MACINA_TX_Pin
//#define MDB_USART_Remap        (GPIO_Remap_USART1)
//#define MDB_GPIO_MODE          (GPIO_Mode_AF_PP)
#define MDB_DMA_TX_CHANNEL     DMA_CHANNEL_4
#define MDB_DMA_RX_CHANNEL     DMA_CHANNEL_4
#define MDB_DMA_STREAM_TX      DMA2_Stream7
#define MDB_DMA_STREAM_RX      DMA2_Stream2
#define MDB_DMA_TX             hdma_usart1_tx
#define MDB_DMA_RX             hdma_usart1_rx
#define MDB_DMA_TX_FLAG        DMA_FLAG_TCIF0_4
#define MDB_DMA_RX_FLAG        DMA_FLAG_TCIF0_4


#elif defined (MDB_USE_USART3 )

#define MDB_BAUD               (115200)
#define MDB_USART              (huart3)
#define MDB_USART_GPIO         RS232_DBG_TX_GPIO_Port
//#define MDB_USART_CLK          (RCC_APB2Periph_USART1)
//#define MDB_USART_GPIO_CLK     (RCC_APB2Periph_GPIOA)
#define MDB_USART_RxPin        RS232_DBG_RX_Pin
#define MDB_USART_TxPin        RS232_DBG_TX_Pin
//#define MDB_USART_Remap        (GPIO_Remap_USART1)
//#define MDB_GPIO_MODE          (GPIO_Mode_AF_PP)
#define MDB_DMA_TX_CHANNEL     DMA_CHANNEL_4
#define MDB_DMA_RX_CHANNEL     DMA_CHANNEL_4
#define MDB_DMA_STREAM_TX      DMA1_Stream3
#define MDB_DMA_STREAM_RX      DMA1_Stream1
#define MDB_DMA_TX             hdma_usart3_tx
#define MDB_DMA_RX             hdma_usart3_rx
#define MDB_DMA_TX_FLAG        DMA_FLAG_TCIF0_4
#define MDB_DMA_RX_FLAG        DMA_FLAG_TCIF0_4
#endif


#if MDB_BAUD < 19200U

#define MDB_CHAR_US		(11 * 1000000U / MDB_BAUD)
#define MDB_TIMEOUT_US		((uint32_t)(35 * MDB_CHAR_US /10))

#else

#define MDB_TIMEOUT_US	((uint32_t)750)

#endif



/*=== Constant definitions DMA (File scope) ================================================================================================*/

#define DMA_COMM_BUFFER_PREFIX      (0xABBACAFE)
#define DMA_COMM_BUFFER_POSTFIX     (0xDEADFACE)

/* The RX buffer will be filled with this value. It should be different from any possible message ID and the inverted value */
#define DMA_BUFFER_DEFAULT_DATA     (0xFF)

#define DMA_DUMMY_RX_BUFFER_SIZE    (128)//(32)//(8)

#define DMA_LENGTH_SINGLE_BYTE      (1)


typedef enum {
	MDB_WAIT_MSG,
	MDB_SEND_REPLY
} MDB_STATUS;

/*=== Data definitions (File scope) ==================================================================================*/
T_DMA_COMM_CONTROL	dmaControlMdb  ;


static uint32_t last_recv_num = 0;

static MDB_STATUS mdb_status = MDB_WAIT_MSG;


/* This buffer is used when no receive is active. All data data will be received in this buffer
 * to avoid RC overruns */
static uint8_t dummyRxBuffer[DMA_DUMMY_RX_BUFFER_SIZE] ;


/*=== Local Function definitions (File scope) ==========================================================================================*/



/*=== Global function definitions (Global system or Local sub-system scope) ============================================================*/







/**
 *
 * @brief Initialize the UART RX and TX DMA channels
 *
 * @note \b MODULE \b INVOLVED:  MB
 *
 * @note \b STANDARD: This function respects the Standard 61508-2/3,
 * it is safety relevant respecting the Safety Requirement S.SW.1 and S.SW.15
 *
 * @note \b CONDICTIONS: Skipped on OB
 *
 *  @param[in]   dmaControl     Pointer to DMA control structure
 *
 */

void dma_comm_Initialize(T_DMA_COMM_CONTROL* dmaControl)
{


	  /* USART6_RX Init */
//	    hdma_usart6_rx.Instance = DMA2_Stream1;
//	    hdma_usart6_rx.Init.Channel = DMA_CHANNEL_5;
//	    hdma_usart6_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
//	    hdma_usart6_rx.Init.PeriphInc = DMA_PINC_DISABLE;
//	    hdma_usart6_rx.Init.MemInc = DMA_MINC_ENABLE;
//	    hdma_usart6_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
//	    hdma_usart6_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
//	    hdma_usart6_rx.Init.Mode = DMA_NORMAL;
//	    hdma_usart6_rx.Init.Priority = DMA_PRIORITY_MEDIUM;
//	    hdma_usart6_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
//	    if (HAL_DMA_Init(&hdma_usart6_rx) != HAL_OK)
//	    {
//	      Error_Handler();
//	    }
//
//	    __HAL_LINKDMA(&huart6,hdmarx,hdma_usart6_rx);

	/* USART3 DMA Init */
	   /* USART3_RX Init */
//	   hdma_usart3_rx.Instance = DMA1_Stream1;
//	   hdma_usart3_rx.Init.Channel = DMA_CHANNEL_4;
//	   hdma_usart3_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
//	   hdma_usart3_rx.Init.PeriphInc = DMA_PINC_DISABLE;
//	   hdma_usart3_rx.Init.MemInc = DMA_MINC_ENABLE;
//	   hdma_usart3_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
//	   hdma_usart3_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
//	   hdma_usart3_rx.Init.Mode = DMA_CIRCULAR;
//	   hdma_usart3_rx.Init.Priority = DMA_PRIORITY_HIGH;
//	   hdma_usart3_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
//	   if (HAL_DMA_Init(&hdma_usart3_rx) != HAL_OK)
//	   {
//	     Error_Handler();
//	   }



	   DMA_HandleTypeDef DMA_HandleRX;
	   /* USART6_RX Init */
	    DMA_HandleRX.Instance= MDB_DMA_STREAM_RX;
	    DMA_HandleRX.Init.Channel= MDB_DMA_RX_CHANNEL;
	    DMA_HandleRX.Init.Direction = DMA_PERIPH_TO_MEMORY;
	    DMA_HandleRX.Init.PeriphInc = DMA_PINC_DISABLE;
	    DMA_HandleRX.Init.MemInc = DMA_MINC_ENABLE;
	    DMA_HandleRX.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	    DMA_HandleRX.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	    DMA_HandleRX.Init.Mode = DMA_CIRCULAR;//DMA_NORMAL;//
	    DMA_HandleRX.Init.Priority = DMA_PRIORITY_HIGH;
	    dmaControl->rxDma=&DMA_HandleRX;

	    if (HAL_DMA_Init(dmaControl->rxDma) != HAL_OK)
        {
	    	Error_Handler();//Error_Handler(SYSTEM_DMA_INIT_ERROR);
	    }

        __HAL_LINKDMA(dmaControl->uart,hdmarx,MDB_DMA_RX);



}

/**
 *
 * @brief Send multiple buffer using DMA
 *
 * @note \b MODULE \b INVOLVED:  MB
 *
 * @note \b STANDARD: This function respects the Standard 61508-2/3,
 * it is safety relevant respecting the Safety Requirement S.SW.1 and S.SW.15
 *
 * @note \b CONDICTIONS: Skipped on OB
 *
 *  @param[in]   dmaControl     Pointer to DMA control structure
 *  @param[in]   length         Number of bytes to send
 *
 */
void dma_comm_sendBytes(T_DMA_COMM_CONTROL * const dmaControl, const uint8_t length)
{
	HAL_UART_Transmit(dmaControlMdb.uart,dmaControlMdb.txBuffer[0].data, length, 0xFFFF);

}

/**
 *
 * @brief Start DMA Receive
 *
 * @note \b MODULE \b INVOLVED:  MB
 *
 * @note \b STANDARD: This function respects the Standard 61508-2/3,
 * it is safety relevant respecting the Safety Requirement S.SW.1 and S.SW.15
 *
 * @note \b CONDICTIONS: Skipped on OB
 *
 *  @param[in]   dmaControl     Pointer to DMA control structure
 *  @param[in]   msgId          Message ID to search for
 *
 */
void dma_comm_startReceiveMessage(T_DMA_COMM_CONTROL * const dmaControl, const bool continous)
{
	//Every time a frame is received then DMA RX is reinitialized switching in a second app buffer
	//rxBuffer.data[0] and rxBuffer.data[1]

	RS485_TX_HMI_DISABLE; //RS485 Half Duplex Direction in RX default
	RS485_RX_HMI_ENABLE;
    /* Disable the Receive DMA channel */
	HAL_UART_DMAStop(dmaControl->uart);//DMA_Cmd(dmaControl->rxDmaChannel, DISABLE);
    dma_comm_cleanRxBuffer(dmaControl);

    /* Setup RX DMA */  /* Enable the Receive DMA channel */
    HAL_UART_Receive_DMA(dmaControl->uart,&dmaControl->rxBuffer.data[0],  DMA_COMM_BUFFER_SIZE);//DMA_Init(dmaControl->rxDmaChannel, &DMA_InitStructure);


}


/**
 *
 * @brief End DMA receive
 *
 * @note \b MODULE \b INVOLVED:  MB
 *
 * @note \b STANDARD: This function respects the Standard 61508-2/3,
 * it is safety relevant respecting the Safety Requirement S.SW.1 and S.SW.15
 *
 * @note \b CONDICTIONS: Skipped on OB
 *
 *  @param[in]   dmaControl     Pointer to DMA control structure
 *  @param[in]   msgId          Message ID to search for
 *
 * @return  returns pointer to TX buffer that can be filled
 *
 */
void dma_comm_endReceiveMessage(T_DMA_COMM_CONTROL * const dmaControl)
{

//	    /* Enable the Receive DMA channel */
//	    DMA_Cmd(dmaControl->rxDmaChannel, ENABLE);
//	    RS485_RX_HMI_DISABLE; //RS485 Half Duplex Direction in TX
//	    RS485_TX_HMI_ENABLE;
	 /* Disable the Receive DMA channel */
		HAL_UART_DMAStop(dmaControl->uart);//DMA_Cmd(dmaControl->rxDmaChannel, DISABLE);
		 /* Setup dummy RX DMA */
		HAL_UART_Receive_DMA(dmaControl->uart,&dummyRxBuffer[0], DMA_DUMMY_RX_BUFFER_SIZE);//
}


/**
 *
 * @brief Get pointer to TX buffer
 *
 * @note \b MODULE \b INVOLVED:  MB
 *
 * @note \b STANDARD: This function respects the Standard 61508-2/3,
 * it is safety relevant respecting the Safety Requirement S.SW.1 and S.SW.15
 *
 * @note \b CONDICTIONS: Skipped on OB
 *
 *  @param[in]   dmaControl     Pointer to DMA control structure
 *  @param[in]   msgId          Message ID to search for
 *
 * @return  returns pointer to TX buffer that can be filled
 *
 */
uint8_t* dma_comm_getTxBuffer(T_DMA_COMM_CONTROL* const dmaControl)
{
    return dmaControl->txBuffer[dmaControl->txBufferIndex].data;
}

/**
 *
 * @brief Get pointer to RX buffer
 *
 * @note \b MODULE \b INVOLVED:  MB
 *
 * @note \b STANDARD: This function respects the Standard 61508-2/3,
 * it is safety relevant respecting the Safety Requirement S.SW.1 and S.SW.15
 *
 * @note \b CONDICTIONS: Skipped on OB
 *
 *  @param[in]   dmaControl     Pointer to DMA control structure
 *  @param[in]   msgId          Message ID to search for
 *
 * @return  returns pointer to RX buffer that can be processed
 *
 */
uint8_t* dma_comm_getRxBuffer(T_DMA_COMM_CONTROL* const dmaControl)
{
    return dmaControl->rxBuffer.data;
}

/**
 *
 * @brief Clear RX buffer
 *
 * @note \b MODULE \b INVOLVED:  MB
 *
 * @note \b STANDARD: This function respects the Standard 61508-2/3,
 * it is safety relevant respecting the Safety Requirement S.SW.1 and S.SW.15
 *
 * @note \b CONDICTIONS: Skipped on OB
 *
 *  @param[in]   dmaControl     Pointer to DMA control structure
 *
 * @return  returns pointer to RX buffer that can be processed
 *
 */
void dma_comm_cleanRxBuffer(T_DMA_COMM_CONTROL* const dmaControl)
{
    int index;

    for (index = 0; index < DMA_COMM_BUFFER_SIZE; index++) {
        dmaControl->rxBuffer.data[index]  = DMA_BUFFER_DEFAULT_DATA;
    }

}

/**
 *
 * @brief Wait for TX ready
 *
 * @note \b MODULE \b INVOLVED:  MB
 *
 * @note \b STANDARD: This function respects the Standard 61508-2/3,
 * it is safety relevant respecting the Safety Requirement S.SW.1 and S.SW.15
 *
 * @note \b CONDICTIONS: Skipped on OB
 *
 *  @param[in]   dmaControl     Pointer to DMA control structure
 *
 * @return  returns pointer to RX buffer that can be processed
 *
 */
void dma_comm_waitForTxReady(T_DMA_COMM_CONTROL* const dmaControl)
{
    while (! dma_comm_isTxReady(dmaControl) );
}



bool dma_comm_isTxReady(T_DMA_COMM_CONTROL* const dmaControl)
{
//    FlagStatus  dmaFlagStatus = DMA_GetFlagStatus(dmaControl->txReadyFlag);
//    FlagStatus  usartFlagStatus = RESET;//dmaControl->uart->Instance->;//USART_GetFlagStatus(dmaControl->uart, USART_FLAG_TC);

//	return dmaFlagStatus == SET && usartFlagStatus == SET;
	HAL_UART_StateTypeDef usart1FlagStatus,usart2FlagStatus;
	usart1FlagStatus=huart1.gState;
	usart2FlagStatus=huart2.gState;
	return  ((usart1FlagStatus !=HAL_UART_STATE_BUSY_TX ) &&(usart2FlagStatus !=HAL_UART_STATE_BUSY_TX));
}





/*=== Global function definitions (Global system or Local sub-system scope) ============================================================*/

void mdb_comm_Initialize(void)
{



	 /* Configure the USART  Original*/
//	    RCC_APB1PeriphClockCmd(MDB_USART_CLK, ENABLE);
//	    RCC_APB2PeriphClockCmd(MDB_USART_GPIO_CLK | RCC_APB2Periph_AFIO , ENABLE);
//	    USART_Cmd(MDB_USART, ENABLE);
//	    USART_Init(MDB_USART, &mdb_usart_init);
//	    USART_HalfDuplexCmd(MDB_USART, DISABLE);
//	    dmaControlMdb.uart          = MDB_USART;
//	    dmaControlMdb.txDmaChannel  = MDB_DMA_TX_CHANNEL;
//	    dmaControlMdb.rxDmaChannel  = MDB_DMA_RX_CHANNEL;
//	    dmaControlMdb.txReadyFlag   = MDB_DMA_TX_FLAG;
//	    dmaControlMdb.rxReadyFlag   = MDB_DMA_RX_FLAG;
//	    dma_comm_Initialize(&dmaControlMdb);
//		dma_comm_startReceiveMessage(&dmaControlMdb, FALSE);
//		mdb_status = MDB_WAIT_MSG;
//		last_recv_num = 0;
//		last_recv_mdb_time = 0;
//		mdb_serial_setSlaveAddress(MDB_MB_ADDRESS);
	dmaControlMdb.uart          = &MDB_USART;
	dmaControlMdb.txDma         = &MDB_DMA_TX;
	dmaControlMdb.rxDma         = &MDB_DMA_RX;
	dmaControlMdb.DmaStreamTx   = MDB_DMA_STREAM_TX;
	dmaControlMdb.DmaStreamRx   = MDB_DMA_STREAM_RX;
	dmaControlMdb.txReadyFlag   = MDB_DMA_TX_FLAG;
	dmaControlMdb.rxReadyFlag   = MDB_DMA_RX_FLAG;
	dma_comm_Initialize(&dmaControlMdb);
	dma_comm_startReceiveMessage(&dmaControlMdb, FALSE);
	mdb_status = MDB_WAIT_MSG;
	last_recv_num = 0;
	last_recv_mdb_time = 0;
	//mdb_serial_setSlaveAddress(DP_SW_RS485_adr);



}


void mdb_comm_processMessage(void)
{
	uint32_t recv_len, reply_len;
	 //uint16_t modbus_cmd,modbus_reg;
	switch (mdb_status) {

	case MDB_WAIT_MSG:

	    recv_len = (DMA_COMM_BUFFER_SIZE -MDB_DMA_RX.Instance->NDTR);// dmaControlMdb.rxDma->Instance->CNDTR); //dmaControlMdb.rxDmaChannel->CNDTR;
	    if ( recv_len > last_recv_num) {
	    	last_recv_mdb_time = 0;// bas_systemTicksUs();carico l'istante in cui ho ricevuto ultimo byte o sequenza di bytes
	    	last_recv_num = recv_len;
	    } else if ( ( recv_len>LEN_MIN_FRAME_RS485) && (last_recv_mdb_time>TIME_END_OF_FRAME_RS485_MS)){//controllo che sia passato pi� del timeout necessario per ricevere 1 byte//bas_deltaTimeUs(last_recv_mdb_time) >  MDB_TIMEOUT_US ) {
	    	dma_comm_endReceiveMessage(&dmaControlMdb);
	    	last_recv_num = 0;
	    	last_recv_mdb_time = 0;
	    	//modbus_reg=((dmaControlMdb.rxBuffer.data[2]<<8)+dmaControlMdb.rxBuffer.data[3]);
	    	reply_len = mdb_serial_receiveMessage( dmaControlMdb.rxBuffer.data, recv_len,  dma_comm_getTxBuffer(&dmaControlMdb), DMA_COMM_BUFFER_SIZE );

	    	if ( reply_len > 0 ) {
	    		RS485_RX_HMI_DISABLE; //RS485 Half Duplex Direction in TX
	    	    RS485_TX_HMI_ENABLE;
	    	    dma_comm_sendBytes(&dmaControlMdb, reply_len);
	    	    RS485_TX_HMI_DISABLE; //RS485 Half Duplex Direction in TX
	    	 	RS485_RX_HMI_ENABLE;


	    	    //printf("\n\r REVO State:%2X Model:%2d \n\r",revo_state,revo_board_conf);

	    	    // modbus_cmd=dmaControlMdb.txBuffer[0].data[1];
	    	    //printf("\n\r Modbus Cmd:%2X Reg:%4X",modbus_cmd,modbus_reg);

	    		mdb_status = MDB_SEND_REPLY;
	    	} else {
		    	dma_comm_startReceiveMessage(&dmaControlMdb, false);
		    	mdb_status = MDB_WAIT_MSG;
	    	}
	    }
	    break;

	case MDB_SEND_REPLY:

		//if ( dma_comm_isTxReady(&dmaControlMdb) ) {//Attesa che il buffer di trasmissione si sia svuotato dall'ultima Tx
	    	dma_comm_startReceiveMessage(&dmaControlMdb, false);
	    	mdb_status = MDB_WAIT_MSG;
		//}
		break;

	default:
		break;
	}


}
