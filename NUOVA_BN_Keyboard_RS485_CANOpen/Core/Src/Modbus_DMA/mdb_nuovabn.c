/*
 * mdb.c
 *
 *  Created on: 04 ago 2019
 *      Author: VALENTI
 */


/*=== Include files ==================================================================================================*/

#include "globals.h"
#include "mdb_registers.h"
#include "mdb.h"
#include "./PCA9685_LED_DRIVER/pca9685.h"
#include "eeprom.h"
#include "usart.h"
/*=== Constant definitions (File scope) ==============================================================================*/

#define MDB_SLAVE_ADR_ANSWER        0x80

#define MDB_FN_READCOILSTATUS		0x01
#define MDB_FN_READDISCRETEINPUT	0x02
#define MDB_FN_READHOLDINGREG		0x03
#define MDB_FN_READINPUTREG			0x04
#define MDB_FN_FORCESINGLECOIL		0x05
#define MDB_FN_PRESETSINGLEREG		0x06
#define MDB_FN_READEXCEPTION		0x07
#define MDB_FN_FORCEMULTIPLECOILS 	0x0F
#define MDB_FN_FORCEMULTIPLEREGS	0x10

#define MDB_ERROR_SIZE	((uint32_t)2)
#define MDB_MULTIPLE_REG_HEADER_SIZE ((uint32_t)5)

#define MDB_MINIMUM_REPLY_SIZE	((uint32_t)2)


#define MDB_NUM_BYTES_ADDR ((uint32_t)1)
#define MDB_NUM_BYTES_CRC ((uint32_t)2)
#define MDB_SER_OVERHEAD	(MDB_NUM_BYTES_ADDR + MDB_NUM_BYTES_CRC)

#define MDB_INVALID_MDB_ADDRESS ((uint8_t)255)


/*=== Macro definitions (File scope) =================================================================================*/

#define READ_UNS16_BE(buffer,offset) ( (uint16_t)((uint8_t*)buffer)[offset]<<8|((uint8_t*)buffer)[(offset)+1] )
#define WRITE_UNS16_BE(buffer,offset,value) ((uint8_t*)buffer)[offset]=(uint8_t)((value)>>8), ((uint8_t*)buffer)[(offset)+1]=(uint8_t)((value)&0xFF)


/*=== Type definitions (File scope) ==================================================================================*/

//Internal parameter passing structure
typedef struct {
	uint8_t* rcv_data;
	uint32_t rcv_size;
	uint8_t* reply_data;
	uint32_t reply_max_size;
} ModbusMessage;


typedef enum {
	DISCRETE_INPUT,
	COIL,
	INPUT_REGISTER,
	HOLDING_REGISTER
} REGISTER_TYPE;

/*=== Constant definitions (File scope) ==============================================================================*/


/*=== Data definitions (File scope) ==================================================================================*/

static uint8_t mdb_slave_addr ;//= 0;


/*=== Function declarations (File scope) =============================================================================*/

static uint32_t reply_error(ModbusMessage*, uint8_t error);
//static uint32_t reply_echo(ModbusMessage*, uint32_t size);

//static uint32_t read_bits(REGISTER_TYPE type, ModbusMessage *msg);
//static uint32_t read_registers(REGISTER_TYPE type, ModbusMessage *msg);
//static uint32_t force_single_coil(ModbusMessage*);
//static uint32_t preset_single_register(ModbusMessage*);
//static uint32_t force_multiple_coils(ModbusMessage*);
//static uint32_t force_multiple_registers(ModbusMessage*);

static uint16_t mdb_crc16(uint8_t *block, uint32_t size);
//static uint16_t mdb_chksum16(uint8_t *block, uint32_t size);

static uint32_t read_paramater(KEYBOARD_CMD_enum_t type, ModbusMessage *msg);
//static uint32_t set_paramater(KEYBOARD_CMD_enum_t type, ModbusMessage *msg);
/*=== Global Function definitions ========================================================================*/

uint32_t mdb_receive_message(uint8_t* rcv_data, uint32_t rcv_len, uint8_t* reply_data, uint32_t reply_max_size)
{
	//structure to avoid passing 4 parameters
	ModbusMessage msg = {rcv_data, rcv_len, reply_data, reply_max_size};

	if ( rcv_len > 1 && reply_max_size >= MDB_MINIMUM_REPLY_SIZE )
		switch(rcv_data[0]) {
		case KEY_CMD_RD_TASTI:
			return read_paramater(KEY_CMD_RD_TASTI, &msg);
		case KEY_CMD_SET_LED_ON:
			return read_paramater(KEY_CMD_SET_LED_ON, &msg);
		case KEY_CMD_SET_LED_TOGGLE:
			return read_paramater(KEY_CMD_SET_LED_TOGGLE, &msg);
		case KEY_CMD_BEEP:
			return read_paramater(KEY_CMD_BEEP, &msg);
		case KEY_CMD_RD_MODEL:
			return read_paramater(KEY_CMD_RD_MODEL, &msg);
		case KEY_CMD_SET_LED_BKL:
			return read_paramater(KEY_CMD_SET_LED_BKL, &msg);
		case KEY_CMD_SET_LED_TASTO:
			return read_paramater(KEY_CMD_SET_LED_TASTO, &msg);
		case KEY_CMD_SET_LED_BAT:
			return read_paramater(KEY_CMD_SET_LED_BAT, &msg);
//		case KEY_CMD_SET_LED_CHARGE:
//			return read_paramater(KEY_CMD_SET_LED_CHARGE, &msg);
		case KEY_CMD_NEW1:
			 return read_paramater(KEY_CMD_NEW1, &msg);
		case KEY_CMD_SET_ADR_SLAVE:
			 return read_paramater(KEY_CMD_SET_ADR_SLAVE, &msg);
			 break;
		case KEY_CMD_SET_PROTOCOL:
			 return read_paramater(KEY_CMD_SET_PROTOCOL, &msg);
			 break;
		case KEY_CMD_SET_BAUDRATE:
			 return read_paramater(KEY_CMD_SET_BAUDRATE, &msg);
			 break;
		case KEY_CMD_SET_PARAM1:
			 return read_paramater(KEY_CMD_SET_PARAM1, &msg);
			 break;
		case KEY_CMD_RD_FW:
			 return read_paramater(KEY_CMD_RD_FW, &msg);
			 break;
		default:
			break;
		}

	return reply_error(&msg, MDB_ERR_ILLEGAL_FUNCTION);
}


/*=== Local Function definitions (File scope) ========================================================================*/

static uint32_t reply_error(ModbusMessage* msg, uint8_t error)
{
/*	MODBUS
 * if (msg->reply_max_size >= MDB_ERROR_SIZE) {
		msg->reply_data[0] = 0x80 | msg->rcv_data[0];
		msg->reply_data[1] = error;
		return MDB_ERROR_SIZE;
	} else {
		return 0;
	}
*/
/*NUOVA BN COMMAND ERROR*/
//	if (msg->reply_max_size >= MDB_ERROR_SIZE) {
//			//msg->reply_data[0] = //(msg->rcv_data[0] + MDB_SLAVE_ADR_ANSWER);
//			msg->reply_data[0] = 0x80 ;
//			msg->reply_data[1] = error;
//			return MDB_ERROR_SIZE;
//		} else {
//			return 0;
//		}
	return 0;
}


//static uint32_t reply_echo(ModbusMessage* msg, uint32_t size)
//{
//	int32_t i;
//	if ( msg->reply_max_size >= size ) {
//		for (i=0; i<size; i++) msg->reply_data[i] = msg->rcv_data[i];
//		return size;
//	} else {
//		return 0;
//	}
//}



//static uint32_t read_registers(REGISTER_TYPE type, ModbusMessage *msg)
//{
//	int32_t i;
//	int32_t value;
//	uint16_t index, num;
//
//	if (msg->rcv_size < 5){
//		return reply_error(msg, MDB_ERR_ILLEGAL_FUNCTION);
//	}
//
//	index = READ_UNS16_BE(msg->rcv_data,1);
//	num = READ_UNS16_BE(msg->rcv_data,3);
//
//	msg->reply_data[0] = msg->rcv_data[0];
//	msg->reply_data[1] = (uint8_t)(num*2); //numero byte risposta = 2 byte per word richiesta
//
//	// controlla che non stiamo chiedendo di creare un messaggio di risposta troppo lungo
//	if ( msg->reply_data[1] > msg->reply_max_size - 2 ) {
//		return reply_error( msg, MDB_ERR_ILLEGAL_FUNCTION );
//	}
//
//	i=2; //offset della prima word nella risposta
//	while ( num-- > 0 ) {
//		if (type==INPUT_REGISTER) value = mdb_read_input_register(index);
//		else value = mdb_read_holding_register(index);
//
//		if (value<0) {
//			return reply_error( msg, (uint8_t) -value );
//
//		} else {
//			WRITE_UNS16_BE(msg->reply_data, i, (uint16_t) value );
//		}
//		i+=2;
//		index++;
//	}
//
//	return msg->reply_data[1] + 2;
//}



//static uint32_t preset_single_register(ModbusMessage *msg)
//{
//	if (msg->rcv_size < 5) {
//		return reply_error(msg, MDB_ERR_ILLEGAL_FUNCTION);
//	}
//
//	uint16_t index = READ_UNS16_BE(msg->rcv_data,1);
//	uint16_t value = READ_UNS16_BE(msg->rcv_data,3);
//
//	int32_t error = mdb_write_holding_register(index, value);
//
//	if (error) return reply_error(msg, (uint8_t) -error);
//	else return reply_echo(msg, msg->rcv_size);
//}




//static uint32_t force_multiple_registers(ModbusMessage *msg)
//{
//	if (msg->rcv_size < MDB_MULTIPLE_REG_HEADER_SIZE) return reply_error(msg, MDB_ERR_ILLEGAL_FUNCTION);
//
//	uint16_t index = READ_UNS16_BE(msg->rcv_data,1); // data start
//	uint16_t num_words = READ_UNS16_BE(msg->rcv_data,3);
//	uint8_t num_bytes = msg->rcv_data[5];
//	int i=6;
//	int32_t error;
//
//	if ( (num_bytes&1) != 0 || num_bytes != num_words*2 || (num_bytes + MDB_MULTIPLE_REG_HEADER_SIZE) > msg->rcv_size ) {
//		return reply_error(msg, MDB_ERR_ILLEGAL_FUNCTION);
//	}
//
//	while ( (num_words--) > 0 ) {
//		uint16_t value = READ_UNS16_BE(msg->rcv_data,i);
//		error = mdb_write_holding_register(index++, value);
//		if (error) return reply_error(msg, (uint8_t) -error);
//		i+=2;
//	}
//
//	return reply_echo(msg, MDB_MULTIPLE_REG_HEADER_SIZE);
//}


/*Nuova BN protocol*/
static uint32_t read_paramater(KEYBOARD_CMD_enum_t type, ModbusMessage *msg){
//        int32_t i;
		uint32_t value;
		uint16_t index, num,blink_lev;
		uint16_t nblink,start,end,len=0;
		led_tasto_cmd_t data1;
		led_bat_cmd_t   data2;
		byte_bit_t index_b;
//		if (msg->rcv_size < 5){
//			return reply_error(msg, MDB_ERR_ILLEGAL_FUNCTION);
//		}

//		index = READ_UNS16_BE(msg->rcv_data,1);
//		num = READ_UNS16_BE(msg->rcv_data,3);
//		msg->reply_data[0] = msg->rcv_data[0];
//		msg->reply_data[1] = (uint8_t)(num*2); //numero byte risposta = 2 byte per word richiesta

		// controlla che non stiamo chiedendo di creare un messaggio di risposta troppo lungo
//		if ( msg->reply_data[1] > msg->reply_max_size - 2 ) {
//			return reply_error( msg, MDB_ERR_ILLEGAL_FUNCTION );
//		}

//		i=2; //offset della prima word nella risposta
//		while ( num-- > 0 ) {
//			if (type==INPUT_REGISTER) value = mdb_read_input_register(index);
//			else value = mdb_read_holding_register(index);
//
//			if (value<0) {
//				return reply_error( msg, (uint8_t) -value );
//
//			} else {
//				WRITE_UNS16_BE(msg->reply_data, i, (uint16_t) value );
//			}
//			i+=2;
//			index++;
//		}

		        msg->reply_data[len++] = type;


				switch(type) {
				case KEY_CMD_RD_TASTI:
					                        msg->reply_data[len++] =NBN_KB_CSM.key_code  ; //KEY-CODE
					                        msg->reply_data[len++] =NBN_KB_CSM.sleep_time;//NBN_KB_CSM.shift_key ; //KEY-SHIFT status
					                        msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
					                        break;

				case KEY_CMD_SET_LED_ON:
					                        index = msg->rcv_data[1];
					                        index_b.bytes=index;
					                        //NBN_KB_CSM.led_all[index].status=((NBN_KB_CSM.led_all[index].status==SET_LED_ON)?SET_LED_OFF:SET_LED_ON);
					                        set_led_tasto_retro_NBN(index_b,SET_LED_ON,0,5);
					                        msg->reply_data[len++] =NBN_KB_CSM.key_code  ; //KEY-CODE
										    msg->reply_data[len++] =NBN_KB_CSM.sleep_time;////KEY-SHIFT status
										    msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
					                        break;

				case KEY_CMD_SET_LED_TOGGLE:
					                        index = msg->rcv_data[1];
					                        index_b.bytes=index;
										   // NBN_KB_CSM.led_all[index].status=SET_LED_BLINK;
										   // NBN_KB_CSM.led_all[index].time_blink=5;
											//NBN_KB_CSM.led_all[index].num_blink =0;//everlasting blinking
											//NBN_KB_CSM.led_all[index].countdown_blink=(NBN_KB_CSM.led_all[index].num_blink*NBN_KB_CSM.led_all[index].time_blink);

											//NBN_KB_CSM.led_all[index].blink_lev =BLINK_LED_FAST;
					                        set_led_tasto_retro_NBN(index_b,SET_LED_BLINK,0,5);
					                        msg->reply_data[len++] =NBN_KB_CSM.key_code  ; //KEY-CODE
											msg->reply_data[len++] =NBN_KB_CSM.sleep_time;////KEY-SHIFT status
											msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
					                        break;


				case KEY_CMD_BEEP:
					                        index = msg->rcv_data[1];
					                        if(index>0){
					                        	NBN_KB_CSM.buzzer_time=BUZZER_BEEP10MS;
					                        }else{
					                        	NBN_KB_CSM.buzzer_time=0;
					                        }
					                        msg->reply_data[len++] =NBN_KB_CSM.key_code  ; //KEY-CODE
											msg->reply_data[len++] =NBN_KB_CSM.sleep_time;////NBN_KB_CSM.shift_key ; //KEY-SHIFT status
											msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
					                        break;

				case KEY_CMD_RD_MODEL:
					                        index = msg->rcv_data[1];
					                        if((index>0)&&(index<4)){
					                        	NBN_KB_CSM.model=index;
					                        	EE_SaveVariable(NBN_KB_CSM.model,EE_VAR_MODEL_ADR);
					                        }
					                        if (NBN_KB_CSM.model==KEYB_CSM131){//RETROCOMPATIBILITA' Keyboard
											   NBN_KB_CSM.keys_enabled.all32=NBN_MASK_ENABLED_KEYS_CSM131;
											   NBN_KB_CSM.keys_logic.all32  =NBN_MASK_LOGIC_KEYS_CSM125;
											}else{//All KEYS are enabled
											   NBN_KB_CSM.keys_enabled.all32=NBN_MASK_ENABLED_KEYS_CSM125;
											   NBN_KB_CSM.keys_logic.all32  =NBN_MASK_LOGIC_KEYS_CSM125;
											}
					                        msg->reply_data[len++] =NBN_KB_CSM.model ; //Model
											msg->reply_data[len++] =NBN_KB_CSM.sleep_time;// //padding
											msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
					                        break;
				case KEY_CMD_SET_LED_BKL:
					                        num = msg->rcv_data[1];
					                        //value = msg->rcv_data[4];
					                        if(num<SET_LED_BLINK){
												start=0;
												end=(NUM_MAX_LEDS);//*2);;
												for(index=start;index < end;index++){
													  //pca9685_set_led_tasto(index,msg->rcv_data[2],nblink,num);//if index is id tasto
													//if(index!=LED_CHARGE_ID){
													  NBN_KB_CSM.led_bkl[index].status =num;
													  //NBN_KB_CSM.led_all[index].status=msg->rcv_data[2];
													//}
												}
												if (NBN_KB_CSM.model!=KEYB_CSM131){//
													NBN_KB_CSM.lock_unlock_key=((num==SET_LED_OFF)?KEY_RELEASED:KEY_PRESSED);
													if(NBN_KB_CSM.lock_unlock_key==KEY_RELEASED){
														NBN_KB_CSM.fading_duty=DUTY_FADING_MIN_LEV;
														NBN_KB_CSM.fading_inc_dec=1;
														NBN_KB_CSM.fading_time=0;//FADING_TIME_100MS;
													}
												}
					                        }
											msg->reply_data[len++] =NBN_KB_CSM.key_code  ; //KEY-CODE
											msg->reply_data[len++] =NBN_KB_CSM.sleep_time;//NBN_KB_CSM.shift_key ; //KEY-SHIFT status
											msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
					                        break;

				case KEY_CMD_SET_LED_TASTO:
					                        index = msg->rcv_data[1];//id led
					                        num=msg->rcv_data[2];//status/blinklev /*Da version FW 1.24*/ //num=msg->rcv_data[3];//blink level
					                        //nblink=msg->rcv_data[3];//num of blink//nblink=msg->rcv_data[4];//num of blink
					                        if(index != 0xFF){
												//value = msg->rcv_data[4];
												start=index;
												end=index;
												blink_lev=0;
												data1.all=num;
												nblink=0;
											    if(data1.part.status<6){
													if(data1.part.status>SET_LED_OFF){
														blink_lev=(data1.part.status-SET_LED_BLINK);
														num=SET_LED_BLINK;
														//blink_lev=3;
														nblink=data1.part.nblink;
													}

												    pca9685_set_led_tasto(index,num,nblink,blink_lev);//if index is id tasto
											    }
//												if(num!=0){//timing blink not NULL
//													 set_led_tasto_NBN(index,SET_LED_BLINK,nblink,num);
//												}else{
//													set_led_tasto_NBN(index,msg->rcv_data[2],nblink,num);
//												}
					                        }else{//Broadcasting for all leds
					                        	start=0;
					                        	end=(NUM_MAX_LEDS);//*2);;
					                        	data1.all=num;
					                        	if(data1.part.status<6){
													for(index=start;index < end;index++){
														//pca9685_set_led_tasto(index,msg->rcv_data[2],nblink,num);//if index is id tasto
														if((index!=LED_CHARGE_ID)&&(index!=KEY_SHIFT_ID)){

															if(data1.part.status>SET_LED_OFF){//timing blink not NULL
															  blink_lev=(data1.part.status-SET_LED_BLINK);
															  nblink=data1.part.nblink;
															  NBN_KB_CSM.led_all[index].num_blink      =nblink;//msg->rcv_data[4];//everlasting blinking
															  NBN_KB_CSM.led_all[index].countdown_blink=(nblink*NBN_KB_CSM.blink_timing[blink_lev]);
															  NBN_KB_CSM.led_all[index].status         =SET_LED_BLINK;
															  NBN_KB_CSM.led_all[index].blink_lev      =blink_lev;
															  NBN_KB_CSM.led_all[index].time_blink     =NBN_KB_CSM.blink_timing[blink_lev];

															}else{
															  blink_lev=0;
															  nblink=0;
															  NBN_KB_CSM.led_all[index].num_blink      =nblink;//msg->rcv_data[4];//everlasting blinking
															  NBN_KB_CSM.led_all[index].countdown_blink=(nblink*NBN_KB_CSM.blink_timing[blink_lev]);
															  NBN_KB_CSM.led_all[index].status         =data1.part.status;//msg->rcv_data[2];
															  NBN_KB_CSM.led_all[index].blink_lev      =blink_lev;
															  NBN_KB_CSM.led_all[index].time_blink     =NBN_KB_CSM.blink_timing[blink_lev];

															}
														}
													}
					                        	}
					                        }

											msg->reply_data[len++] =NBN_KB_CSM.key_code  ; //KEY-CODE
											msg->reply_data[len++] =NBN_KB_CSM.sleep_time;//NBN_KB_CSM.shift_key ; //KEY-SHIFT status
											msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
				                        	break;
				case KEY_CMD_SET_LED_BAT:
											//value = msg->rcv_data[4];
					                        data2.all=msg->rcv_data[1];
					                        if (NBN_KB_CSM.model!=KEYB_CSM131){
//					                        	num=BAT_LEV_0;
//					                        	if(data2.led.led_25==1){
//					                        		num=BAT_LEV_25;
//					                        	}
//					                        	if(data2.led.led_50==1){
//													num=BAT_LEV_50;
//												}
//					                        	if(data2.led.led_75==1){
//													num=BAT_LEV_75;
//												}
//					                        	if(data2.led.led_100==1){
//													num=BAT_LEV_100;
//												}
//											    NBN_KB_CSM.bat_lev=num;
											    NBN_KB_CSM.bat_led.all=data2.all;
											    if(data2.led.charge==1){
											    	NBN_KB_CSM.led_charge=SET_LED_ON;
											    	NBN_KB_CSM.led_all[LED_CHARGE_ID].status =SET_LED_ON;
												}else{
													NBN_KB_CSM.led_charge=SET_LED_OFF;
													NBN_KB_CSM.led_all[LED_CHARGE_ID].status =SET_LED_OFF;
												}
					                         }
											 msg->reply_data[len++] =NBN_KB_CSM.key_code  ; //KEY-CODE
											 msg->reply_data[len++] =NBN_KB_CSM.sleep_time;//NBN_KB_CSM.shift_key ; //KEY-SHIFT status
											 msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
					                         break;
//				case KEY_CMD_SET_LED_CHARGE:
//					                        num=msg->rcv_data[1];
//					                        if (NBN_KB_CSM.model!=KEYB_CSM131){
//												if(num<SET_LED_BLINK){
//													NBN_KB_CSM.led_charge=num;
//													NBN_KB_CSM.led_all[LED_CHARGE_ID].status =num;
//												}
//					                        }
//											msg->reply_data[len++] =NBN_KB_CSM.key_code  ; //KEY-CODE
//											msg->reply_data[len++] =NBN_KB_CSM.sleep_time;//NBN_KB_CSM.shift_key ; //KEY-SHIFT status
//											msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
//					                        break;
				case KEY_CMD_NEW1:
					                        msg->reply_data[len++] =NBN_KB_CSM.key_code  ; //KEY-CODE
											msg->reply_data[len++] =NBN_KB_CSM.sleep_time;//NBN_KB_CSM.shift_key ; //KEY-SHIFT status
											msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
					                        break;
				case KEY_CMD_SET_ADR_SLAVE:
					                        index = msg->rcv_data[1];
					                        if((index > 0x2F)&&(index < 0x34)){//new address valid
					                        	DP_SW_RS485_adr=index;//address  to identify  Keyboard on RS485 ModBus
					                        	mdb_serial_setSlaveAddress(DP_SW_RS485_adr);
					                        	NBN_KB_CSM.slave=index;
					                        	EE_SaveVariable(NBN_KB_CSM.model,EE_VAR_SLAVE_ADR);
					                        }
					                        msg->reply_data[len++] =NBN_KB_CSM.key_code  ; //KEY-CODE
											msg->reply_data[len++] =NBN_KB_CSM.sleep_time;//NBN_KB_CSM.shift_key ; //KEY-SHIFT status
											msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
									        break;
				case KEY_CMD_SET_PROTOCOL:
											index = msg->rcv_data[1];
											if(index<2){//new address valid
												NBN_KB_CSM.protocol=index;
												EE_SaveVariable(NBN_KB_CSM.protocol,EE_VAR_PROTOCOL_ADR);
											}
											msg->reply_data[len++] =NBN_KB_CSM.key_code  ; //KEY-CODE
											msg->reply_data[len++] =NBN_KB_CSM.sleep_time;//NBN_KB_CSM.shift_key ; //KEY-SHIFT status
											msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
											break;
				case KEY_CMD_SET_BAUDRATE:
					                        index = msg->rcv_data[1];
                                            index <<=8;
                                            index+=msg->rcv_data[2];
                                            if(index!=0){
												NBN_KB_CSM.baudrate=index*100;
												EE_SaveVariable(index,EE_VAR_BAUD_ADR);
												msg->reply_data[len++] =NBN_KB_CSM.key_code  ; //KEY-CODE
												msg->reply_data[len++] =NBN_KB_CSM.sleep_time;//NBN_KB_CSM.shift_key ; //KEY-SHIFT status
												msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
                                            }else{
                                            	value=(NBN_KB_CSM.baudrate/100);
                                            	value>>=8;
                                            	msg->reply_data[len++] =(uint8_t) (value) ; //KEY-CODE
                                            	value=(NBN_KB_CSM.baudrate/100);

												msg->reply_data[len++] =(uint8_t)(value) ; //KEY-SHIFT status
												msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
                                            }


											break;
				case KEY_CMD_SET_PARAM1:
											index = msg->rcv_data[1];
											index <<=8;
											index+=msg->rcv_data[2];
					                        //NBN_KB_CSM.gotosleep=index;
					                        //EE_SaveVariable(index,EE_VAR_PARAM3_ADR);
					                    	msg->reply_data[len++] =NBN_KB_CSM.key_code  ; //KEY-CODE
											msg->reply_data[len++] =NBN_KB_CSM.sleep_time;//NBN_KB_CSM.shift_key ; //KEY-SHIFT status
											msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding
											break;

				case KEY_CMD_RD_FW:
											index = msg->rcv_data[1];
        									msg->reply_data[len++] =FW_VERSION ; //KEY-CODE
											msg->reply_data[len++] =FW_SUBVERSION ; //KEY-SHIFT status
											msg->reply_data[len++] =NBN_BYTE_PADDING ; //padding

                                            break;

				default:
					//return reply_error( msg, MDB_ERR_ILLEGAL_FUNCTION );
					break;
				}




		return len;//msg->reply_data[1] + 2;
}




/*=== Global function definitions (Global system or Local sub-system scope) ============================================================*/

void mdb_serial_setSlaveAddress(uint8_t slave_address)
{

	mdb_slave_addr = slave_address;

}


uint32_t mdb_serial_receiveMessage(uint8_t* rcv_data, uint32_t rcv_len, uint8_t* txBuffer, uint32_t txBufferLen)
{


	uint16_t crc;
	uint8_t addr = MDB_INVALID_MDB_ADDRESS;
	uint32_t reply_len = (uint32_t) 0;
//    uint8_t i;
//	  for(i=0;i<rcv_len;i++){
//			//printf("RX[%d]:%X \n\r",i,rcv_data[i]);
//		  buffer_RX_RS232[i]=rcv_data[i];
//		}
//	  new_data_rx_balance=true;
	// exclude messages too short or too long  TODO verify if txBufferLen is a good boundary
	if ( rcv_len > MDB_SER_OVERHEAD && rcv_len < txBufferLen ) {
		addr = rcv_data[0];
	}

	if ( addr == mdb_slave_addr ) {//( (addr == MDB_BROADCAST_ADDR) ||  ( addr == mdb_slave_addr )) {
		// check CRC
		crc =mdb_crc16(rcv_data, rcv_len - MDB_NUM_BYTES_CRC);//mdb_chksum16(rcv_data, rcv_len - MDB_NUM_BYTES_CRC);//
		if ( (crc&0xFF) == rcv_data[rcv_len - MDB_NUM_BYTES_CRC] &&
			 (crc>>8) == rcv_data[rcv_len-MDB_NUM_BYTES_CRC+1] )
		{
			// crc OK, decode payload message
			reply_len = mdb_receive_message(rcv_data + MDB_NUM_BYTES_ADDR,
					rcv_len - MDB_SER_OVERHEAD,
					txBuffer + MDB_NUM_BYTES_ADDR,
					txBufferLen - MDB_SER_OVERHEAD);

			//prepare serial reply prepending slave number and appending crc
			if ((addr != MDB_BROADCAST_ADDR) && (reply_len > 0) ) {
				//head slave number
				txBuffer[0] = mdb_slave_addr + MDB_SLAVE_ADR_ANSWER;

				//and tail crc
				crc =mdb_crc16(txBuffer, reply_len + MDB_NUM_BYTES_ADDR);// mdb_chksum16(txBuffer, reply_len + MDB_NUM_BYTES_ADDR);//
				txBuffer[reply_len + MDB_NUM_BYTES_ADDR] = (uint8_t)(crc&0xFF);
				txBuffer[reply_len + MDB_NUM_BYTES_ADDR + 1 ] = (uint8_t)(crc>>8);

				reply_len += MDB_SER_OVERHEAD;
			} else {
				//do not reply
				reply_len = (uint32_t)0;
			}
		}
	}


	return reply_len;
}



/*=== Local Function definitions (File scope) ========================================================================*/

//TODO faster crc16 calculation
static uint16_t mdb_crc16(uint8_t *block, uint32_t size)
{
	int i,j;
	uint16_t crc=0xffff;
	for (i=0; i<size; i++) {
		crc^= (uint16_t)block[i];
		for (j=0; j<8; j++) {
			if ((crc & 0x01)==0)
				crc>>=1;
			else
				crc=(crc>>1)^0xA001;
		}
	}
	return crc;
}

//static uint16_t mdb_chksum16(uint8_t *block, uint32_t size)
//{
//	int i;
//	uint16_t crc=0x0000;
//
//	for (i=0; i<size; i++) {
//		crc += (uint16_t)block[i];
//	}
//
//
//
//	return crc;
//}
