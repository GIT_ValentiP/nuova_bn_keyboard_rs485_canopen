/*
 * mdb_registers.c
 *
 *  Created on: 04 ago 2019
 *      Author: VALENTI
 */


/*=== Include files ====================================================================================================================*/


#include "mdb_registers.h"
#include "globals.h"

/*=== Global function definitions (Local sub-system scope) ============================================================*/

int32_t mdb_read_status(uint16_t index)
{
	return -MDB_ERR_ILLEGAL_DATAADDRESS;
}


int32_t mdb_read_coil(uint16_t index)
{

	return -MDB_ERR_ILLEGAL_DATAADDRESS;
}


int32_t mdb_write_coil(uint16_t index, uint16_t value)
{

	return -MDB_ERR_ILLEGAL_DATAADDRESS;

}

int32_t mdb_read_input_register(uint16_t index)
{

	return -MDB_ERR_ILLEGAL_DATAADDRESS;
}


int32_t mdb_read_holding_register(uint16_t index)
{
//uint16_t value_rd;
//	int16_t result_rd=0;
	switch ( index ) {
	   case MDB_FW_REL_REG      :
				return (uint16_t) version_fw[0];
	   case MDB_FW_INV_REG      :
	  		    return (uint16_t) version_fw[1];
	   case MDB_FW_LOADCELL_REG      :
	  		    return (uint16_t) version_fw[2];



 /*ILLUMINA PWM duty cycle*/


/*DEFAULT*/

		default:
						 return -MDB_ERR_ILLEGAL_DATAADDRESS;

	}


}




int32_t mdb_write_holding_register(uint16_t index, uint16_t value)
{

	//uint16_t new_value;
	 new_data_rx=true;



//#define MDB_SET_STOP_DOS1_REG                              0x070D
//#define MDB_SET_STOP_DOS2_REG                              0x070E
//#define MDB_SET_STOP_DOS3_REG                              0x070F
//#define MDB_SET_STOP_VIBRO_REG                             0x0710
	switch ( index ) {

	   case MDB_BOARD_CONFIG_REG:
		    new_data_rx=false;

            return MDB_OK;
/*REVO CONFIGURATION MODE*/
       case MDB_OPMODE_REG:



            return MDB_OK;

/*REVO OPERATIONAL COMMAND*/
       case MDB_OPMODE_CMD_REG:
    	 // if(revo_state==REVO_READY_TO_USE){  //((revo_state == REVO_IDLE )||(revo_state==REVO_READY_TO_USE))

             return MDB_OK;
    	 // }
    	 // return -MDB_ERR_ILLEGAL_DATAVALUE;

/*REVO CALIBRATION COMMAND*/
    	case MDB_CAL_CMD_REG :

            return MDB_OK;


/*HOPPER Weight SET*/
//	   case MDB_SET_HOP1_WEIGHT_REG:
//
//
//	  		hopper_burr[HOPPER_1].hopper_weight_target=value;
//		    //hopper_burr[HOPPER_1].hopper_weight_rd=0;
//	        return MDB_OK;
//
//	   case MDB_SET_HOP2_WEIGHT_REG:
//
//
//		  		hopper_burr[HOPPER_2].hopper_weight_target=value;
//			    //hopper_burr[HOPPER_2].hopper_weight_rd=0;
//		        return MDB_OK;
//
//	   case MDB_SET_HOP3_WEIGHT_REG:
//
//
//		  		hopper_burr[HOPPER_3].hopper_weight_target=value;
//			    //hopper_burr[HOPPER_3].hopper_weight_rd=0;
//		        return MDB_OK;
// /*HOPPER Time SET*/
//	   case MDB_SET_HOP1_TIME_REG:
//
//		  		hopper_burr[HOPPER_1].hopper_time_target=value;
//			    //hopper_burr[HOPPER_1].hopper_time_rd=0;
//		        return MDB_OK;
//
//	  case MDB_SET_HOP2_TIME_REG:
//
//			  		hopper_burr[HOPPER_2].hopper_time_target=value;
//				    //hopper_burr[HOPPER_2].hopper_time_rd=0;
//			        return MDB_OK;
//
//	  case MDB_SET_HOP3_TIME_REG:
//
//			  		hopper_burr[HOPPER_3].hopper_time_target=value;
//				    //hopper_burr[HOPPER_3].hopper_time_rd=0;
//			        return MDB_OK;
///*HOPPER SPEED*/
//	   case MDB_SET_HOP1_PWM_REG:
//
//			  		hopper_burr[HOPPER_1].hopper_speed_target=value;
//			        return MDB_OK;
//
//	   case MDB_SET_HOP2_PWM_REG:
//
//				  	hopper_burr[HOPPER_2].hopper_speed_target=value;
//				    return MDB_OK;
//
//	   case MDB_SET_HOP3_PWM_REG:
//
//				    hopper_burr[HOPPER_3].hopper_speed_target=value;
//				    return MDB_OK;
///*HOPPER OFFSET Weight*/
//	   case MDB_OFSET_HOP1_REG:
//
//					hopper_burr[HOPPER_1].hopper_weight_offset=value;
//					return MDB_OK;
//
//	   case MDB_OFSET_HOP2_REG:
//
//					hopper_burr[HOPPER_2].hopper_weight_offset=value;
//					return MDB_OK;
//
//	  case MDB_OFSET_HOP3_REG:
//
//				   hopper_burr[HOPPER_3].hopper_weight_offset=value;
//				   return MDB_OK;
//
///*GRINDER Power*/
//
//	  case MDB_SET_GRIND_POWER_REG:
//	 		                grinder.power_target=value;
//	 				        return MDB_OK;
//
///*GRINDING Burr distance */
//
//	 case MDB_SET_GRIND_SIZE_REG:
//		                grinder.grinding_size=value;
//				        return MDB_OK;
//
//	 case MDB_SET_GRIND_SPEED_REG:
//					    grinder.speed_rpm=value;
//				        return MDB_OK;
//
//	 case MDB_SET_GRIND_TIME_REG:
//					    grinder.time_grinding=value;
//				        return MDB_OK;
//
///* VIBRO REGULATION*/
//	 case MDB_SET_VIBRO_REG:
////		 doser_vibro[VIBRO_0].delay_over_stop=((value & 0x00FF)*1000);
////		 doser_vibro[VIBRO_0].pwm_duty=value>>8;
//		 return MDB_OK;
//
///* FAN Speed*/
//
//	 case MDB_SET_FAN_SPEED_REG:
//	 					fan[FAN_1].cooling_set.fan_w=value;
//	 					fan[FAN_2].cooling_set.fan_w=value;
//	 					return MDB_OK;
//	 case MDB_SET_TEMP_THRES1_REG:
//	 	 					fan[FAN_1].temp[0]=(value & 0x00FF);
//	 	 					fan[FAN_1].temp[1]=(value>>8);
//	 	 					fan[FAN_2].temp[0]=fan[FAN_1].temp[0];
//	 	 					fan[FAN_2].temp[1]=fan[FAN_1].temp[1];
//	 	 					return MDB_OK;
//	 case MDB_SET_TEMP_THRES2_REG:
//						    fan[FAN_1].temp[2]=(value & 0x00FF);
//							fan[FAN_1].temp[3]=(value>>8);
//							fan[FAN_2].temp[2]=fan[FAN_1].temp[2];
//							fan[FAN_2].temp[3]=fan[FAN_1].temp[3];
//	 	 					return MDB_OK;
//
///*DOSER and VIBRO PWM frequency*/
//	    case MDB_SET_DOS1_FREQ_REG:
//			        doser_vibro[DOSER_1].pwm_freq=value;
//			        doser_vibro[DOSER_1].cmd.b.FREQ=1;
//			        return MDB_OK;
//
//		case MDB_SET_DOS2_FREQ_REG:
//					doser_vibro[DOSER_2].pwm_freq=value;
//					doser_vibro[DOSER_2].cmd.b.FREQ=1;
//					return MDB_OK;
//
//		case MDB_SET_DOS3_FREQ_REG:
//					doser_vibro[DOSER_3].pwm_freq=value;
//					doser_vibro[DOSER_3].cmd.b.FREQ=1;
//					return MDB_OK;
//
//		case MDB_SET_VIBRO_FREQ_REG:
//					doser_vibro[VIBRO_0].pwm_freq=value;
//					doser_vibro[VIBRO_0].cmd.b.FREQ=1;
//					return MDB_OK;
//
///*DOSER and VIBRO PWM duty cycle*/
//		case MDB_SET_DOS1_DUTY_REG:
//					if (value<101){
//						doser_vibro[DOSER_1].pwm_duty=value;
//						doser_vibro[DOSER_1].cmd.b.DUTY=1;
//					}
//					return MDB_OK;
//		case MDB_SET_DOS2_DUTY_REG:
//						if (value<101){
//							doser_vibro[DOSER_2].pwm_duty=value;
//							doser_vibro[DOSER_2].cmd.b.DUTY=1;
//						}
//						return MDB_OK;
//		case MDB_SET_DOS3_DUTY_REG:
//						if (value<101){
//							doser_vibro[DOSER_3].pwm_duty=value;
//							doser_vibro[DOSER_3].cmd.b.DUTY=1;
//						}
//						return MDB_OK;
//		case MDB_SET_VIBRO_DUTY_REG:
//						if (value<101){
//							doser_vibro[VIBRO_0].pwm_duty=value;
//							doser_vibro[VIBRO_0].cmd.b.DUTY=1;
//						}
//						return MDB_OK;
//
///*DOSER and VIBRO Direction*/
//		case MDB_SET_DIR_DOS1_REG:
//						if (value<2){
//							doser_vibro[DOSER_1].DIR_pin=value;
//							doser_vibro[DOSER_1].DIR=value;
//							//HAL_GPIO_WritePin(DIR_DOSER1_GPIO_Port, DIR_DOSER1_Pin, value);
//							doser_vibro[DOSER_1].cmd.b.DIR=1;
//						}
//						return MDB_OK;
//		case MDB_SET_DIR_DOS2_REG:
//						if (value<2){
//							doser_vibro[DOSER_2].DIR_pin=value;
//							doser_vibro[DOSER_2].DIR=value;
//							doser_vibro[DOSER_2].cmd.b.DIR=1;
//						}
//						return MDB_OK;
//		case MDB_SET_DIR_DOS3_REG:
//						if (value<2){
//							doser_vibro[DOSER_3].DIR_pin=value;
//							doser_vibro[DOSER_3].DIR=value;
//							doser_vibro[DOSER_3].cmd.b.DIR=1;
//						}
//						return MDB_OK;
//
//		case MDB_SET_DIR_VIBRO_REG:
//						if (value<2){
//							doser_vibro[VIBRO_0].DIR_pin=value;
//							doser_vibro[VIBRO_0].DIR=value;
//							doser_vibro[VIBRO_0].cmd.b.DIR=1;
//						}
//						return MDB_OK;
//
///*DOSER and VIBRO DISable*/
//		case MDB_SET_STOP_DOS1_REG:
//						if (value<2){
//							doser_vibro[DOSER_1].DIS_pin=value;
//							doser_vibro[DOSER_1].DIS=value;
//							//HAL_GPIO_WritePin(DIS_DOSER1_GPIO_Port, DIS_DOSER1_Pin, value);
//							doser_vibro[DOSER_1].cmd.b.DIS=1;
//						}
//						return MDB_OK;
//
//		case MDB_SET_STOP_DOS2_REG:
//								if (value<2){
//									doser_vibro[DOSER_2].DIS_pin=value;
//									doser_vibro[DOSER_2].DIS=value;
//									doser_vibro[DOSER_2].cmd.b.DIS=1;
//								}
//								return MDB_OK;
//		case MDB_SET_STOP_DOS3_REG:
//								if (value<2){
//									doser_vibro[DOSER_3].DIS_pin=value;
//									doser_vibro[DOSER_3].DIS=value;
//									doser_vibro[DOSER_3].cmd.b.DIS=1;
//								}
//								return MDB_OK;
//		case MDB_SET_STOP_VIBRO_REG:
//								if (value<2){
//									doser_vibro[VIBRO_0].DIS_pin=value;
//									doser_vibro[VIBRO_0].DIS=value;
//									doser_vibro[VIBRO_0].cmd.b.DIS=1;
//								}
//								return MDB_OK;
//
///*FAN PWM frequency*/
//		case MDB_SET_FAN1_FREQ_REG:
//					fan[FAN_1].pwm_freq=value;
//					fan[FAN_1].cmd.b.FREQ=1;
//					return MDB_OK;
//
//		case MDB_SET_FAN2_FREQ_REG:
//					fan[FAN_2].pwm_freq=value;
//					fan[FAN_2].cmd.b.FREQ=1;
//					return MDB_OK;
//
//
///*FAN PWM duty cycle*/
//		case MDB_SET_FAN1_DUTY_REG:
//					if (value<101){
//						fan[FAN_1].pwm_duty[0]=value;
//						fan[FAN_1].cmd.b.DUTY=1;
//					}
//					return MDB_OK;
//		case MDB_SET_FAN2_DUTY_REG:
//						if (value<101){
//							fan[FAN_2].pwm_duty[0]=value;
//							fan[FAN_2].cmd.b.DUTY=1;
//						}
//				return MDB_OK;
//
///*FAN Power pin*/
//		case MDB_SET_POWER_FAN1_REG:
//						if (value<2){
//							fan[FAN_1].POWER_pin=value;
//							fan[FAN_1].cmd.b.DIS=1;
//						}
//						return MDB_OK;
//
//		case MDB_SET_POWER_FAN2_REG:
//								if (value<2){
//									fan[FAN_2].POWER_pin=value;
//
//									fan[FAN_2].cmd.b.DIS=1;
//								}
//						return MDB_OK;
//
//
///*HOPPER BURR STEPPER Pos TARGET*/
//			case MDB_SET_HOP1_POS_REG:
//				  hopper_burr[HOPPER_1].step_from_home=value;
//				 return MDB_OK;
//
//			case MDB_SET_HOP2_POS_REG:
//				  hopper_burr[HOPPER_2].step_from_home=value;
//				 return MDB_OK;
//
//			case MDB_SET_HOP3_POS_REG:
//				  hopper_burr[HOPPER_3].step_from_home=value;
//				 return MDB_OK;
//
//			case MDB_SET_BURR0_POS_REG:
//				  hopper_burr[BURR_0].step_from_home=value;
//				 return MDB_OK;
//
//
///*HOPPER BURR STEPPER Freq*/
//			case MDB_SET_HOP1_FREQ_REG:
//				  hopper_burr[HOPPER_1].pwm_freq=value;
//				 return MDB_OK;
//
//			case MDB_SET_HOP2_FREQ_REG:
//				  hopper_burr[HOPPER_2].pwm_freq=value;
//				 return MDB_OK;
//
//			case MDB_SET_HOP3_FREQ_REG:
//				  hopper_burr[HOPPER_3].pwm_freq=value;
//				 return MDB_OK;
//
//			case MDB_SET_BURR0_FREQ_REG:
//				  hopper_burr[BURR_0].pwm_freq=value;
//				 return MDB_OK;
//
// /*HOPPER BURR STEPPER DIR*/
//			case MDB_SET_HOP1_DIR_REG:
//				  hopper_burr[HOPPER_1].DIR_pin=value;
//				 return MDB_OK;
//
//			case MDB_SET_HOP2_DIR_REG:
//				  hopper_burr[HOPPER_2].DIR_pin=value;
//				 return MDB_OK;
//
//			case MDB_SET_HOP3_DIR_REG:
//				  hopper_burr[HOPPER_3].DIR_pin=value;
//				 return MDB_OK;
//
//			case MDB_SET_BURR0_DIR_REG:
//				  hopper_burr[BURR_0].DIR_pin=value;
//				 return MDB_OK;

/*ILLUMINA PWM duty cycle*/
	       case MDB_SET_ILLUMINA_DUTY_REG:

				 return MDB_OK;

		default:

			     new_data_rx=false;
			     return -MDB_ERR_ILLEGAL_DATAADDRESS;

		}


}
