/*
 ******************************************************************************
 *  @file      : scheduler_main.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 08 ott 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "scheduler_main.h"

//#include "fan.h"

#include "globals.h"
#include "mdb_com.h"
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

void scheduler_task(void){
//	KEYB_NOT_CONFIG      =0x00,
//		KEYB_READY           =0x01,
//		KEYB_RESET_TO_DEFAULT=0x02,
//		KEYB_ERROR           =0x03,
//		KEYB_STANDBY         =0x04,
//		KEYB_SLEEP           =0x05,
//		KEYB_CONFIG          =0x06,
//		KEYB_WAKEUP          =0x07,

	switch(NBN_KB_CSM.status){



	    case(KEYB_NOT_CONFIG):
								 /*HOPPER and BURR Stepper Init*/
								  NBN_KB_CSM.status=KEYB_NOT_CONFIG;

	   	    	                  break;


	    case(KEYB_READY):
		                         NBN_KB_CSM.status=KEYB_READY;
	 	    	                 break;


	    case(KEYB_STANDBY):

	                             NBN_KB_CSM.status=KEYB_STANDBY;
	    		                 break;

	    case(KEYB_CONFIG):

								 NBN_KB_CSM.status=KEYB_CONFIG;
	   	    	                 break;



	    case(KEYB_SLEEP):
		                          // printf("\n\r End Weigh1");
	                              // printf("\n\r Weight:%4ld",balance.weight_rd);
	                              NBN_KB_CSM.status=KEYB_SLEEP;
	   	    	                  break;

	    case(KEYB_WAKEUP):
	   		                          // printf("\n\r End Weigh1");
	   	                              // printf("\n\r Weight:%4ld",balance.weight_rd);
	   	                              NBN_KB_CSM.status=KEYB_WAKEUP;
	   	   	    	                  break;

	    case(KEYB_ERROR):

	   	    	                  break;



	    case(KEYB_RESET_TO_DEFAULT):
		                          printf("\n\r Grinding-Start");

		                          NBN_KB_CSM.status=KEYB_RESET_TO_DEFAULT;

	 	   	    	              break;



	    default:
	    	    NBN_KB_CSM.status=0;
	    	    break;
	}


}














/* Private user code ---------------------------------------------------------*/


